Mean CER point estimate: 3.26%
Mean CER 95.00% CI estimate: [2.92%; 3.59%]

Worst CER: 10.83%, book id 103, page 46:

Correct text:

34 Urkunden zum Jahre 1420.
[1420]. Ende April bis Anfang August.
Einzelausgaben der Stadt Görlitz für den Feldzug nach
Böhmen.
Görl. Rr. V. Bl. 37a ff. 1)
[Bl. 37 a] Dis nochgeschrebene geschefte ist gegangen of die
herfart:
Zu der rynner 2) banyr zwu elen roter seide 1/2 mr., vor
funf virteil weissir zeide 16 gr., deme moler zu lyme 10 gr., vor
60 elen lymet 1/2 sch.; vor 7 phund odern 3) 14 gr.; vor steyn-
kulen zu hauen zu den buchsen 9 fert.; vor eyne trone zu ge-
rethe 4 gr.; vor zwene kessel zu der herfart, die haben 1 1/2 stein
koppher minus 2 phund, dorzu ist unsers kopphers 1/2 steyn
2 phund, 7 fert.; vor einen iserinnen twirl 1 1/2 gr.; vor 6 exse
16 gr. 4); vor 6 kilhauen unde rodehauen 18 gr., vor 2 1/2 schok
hufysen 1 sch. 15 gr., item vor oberige hufnaile zu den ysen
18 gr.; item vor röste, dreiffüsse, stocczen 5) unde eine iserinne
spille zu deme gezelde 1/2 mr.; vor 4 neue schyben 6) zu deme
buchsenwayne 18 gr., vor waynschenen 7) zu denselben raden
34 gr., item dieselben rade zu beslohen 1/2 mr.; vor buchsenachsen,
schossbaume 8) zu deme buchsenwayne 6 gr.; item vor binden unde
börsten 9) die selben waynerade 2 1/2 gr. 10); deme buchsenmeister
vor eine trone zu den buchsen 6 gr.; item vor ein schok vor-
slege 11) zu den buchsen 5 gr.; [Bl. 370] Item vor 4 schirmen
ader 12) setczetarczhen zu machen 13) 12 gr.; vor haspen zu den
-
1) Die folgenden Eintragungen finden sich auch Bl. 48 b ff., so daß Bl. 37a bis
39b und Bl. 48 b bis 51 b des 5. Bandes der Ratsrechnungen bis auf kleine unwesent-
liche Verschiedenheiten dasselbe bringen. Die Ausgabeposten Bl. 48 b ff. sind gestrichen.
Alle Varianten anzugeben halte ich für unnütz
2) Mittelhochdeutsch rennaere Reitknecht.
3) Steht wohl für ader = Bogensehne.
4) 48 b kosten dieselben Ärte 21 gr.
5) Klötze.
6) Räder.
7) Wagenschienen.
8) Sind wahrscheinlich die Hemmbäume (Schuhe), die quer unter dem Wagenkasten
angebracht sind, vergl. mittelniederd. schot = Riegel, Verschluß behufs Hemmung.
9) Räder binden = mit Reifen versehen; das alliterirende borsten ist mir
nicht klar, es kommt des Öfteren in den Rr. vor.
10) 48 b steht 3 gr.
11) Vorschlag ist das Ortscheit am Büchsenwagen, f. Schmoller, bayr. Wörterb. II. S.18.
12) 49a steht unde.
13) 49a setzt hinzu mit eyme cymmermanne. Über Setzetarschen — eine
bretterne Wand zum Schutz des Büchsenmeisters — s. Anzeiger für Kunde der deutschen
Vorzeit 17. Bd. 1870 Sp. 356.


Predicted text:

 

 

 

———

   

TREE EE EEE

 

 

 

34 Urkunden zum Jahre 1420.

[1420]. Ende April bis Anfang August.
Einzelausgaben der Stadt Görlitz für den Feldzug nach
Böhmen.
Gěrl. Rr. V. Bl. sca ff.

5 /Bl. 370] Dis nochgeschrebene geschefte ist gegangen of die
herfart:

Zu der rynner?) banyr zwu elen roter seide '/a mr., vor
funf virteil weissir zeide 16 gr., deme moler zu lyme 10 gr., vor
60 elen lymet !/; sch.; vor ? phund odern?) 14 gr.; vor steyn-

;:; kulen zu hauen zu den buchsen 9 fert; vor eyne trone zu ge-
rethe 4 gr.; vor zwene kessel zu der herfart, die haben 1!/2 stein
koppher minus 2 phund, dorzu ist unsers kopphers !/ steyn
2 phund, 7 fert.; vor einen iserinnen twirl 1!/s gr.; vor 6 exse
16 gr.5; vor 6 kilhauen unde rodehauen 18 gr., vor 21/; schok

js hufysen 1 sch. 15 gr., item vor oberige hufnaile zu den ysen
18 gr.; item vor róste, dreiffüsse, stocezen?) unde eine iserinne
spille zu deme gezelde !/s mr.; vor 4 neue schyben$) zu deme
buchsenwayne 18 gr. vor waynschenen?) zu denselben raden
84 gr., item dieselben rade zu beslohen !/s mr.; vor buchsenachsen,

s; schossbaume*) zu deme buchsenwayne 6 gr.; item vor binden unde
bórsten?) die selben waynerade 2!/» gr.!); deme buchsenmeister
vor eine trone zu den buchsen 6 gr.; item vor ein schok vor-
slege!!) zu den buchsen b gr; /Bl. 37b] Item vor 4 schirmen
ader!?) setezetarczhen zu machen!?) 12 gr.; vor haspen zu den

1) Die folgenden Eintragungen finden fich auch Bl. agb ff., fo daf Bl. z7a bis
59b unó 3L. 48b bis sib des 5. Bandes der Ratsrechnungen bis auf Fleine unwefent-
fide Derfchiedenheiten daffelbe bringen. Die Ansgabepoften 3 48b ff. find geftrichen.
Alle Varianten anzugeben halte ih für unnüg.

2) Mittelhochdeutfch rennaere Reitfnecht.

3) Steht wohl für ader = Bogenfehne.

4) 48) Foften diefelben Arte 21 gr.

5) Xlöße.

6) Räder.

7) Wagenfchienen.

8) Sind wahrfcheinlich die Hemmbänume (fchuhe), die quer unter dem YDageufajteit
angebracht {ind, vergl. mittelniederd. schot = Riegel, Derfchluf behufs Hemmung,

9) Räder binden = mit Reifen verfehen; das alliterirende börsten if mir
nicht Far, es Fommt des Öfteren in den Rr. vor.

10) 48b {teht 3 ar.

1) Dorfchlag tft das Ortfcheit am Büchfenwagen, f. Schmoller, bayr. Wörterb, IL. S.18.

12) 49a {teht unde. .

18) 49a fegt hinzu mit eyme cymmermanne, Über Sebetarjden — eine
bretterné Mand 3um Sd bes Xidfenmeifters — f. 2Iusetger. für Kunde dev deutýchen
Dorsett 17. 230. 1870 Sp. 356.


Best CER: 1.07%, book id 944, page 437:

Correct text:

418
rum, et pene omnia privilegia ad manus ipsius generosi domini
Joannis data sunt fideliter servanda.
Nach einer Aufzeichnung auf Papier aus dem 17. Jahrhundert in
Stiftsarchive zu Hohenfurt.

CLXXV.
1425 cc., October 28, Goldenkron. — Abt Rüdiger von Goldenkron
an die von Budweis um schnelle Hilfe gegen die heranrückenden Hussiten.

Prudentibus viris magistro civium, consulibus et iuratis
in Budwaiss vicinis et amicis dilectis cum devotis in domino
orationibus pronam complacendi voluntatem! Vicini et amici
dilecti! Quia veridice sumus praemoniti, quod Wiklephistae
iam in monte congregati monasterium nostrum ahuc hodie vel
cras mane omnino invadere disposuerunt, petimus igitur amici-
tiam vestram, de qua plenam fiduciam habemus, quatenus
absque omni negligentia (et?) statim nobis cum aliquot bali-
stariis et armatis hominibus numero quo poteritis maiori sub-
venire velitis, sic quod iidem, quos duxeritis dirigendos, adhuc
hodie die et nocte festinando veniant et sint in monasterio
constituti. Hoc volumus in simili erga vestram amicitiam
promereri. Datum dominica die ante festum omnium sanctorum.
Ruthgerus in Sancta Corona vester.

Nach Abschriften aus dem 17. Jahrhundert im Stiftsarchive zu Hohen-
furt. Eine ebendaselbst befindliche Klostergeschichte aus demselben Zeitraum,
welche wir schon mehrmal zu citiren Veranlassung hatten, hat diesen Brief
gleichfalls aufgenommen und geradezu in das Jahr 1420 versetzt, offenbar
weil es hiess, dass in diesem Jahre das Kloster von den Hussiten zerstört
worden sei. Aber diese Zerstörung erfolgte bereits am 10. Mai (Palacky,
Gesch. v. Böhmen, III b. 99) und vorstehendes Schreiben datirt vom October.
Mit der Zerstörung kann daher dieses Schreiben in keine Verbindung ge-
bracht werden. Weil aber der Inhalt des Schreibens, dessen Original im Bud-
weiser Archive zu suchen wäre, keine Anhaltspunkte bietet, um überhaupt ein
Jahr mit Sicherheit für dasselbe zu bestimmen, so haben wir es bloss mit
Rücksicht auf die vorhergehende selbst allerlei Zweifeln Raum gebende Num-
mer hier eingereiht, auch das Tagesdatum hiernach reducirt und wollen uns
sonst gerne einer wirklichen Reparatur unterwerfen, wenn eine solche von

ist, welches zur Zeit Herrn Johanns errichtet worden ist; daher obige
Abweichungen. Die erwähnte Klostergeschichte lässt übrigens durch-
scheinen, dass der Schatz wenigstens nicht mehr ganz restituirt worden;
vergl. deshalb aber N. CCLXII.


Predicted text:

418

rum, et pene omnia privilegia ad manus ipsius generosi domini
Joannis data sunt fideliter servanda.

Nach einer Aufzeichnung auf Papier aus dem 17. Jahrhundert im
Btiftsarchive zu Hohenfurt.

CLXXV.

1425 cc. October 28, Goldenkron. —- Abt Hüdiger von Goldenkron
an die von Budweis um schnelle Hilfe gegen die heranrückenden Husiten.

Prudentibus viris magistro civium, consulibus et iuratis
in Budwaiss vicinis et amicis dilectis cum devotis in domino
orationibus pronam complacendi voluntatem! Vicini et amici
dilecti! Quia veridice sumus praemoniti, quod Wiklephistae
iam in monte congregati monasterium nostrum ahuc hodie vel
cras mane omnino invadere disposuerunt, petimus igitur amici-
tiam vestram, de qua plenam fiduciam habemus, quatenus
absque omni negligentia (et?) statim nobis cum aliquot bali-
stariis et armatis hominibus numero quo poteritis maiori sub-
venire velitis, sic quod iidem, quos duxeritis dirigendos, adhuc
hodie die et nocte festinando veniant et sint in monasterio
constituti. Hoc volumus in simili erga vestram amicitiam
promereri. Datum dominica die ante festum omnium sanctorum.

Ruthgerus in Sancta Corona vester.

Nach Abschriften aus dem 17. Jahrhundert im Stiftsarchive zu Hohen-
furt..Eine ebendaselbst befindliche Klostergeschichte aus demselben Zeitraum.
welche wir schon mehrmal zu citiren Veranlassung hatten, hat diesen Brief
gleichfalls aufgenommen und geradezu in das Jahr 1420 versetzt, offenbar
weil es hiess, dass in diesem Jahre das Kloster von den Hussiten zerstórt
worden sei. Aber diese Zerstörung erfolgte bereits am 10. Mai (Palackr.
Gesch. v. Bóhmen, IIIb. 99) und vorstehendes Schreiben datirt vom October.
Mit der Zerstórung kann daher dieses Schreiben in keine Verbindung g€
bracht werden, Weil aber der Inhalt des Schreibens, dessen Original im Bud-
weiser Archive zu suchen wäre, keine Anhaltspunkte bietet, um überhaupt ein
Jahr mit Sicherheit für dasselbe zu bestimmen, so haben wir es bloss mit
Rücksicht auf die vorhergehende selbst allerlei Zweifeln Raum gebende Num-
mer hier eingereiht, auch das Tagesdatum hiernach reducirt und wollen uns
sonst gerne einer wirklichen Reparatur unterwerfen, wenn eine solche von

ist, welches zur Zeit Herrn Johanns errichtet worden ist; daher obige
Abweichungen. Die erwühnte Klostergeschichte lüsst übrigens durch
scheinen, dass der Schatz wenigstens nicht mehr ganz restituirt wordet;
vergl. deshalb aber N. CCLXII.



Mean WER point estimate: 7.43%
Mean WER 95.00% CI estimate: [6.50%; 8.35%]

Worst WER: 24.29%, book id 103, page 45:

Correct text:

Urkunden zum Jahre 1420. 33

am 27. März 1420) in einen Prozesse, den 9 Männer aus
der Diöcese Bamberg gegen den Landvogt Hlawatsch, dessen
"substitutus” Friedrich Küchenmeister, die Städte Bautzen,
Görlitz, Zittau und Tetschen, sowie gegen den „Simon de
Tetzin“ [es ist wohl Sigmund von Wartenberg gemeint] und
Nikolaus Panik angestellt haben. Die Klage betrifft einen
Raub an Waaren und Vieh, der unmittelbar vor den Thoren
Bautzens trotz des Geleitsbriefes des Hlawatsch an den Händlern
geschehen ist.

Nach der Originalurkunde im Görlitzer Ratsarchiv mit angehängtem
Siegel des Johannes Jakobi de Pilonno. — Abschrift in den Ober=
lausitzer Urkundenabschriften V. Bl. 25 — 30. Regest im Oberlauf.
Urkundenverzeichnis II. S. 3. — Vgl. Provinzialblätter S. 79 ff., N. L.
Mag. 66 S. 94 f.

1420. Ende April bis Anfang August.

Kosten, welche der Stadt Görlitz für den Feldzug nach Böhmen
aufliefen.
Aus Görl. Rr. V. Bl. 55a; Grünhagen script. rer. Silesiac. VI. S. 173,
ders. Hussitenkämpfe S. 31.
Noch Christi geburt 1420, als Nicklas Weyder burgermeister
was, do man mynem allergnedigsten herrn dem keiszer, als her
zum irsten in das land kein Behemen zoch, do man zu im quam
zuo Sweidnicz und synen gnaden fulgete bis vor Prage 14 wochin
mit 42 spissin 1) zu y dem spysse 4 pherde, hat man verzert
summa 330 sch. 3 gr.
Sehr auffallend iſt, daß der erste Feldzug der Oberlausitzer nach Böhmen
unter das Bürgermeisteramt des Niklas Weyder gesetzt ist. Derselbe bekleidete
nämlich nach dem Görlitzer Kürbuche (in der Bibliothek der Oberl. Gesell-
schaft L. II. 283) vom 28. September 1420 bis zum 27. September 1421 diese
Würde. Die heerfahrt der Görlitzer aber fällt ganz ſicherlich von dem Ende
April bis Auguſt 1420. Nur iſt wenigſtens nach Kloß Hussitenkrieg
mspt. I. S. 541 (dazu stimmt auch die von den umstehenden Eintragungen ab-
weichende Schrift) die vorliegende Kostenberechnung erst im J. 1433 erfolgt,
also 13 Jahre später, als die in Rede stehende heerfahrt. Damals aber
mochte man im Sturme und Drange der Zeit die Regierungszeiten der Bürger-
meister nicht genau mehr im Gedächtnis haben. Vergl. meine Anmerkung
zu den Görlitzer Rr. vom 13. Juni 1422 und zur Urkunde 1422, September
und Oktober, wo in der nämlichen Aufstellung vom J. 1433 ähnliche Ver-
sehen unterliefen.

1) Kloß las falsch 16 Spieße, woher Grünhagen Hussitenkämpfe S. 31 seine un-
richtige Notiz hat (derselbe schreibt außerdem noch, der Spieß habe 16 Pferde gehabt).
3


Predicted text:

 

30

35

Urkunden zum Jahre 1420. 38

am 27. Mürz 14920) im einem Prozesse, den 9 Mümmer aus
der Diücese Bamberg gegen dem Lamdvogt Hlawatsch, dessem
* substitutus" Friedrich .IKiichenmeister, die Städte Bautzen,
Görlitz, Zittau und Tetschen, sowie gegen den „Simon de
Tetzin“ [es ist wohl Sigmund von Wartenberg gemeint] und
Nikolaus Panik angestellt haben. Die Klage betrifft einen
Raub an Waaren und Vieh, der unmittelbar vor den Thoren
Bautzens trotz des Geleitsbriefes des Hlawatsch an den Händlern
geschehen ist.

Nach der Originalurfunde im SGörliger Ratsarchiv mit angehängtem
Siegel des Johannes Jafobi de pilonno. — Abfchrift in den Ober:
[aufíger lrfunbenab[driften V. 23L 25— 50. — Neaeft im Oberlauf.
Urfundenverzeichnis II. S. 3. — Dal. provinstalblàtter S. zg ff., AT. $.
Mag. 66 S. 94 f.

1420. Ende April bis Anfang August.

Kosten, welche der Stadt Görlitz für den Feldzug nach Böhmen
auf liefen.
Aus Görl. Yr. V. 33[. 552; Gtiünhagen script. rer. Silesiae. VI. S. 125,
derf. DuffitenKimpfe S. 5t.

Noch Christi geburt 1420, als Nicklas Weyder burgermeister
was, do man mynem allergnedigsten herrn dem keiszer, als her
zum irsten in das land kein Behemen zoch, do man zu im quam
zuo Sweidniez und synen gnaden fulgete bis vor Prage 14 wochin
mit 42 spissin! zu y dem spysse 4 pherde, hat man verzert
summa 330 sch. 3 gr.

Sehr auffallend ift, daß der erfte Feldzug der Oberlanfiger nach Böhmen
unter das Bürgermeifteramt des ANiklas Weyder gefetzt ijt. Derfelbe bekleidete
nämlich nach dem Görliger Kürbuche (in der Bibliothek der Oberl. SGefell-
ídaft £. II. 285) vom 28. September 1420 bis zum 27. September 1421 diefe
Würde. Die Heerfahrt der Görliger aber fällt ganz ficherlich von dem Ende
April bis Auguft 1420. Yun ij — wenigftens nach Kloß Huffitenkrieg
mspt. I. S. 541 (dazu ftimmt auch die von den umftehenden Eintragungen ab-
weichende Schrift) — die vorliegende Koftenberechnung erft im X. 1453 erfolgt,
aljo 15 Jahre jpůter, als die in Rede jtehenbe Deerfahrt. Damals aber
mochte man im Sturme und Drange der Zeit die Regierungszeiten der Bürger:
meifter nicht genau mehr im Gedächtnis haben, Veral.. meine Anmerkung
zu den SGörliger Nr. vom 15. jjuni 1422 und zUr Mrfunde 1422, September
uno Oftober, wo in der nämlichen Aufftellung vom I. 1455 ähnliche Der-
{ehen unterliefen.

 

1) Kloß fas faljd) 16 Spiefe, woher Grünhagen Dujfitenfümpfe S. 5 feine uns

ridtige 21ofis hat (derfelbe fchreibt außerdem noch, der Spieß habe 16 Pferde gehabt).

3


Best WER: 1.53%, book id 350, page 34:

Correct text:

Č. 27–29: Po 24. dubnu-12. června 1420.
15
27.
Po 24. dubnu 1420
Jan z Hrádku řeč. Lopata, Děpolt z Vržna, Jiřík z Malejovic a Černín z Chu-
denic vystříhají se proti Oldřichovi z Rožmberka.
AČ. IV, 379 (v seznamu těch, kteří odpověděli Pražanům). - Srv. k tomu výklad Tomkův
v Děj. Prahy IV2, 52.
28.
Na Vyšehradě, 31. května 1420.
Král Zikmund Oldřichovi z Rožmberka: aby zbořil Tábor a nemůže-li, aby
přitáhl se vší svou mocí ku Praze jemu na pomoc.
Třeboň, Schwarzenb. archiv: Hist. 204, or. pap.
AČ. I, 12 č. 6. — Palacký, UB. I, 30 č. 25 (reg.). — RI. XI, 4138.
Zigmund, z božie milosti římský král, po vše časy rozmnožitel říše a uherský
a český etc. král.
Oldřiše z Rozemberka! Tvému jsme listu dobře srozuměli. A zvláště jakož nám
píšeš o těch Thábořích na Hradiščku etc., věz, že jsme již zde pánu bohu děkujíce
a proti zdejším Tháboróm sě dálibuoh snažíme, žeť těm na Hradiščko nemnoho pomoci
dadie. Protož přikazujem, aby předsě ten Thábor na Hradiščku rozplašil a zbořil.
Pakliby k tomu učiniti nynie nemohl, tehdy chcem, aby inhed bez meškánie se vší
svú mocí i s jíznými i s pěšími lidmi sem k nám přispěl a přitáhl. Neb jsme již na
tom ostali, že těch Tháborov ukrutnost a neřády déle nechcem trpěti, než to mieníme
staviti s boží pomocí. Dán na Vyšehradě ten pátek po Letniciech, léta králov-
stvie našich uherského etc. v XXXIIII a římského v desátém létě.
Ad mandatum domini regis
Michael canonicus Pragensis.
Na rubu: Nobili Ulrico de Rozemberg.
Pečeť odpadla.

29.
Na Vyšehradě, 12. června 1420.
Král Zikmund Oldřichovi z Rožmberka: aby k němu bez prodlení přijel, jak
již vzkázal.
1) List Oldřichův se nezachoval.


Predicted text:

Č. 27—29: Po 24. dubnu—12. června 1420. 15

27. Po 24. dubnu 1420

Jan z Hrádku Tec. Lopata, Dépolt z Vržna, Jiřík z Malejovic a Cernín z Chu-
denic vystříhají se proti Olařichovi z Rožmberka.

AČ. IV, 379 (v seznamu těch, kteří odpověděli Pražanům). — Srv. k tomu výklad Tomkův
v Děj. Prahy IV?, 52.

28. Na Vyšehradě, 31. května 1420.

Král Zikmund Oldřichovi z Rožmberka: aby zbořil Tábor a nemůže-li, aby
přitáhl se vší svou mocí ku Praze jemu na pomoc.

Třeboň, Schwarzenb. archiv: Hist. 204, or. pap.
AČ. I, 12 č. 6. — Palacký, UB. I, 80 č. 25 (reg). — RI. XI, 4138.

Zigmund, z boZie milosti římský král, po vše časy rozmnožitel říše a uherský
a český etc. král.

Oldřiše z Rozemberka! Tvému jsme listu dobře srozuměli.' A zvláště jakož nám
píšeš o těch Thábořích na Hradiščku etc., věz, žet jsme již zde pánu bohu děkujíce
a proti zdejším Tháboróm sě dálibuoh snažíme, žet těm na Hradiščko nemnoho pomoci
dadie. Protož přikazujem, aby předsě ten Thábor na Hradiščku rozplašil a zbořil.
Pakliby k tomu učiniti nynie nemohl, tehdy chcem, aby inhed bez meškánie se vší
svú mocí i s jíznými i s pěšími lidmi sem k nám přispěl a přitáhl. Neb jsme již na
tom ostali, že těch Tháboróv ukrutnost a neřády déle nechcem trpěti, než to mieníme
staviti s boží pomocí. Dán na Vyšehradě ten pátek po Letniciech, léta králov-
stvie našich uherského etc. v XXXIII a římského v desátém létě.

Ad mandatum domini regis
Michael canonicus Pragensis.

Na rubu: Nobili Ulrico de Rozemberg.
Pečeť odpadla.

29. Na Vyšehradě, 12. června 1420.

Král Zikmund Oldřichovi z Rožmberka: aby k němu bez prodlení přijel, jak
Již vzkázal.

1) List Oldřichův se nezachoval.



Worst and best CER and WER are reported on texts of character length >= 1000
