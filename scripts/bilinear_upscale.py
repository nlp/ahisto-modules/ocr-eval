# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import sys

import cv2
from tqdm import tqdm

from .common import read_facts


INPUT_ROOT = Path(sys.argv[1])
INPUT_UPSCALED_FILENAMES = list(read_facts(sys.argv[2]))
OUTPUT_ROOT = Path(sys.argv[3])


def upscale(args):
    _, upscaled_filename = args
    input_filename = INPUT_ROOT / upscaled_filename
    if input_filename.exists():
        output_filename = OUTPUT_ROOT / upscaled_filename
        output_dirname = output_filename.parent
        output_dirname.mkdir(parents=True, exist_ok=True)
        image = cv2.imread(str(input_filename))
        dimensions = (image.shape[1] * 2, image.shape[0] * 2)
        image = cv2.resize(image, dimensions)
        cv2.imwrite(str(output_filename), image)
        return True
    else:
        return False


def main():
    logger = logging.getLogger('main')

    OUTPUT_ROOT.mkdir(exist_ok=True)
    filenames = INPUT_UPSCALED_FILENAMES
    filenames = tqdm(filenames, 'Bilinear upscaling {}/**/* to {}/**/*'.format(
        INPUT_ROOT,
        OUTPUT_ROOT,
    ))
    with Pool(None) as pool:
        successfully_upscaled = 0
        total = 0
        for success in pool.imap_unordered(upscale, filenames):
            if success:
                successfully_upscaled += 1
            total += 1

    logger.info('Successfully upscaled {} out of {} files'.format(successfully_upscaled, total))


if __name__ == '__main__':
    main()
