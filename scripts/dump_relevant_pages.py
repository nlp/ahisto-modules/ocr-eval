# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


import sys

from .common import dump_relevant_pages


SQL_PARAMETERS = {
    'user': sys.argv[1],
    'password': sys.argv[2],
    'db': sys.argv[3],
}
INPUT_FILENAME = sys.argv[4]
OUTPUT_FILENAME = sys.argv[5]


def main():
    dump_relevant_pages(SQL_PARAMETERS, INPUT_FILENAME, OUTPUT_FILENAME)


if __name__ == '__main__':
    main()
