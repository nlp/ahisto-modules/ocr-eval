# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


import json
from multiprocessing import Pool
from pathlib import Path
import sys

from tqdm import tqdm

from .common import read_page_languages


INPUT_FILENAMES = Path(sys.argv[1])
INPUT_OCR_ROOT = Path(sys.argv[2])
OUTPUT_OCR_ROOT = Path(sys.argv[3])


def get_languages_worker(filename):
    basename = str(INPUT_OCR_ROOT / filename.parent / filename.stem)
    languages = read_page_languages(basename)
    return (filename, languages)


def get_languages():
    filenames = INPUT_FILENAMES.open('rt')
    filenames = map(lambda s: s.rstrip('\r\n'), filenames)
    filenames = map(Path, filenames)
    filenames = list(filenames)
    with Pool(None) as pool:
        languages = pool.imap(get_languages_worker, filenames)
        languages = tqdm(languages, desc='Reading detected page languages', total=len(filenames))
        for result in languages:
            filename, languages = result
            yield (filename, languages)


def extract():
    logger = logging.getLogger('extract')

    num_pages = 0
    for filename, languages in get_languages():
        filename = '{}.json'.format(OUTPUT_OCR_ROOT / filename.parent / filename.stem)
        with open(filename, 'wt') as f:
            json.dump(languages, f, sort_keys=True, indent=4)
            num_pages += 1

    logger.info(
        'Extracted languages for {} pages'.format(
            num_pages,
        )
    )


if __name__ == '__main__':
    extract()
