# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import sys

from gensim.corpora.dictionary import Dictionary
from gensim.models.tfidfmodel import TfidfModel
from tqdm import tqdm

from .common import read_filenames, image_filename_to_book, load_all_ocr_texts, get_cossims, tokenize, get_tfidf as _get_tfidf, index_images, preprocess_image, read_book_ids


INPUT_ROOT = Path(sys.argv[1])
INPUT_IMAGE_FILENAMES_LIST_FILENAME = sys.argv[2]
DOWNSCALED_INPUT_ROOT = Path(sys.argv[3])
DOWNSCALED_INPUT_IMAGE_FILENAMES_LIST_FILENAME = sys.argv[4]
OUTPUT_OCR_ROOT = Path(sys.argv[5])
INPUT_JSON_DUMP_FILENAME = sys.argv[6]
TFIDF_FILENAME = str(Path(INPUT_JSON_DUMP_FILENAME).with_suffix('.tfidf'))
DICTIONARY_FILENAME = str(Path(INPUT_JSON_DUMP_FILENAME).with_suffix('.dictionary'))
CSV_DATABASE_FILENAME = sys.argv[7]
OUTPUT_FILENAME = Path(sys.argv[8])

INPUT_IMAGE_FILENAMES = list(read_filenames(INPUT_IMAGE_FILENAMES_LIST_FILENAME))
DOWNSCALED_INPUT_IMAGE_FILENAMES = list(read_filenames(DOWNSCALED_INPUT_IMAGE_FILENAMES_LIST_FILENAME))
INPUT_JSON_DUMP = load_all_ocr_texts(INPUT_JSON_DUMP_FILENAME)
INPUT_BOOK_IDS_MAP, ALL_INPUT_BOOK_IDS = read_book_ids(INPUT_ROOT, CSV_DATABASE_FILENAME)
DOWNSCALED_INPUT_IMAGE_FILENAMES = [
    filename
    for filename
    in read_filenames(DOWNSCALED_INPUT_IMAGE_FILENAMES_LIST_FILENAME)
    if image_filename_to_book(filename)[0] in ALL_INPUT_BOOK_IDS
]

LOGGER.info(
    'Filtered downscaled images down to {} scanned page images from {} books'.format(
        len(DOWNSCALED_INPUT_IMAGE_FILENAMES),
        len(
            set(
                image_filename_to_book(filename)[0]
                for filename
                in DOWNSCALED_INPUT_IMAGE_FILENAMES
            )
        ),
    )
)

ANNOY_K = 100


def get_tfidf():
    logger = logging.getLogger('get_tfidf')

    try:
        dictionary = Dictionary.load(DICTIONARY_FILENAME)
        tfidf = TfidfModel.load(TFIDF_FILENAME)
        logger.info('Loaded TFIDF model from {} and {}'.format(DICTIONARY_FILENAME, TFIDF_FILENAME))
    except IOError:
        logger.info('Failed to load TFIDF model from {} and {}'.format(DICTIONARY_FILENAME, TFIDF_FILENAME))
        texts = (text for _, pages in INPUT_JSON_DUMP.items() for text in pages.values())
        dictionary, tfidf = _get_tfidf(texts)
        dictionary.save(DICTIONARY_FILENAME)
        tfidf.save(TFIDF_FILENAME)
        logger.info('Saved TFIDF model to {} and {}'.format(DICTIONARY_FILENAME, TFIDF_FILENAME))
    return (dictionary, tfidf)


DICTIONARY, TFIDF = get_tfidf()


def _read_image_texts_worker(image_filename):
    text_filename = OUTPUT_OCR_ROOT / image_filename
    text_filename = text_filename.with_suffix('.txt')
    with text_filename.open('rt') as f:
        text = f.read()
    text = tokenize(text)
    return text


def read_image_texts(filenames):
    with Pool(None) as pool:
        image_texts = pool.imap(_read_image_texts_worker, filenames)
        for image_text in image_texts:
            image_text = DICTIONARY.doc2bow(image_text)
            image_text = TFIDF[image_text]
            yield image_text


with Pool(None) as pool:
    OUTPUT_OCR_CORPUS = read_image_texts(INPUT_IMAGE_FILENAMES)
    OUTPUT_OCR_CORPUS = zip(tqdm(INPUT_IMAGE_FILENAMES, desc='Tokenizing OCR output texts'), OUTPUT_OCR_CORPUS)
    OUTPUT_OCR_CORPUS = dict(OUTPUT_OCR_CORPUS)


def _read_downscaled_image_texts(filename):
    book_id, page = image_filename_to_book(filename)
    if book_id not in INPUT_JSON_DUMP:
        return None
    if page not in INPUT_JSON_DUMP[book_id]:
        return None
    text = INPUT_JSON_DUMP[book_id][page]
    text = tokenize(text)
    return text


def read_downscaled_image_texts(filenames):
    with Pool(None) as pool:
        image_texts = pool.imap(_read_downscaled_image_texts, filenames)
        for image_text in image_texts:
            if image_text is not None:
                image_text = DICTIONARY.doc2bow(image_text)
                image_text = TFIDF[image_text]
            yield image_text


FILENAME_MAP = dict()
INDEX = dict()
for basename, book_ids in tqdm(INPUT_BOOK_IDS_MAP.items(), desc='Indexing full-scale image'):
    FILENAME_MAP[basename], INDEX[basename] = index_images(
        INPUT_ROOT,
        tqdm(
            [
                filename
                for filename
                in INPUT_IMAGE_FILENAMES
                if basename in filename
            ],
            desc='for basename {}'.format(basename),
            leave=None,
        ),
    )


def get_index(filename):
    book_id, _ = image_filename_to_book(filename)
    basenames = [
        basename
        for basename, book_ids
        in INPUT_BOOK_IDS_MAP.items()
        if book_id in book_ids
    ]
    if len(basenames) != 1:
        message = 'Filename {} corresponds to {} basenames'.format(filename, len(basenames))
        raise ValueError(message)
    basename, = basenames
    filename_map, index = FILENAME_MAP[basename], INDEX[basename]
    return (filename_map, index)


def query_image(filename):
    filename_map, index = get_index(filename)
    filename = str(DOWNSCALED_INPUT_ROOT / filename)

    image = preprocess_image(filename)
    results = index.get_nns_by_vector(image, ANNOY_K)
    results = [filename_map[identifier] for identifier in results]
    return results


def _query_images_helper(args):
    query_image_filename, query_image_text = args

    if query_image_text is None:
        return None

    query_result_filenames_by_appearance = query_image(query_image_filename)
    first_query_result_filename_by_appearance = query_result_filenames_by_appearance[0]

    result_image_texts = [OUTPUT_OCR_CORPUS[filename] for filename in query_result_filenames_by_appearance]
    similarities = get_cossims(DICTIONARY, query_image_text, result_image_texts)
    query_result_filenames_by_text = zip(similarities, query_result_filenames_by_appearance)
    query_result_filenames_by_text = sorted(query_result_filenames_by_text, reverse=True)
    _, first_query_result_filename_by_text = query_result_filenames_by_text[0]

    if first_query_result_filename_by_appearance == first_query_result_filename_by_text:
        return (query_image_filename, first_query_result_filename_by_appearance)
    else:
        return False


def query_images():
    logger = logging.getLogger('query_images')

    query_image_filenames = DOWNSCALED_INPUT_IMAGE_FILENAMES

    query_image_texts = read_downscaled_image_texts(query_image_filenames)
    query_image_texts = zip(tqdm(query_image_filenames, desc='Querying with downscaled images'), query_image_texts)

    linked_filenames = dict()
    linked_pages = dict()
    skipped = 0

    with Pool(None) as pool:
        results = pool.imap(_query_images_helper, query_image_texts)
        for result in results:
            if not result:
                if result is None:
                    skipped += 1
                continue
            query_image_filename, first_query_result_filename = result
            book_id, page = image_filename_to_book(query_image_filename)
            if book_id not in linked_pages:
                linked_pages[book_id] = set()
            if page not in linked_pages[book_id]:
                linked_filenames[query_image_filename] = first_query_result_filename
                linked_pages[book_id].add(page)

    linked_book_ids = set(
        book_id
        for book_id, pages
        in INPUT_JSON_DUMP.items()
        if book_id in linked_pages and len(linked_pages[book_id]) == len(pages)
    )

    logger.info(
        'Linked {} out of {} query images, skipped {}, completing {} out of {} books, spanning {}'.format(
            len(linked_filenames),
            len(query_image_filenames),
            skipped,
            len(linked_book_ids),
            len(INPUT_JSON_DUMP),
            len(linked_pages)
        )
    )

    return (linked_filenames, linked_book_ids)


def main():
    linked_filenames, linked_book_ids = query_images()
    linked_filenames = sorted(linked_filenames.items())
    with OUTPUT_FILENAME.open('wt') as f:
        for downscaled_input_image_filename, input_image_filename in linked_filenames:
            book_id, _ = image_filename_to_book(downscaled_input_image_filename)
            if book_id in linked_book_ids:
                # when a book is fully linked
                line = '{}\t=>\t{}'.format(downscaled_input_image_filename, input_image_filename)
            else:
                # when a book is only partly linked
                line = '{}\t->\t{}'.format(downscaled_input_image_filename, input_image_filename)
            print(line, file=f)


if __name__ == '__main__':
    main()
