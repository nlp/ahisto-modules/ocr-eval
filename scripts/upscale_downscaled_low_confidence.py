# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import sys

from scipy.spatial.distance import hamming
from tqdm import tqdm

from .common import read_filenames, image_filename_to_book, load_all_ocr_texts, index_images, preprocess_image, read_book_ids


INPUT_ROOT = Path(sys.argv[1])
INPUT_IMAGE_FILENAMES_LIST_FILENAME = sys.argv[2]
DOWNSCALED_INPUT_ROOT = Path(sys.argv[3])
DOWNSCALED_INPUT_IMAGE_FILENAMES_LIST_FILENAME = sys.argv[4]
INPUT_JSON_DUMP_FILENAME = sys.argv[5]
CSV_DATABASE_FILENAME = sys.argv[6]
OUTPUT_FILENAME = Path(sys.argv[7])

INPUT_JSON_DUMP = load_all_ocr_texts(INPUT_JSON_DUMP_FILENAME)
INPUT_IMAGE_FILENAMES = list(read_filenames(INPUT_IMAGE_FILENAMES_LIST_FILENAME))
INPUT_BOOK_IDS_MAP, ALL_INPUT_BOOK_IDS = read_book_ids(INPUT_ROOT, CSV_DATABASE_FILENAME)
DOWNSCALED_INPUT_IMAGE_FILENAMES = [
    filename
    for filename
    in read_filenames(DOWNSCALED_INPUT_IMAGE_FILENAMES_LIST_FILENAME)
    if image_filename_to_book(filename)[0] in ALL_INPUT_BOOK_IDS
]

LOGGER.info(
    'Filtered downscaled images down to {} scanned page images from {} books'.format(
        len(DOWNSCALED_INPUT_IMAGE_FILENAMES),
        len(
            set(
                image_filename_to_book(filename)[0]
                for filename
                in DOWNSCALED_INPUT_IMAGE_FILENAMES
            )
        ),
    )
)


def get_page_number_limits():
    input_pages = {
        book_id: set(pages.keys())
        for book_id, pages
        in INPUT_JSON_DUMP.items()
    }
    downscaled_input_pages = dict()
    for filename in DOWNSCALED_INPUT_IMAGE_FILENAMES:
        book_id, page = image_filename_to_book(filename)
        if book_id not in downscaled_input_pages:
            downscaled_input_pages[book_id] = set()
        downscaled_input_pages[book_id].add(page)
    limits = dict()
    for book_id, pages in downscaled_input_pages.items():
        upper_limit = len(pages)
        lower_limit = len(input_pages[book_id]) if book_id in input_pages else upper_limit
        assert lower_limit <= upper_limit
        limits[book_id] = (lower_limit, upper_limit)
    return limits


FILENAME_MAP = dict()
INDEX = dict()
for basename, book_ids in tqdm(INPUT_BOOK_IDS_MAP.items(), desc='Indexing downscaled images'):
    FILENAME_MAP[basename], INDEX[basename] = index_images(
        DOWNSCALED_INPUT_ROOT,
        tqdm(
            [
                filename
                for filename
                in DOWNSCALED_INPUT_IMAGE_FILENAMES
                if image_filename_to_book(filename)[0] in book_ids
            ],
            desc='for basename {}'.format(basename),
            leave=None,
        ),
    )


def get_index(filename):
    basenames = [
        basename
        for basename
        in INPUT_BOOK_IDS_MAP
        if basename in filename
    ]
    if len(basenames) != 1:
        message = 'Filename {} corresponds to {} basenames'.format(filename, len(basenames))
        raise ValueError(message)
    basename, = basenames
    filename_map, index = FILENAME_MAP[basename], INDEX[basename]
    return (filename_map, index)


def query_image(filename):
    filename = str(INPUT_ROOT / filename)
    filename_map, index = get_index(filename)

    image = preprocess_image(filename)
    results = index.get_nns_by_vector(image, 1)
    results = [filename_map[identifier] for identifier in results]
    assert len(results) == 1
    result = results[0]
    return result


def query_images():
    filenames = INPUT_IMAGE_FILENAMES
    filenames = tqdm(filenames, desc='Querying with full-scale images')
    with Pool(None) as pool:
        results = zip(filenames, pool.imap(query_image, filenames))
        for query_filename, result_filename in results:
            yield (query_filename, result_filename)


def disambiguate_pages(args):
    downscaled_image_filename, image_filenames = args
    book_id, page = image_filename_to_book(downscaled_image_filename)
    downscaled_image = preprocess_image(str(DOWNSCALED_INPUT_ROOT / downscaled_image_filename))
    nearest_image_filename = None
    min_distance = float('inf')
    for image_filename in image_filenames:
        image = preprocess_image(str(INPUT_ROOT / image_filename))
        distance = hamming(downscaled_image, image)
        if distance < min_distance:
            min_distance = distance
            nearest_image_filename = image_filename
    return (book_id, page, nearest_image_filename)


def link_pages():
    logger = logging.getLogger('link_pages')

    num_query_images = 0
    num_linked_images = 0
    downscaled_images = dict()
    linked_pages = dict()
    ambiguous_pages = []
    for image_filename, downscaled_image_filename in query_images():
        num_query_images += 1
        book_id, page = image_filename_to_book(downscaled_image_filename)
        if book_id not in downscaled_images:
            downscaled_images[book_id] = dict()
        downscaled_images[book_id][page] = downscaled_image_filename

        if book_id not in linked_pages:
            linked_pages[book_id] = dict()
        if page not in linked_pages[book_id]:
            linked_pages[book_id][page] = image_filename
            num_linked_images += 1
        else:
            if isinstance(linked_pages[book_id][page], str):
                linked_pages[book_id][page] = [linked_pages[book_id][page]]
                ambiguous_pages.append((book_id, page))
            linked_pages[book_id][page].append(image_filename)

    ambiguous_pages = tqdm(ambiguous_pages, desc='Resolving ambiguities')
    ambiguous_pages = (
        (downscaled_images[book_id][page], linked_pages[book_id][page])
        for book_id, page
        in ambiguous_pages
    )
    with Pool(None) as pool:
        disambiguated_pages = pool.imap(disambiguate_pages, ambiguous_pages)
        for book_id, page, image_filename in disambiguated_pages:
            linked_pages[book_id][page] = image_filename

    page_number_limits = get_page_number_limits()
    linked_book_ids = set()
    for book_id, pages in linked_pages.items():
        lower_limit, upper_limit = page_number_limits[book_id]
        assert len(pages) <= upper_limit
        if len(pages) >= lower_limit:
            linked_book_ids.add(book_id)

    logger.info(
        'Linked {} out of {} query images, completing {} out of {} books, spanning {}'.format(
            num_linked_images,
            num_query_images,
            len(linked_book_ids),
            len(INPUT_JSON_DUMP),
            len(linked_pages)
        )
    )

    return (linked_pages, linked_book_ids, downscaled_images)


def main():
    linked_pages, linked_book_ids, downscaled_images = link_pages()
    with OUTPUT_FILENAME.open('wt') as f:
        for book_id, pages in linked_pages.items():
            for page, input_image_filename in pages.items():
                downscaled_input_image_filename = downscaled_images[book_id][page]
                if book_id in linked_book_ids:
                    # when a book is fully linked
                    line = '{}\t=>\t{}'.format(downscaled_input_image_filename, input_image_filename)
                else:
                    # when a book is only partly linked
                    line = '{}\t->\t{}'.format(downscaled_input_image_filename, input_image_filename)
            print(line, file=f)


if __name__ == '__main__':
    main()
