# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from pathlib import Path
from sys import stdin


PREFERRED_SUFFIXES = ('.tif', '.png', '.jpg')


def main():
    filenames = dict()
    for filename in stdin:
        filename = Path(filename.rstrip('\r\n'))
        new_suffix = filename.suffix.lower()
        basename = str(filename)[:-len(new_suffix)]
        if new_suffix not in PREFERRED_SUFFIXES:
            continue
        if basename in filenames:
            old_suffix = filenames[basename]
            if PREFERRED_SUFFIXES.index(new_suffix) < PREFERRED_SUFFIXES.index(old_suffix):
                filenames[basename] = new_suffix
        else:
            filenames[basename] = new_suffix

    for basename, suffix in filenames.items():
        filename = '{}{}'.format(basename, suffix)
        print(filename)


if __name__ == '__main__':
    main()
