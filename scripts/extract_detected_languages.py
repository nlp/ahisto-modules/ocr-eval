# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import sys

from tqdm import tqdm

from .common import read_page_languages


INPUT_FILENAMES = Path(sys.argv[1])
INPUT_OCR_ROOT = Path(sys.argv[2])
DETECTED_LANGUAGES = sys.argv[3].split('+')
OUTPUT_FILENAME = Path(sys.argv[4])
THRESHOLD = float(sys.argv[5]) / 100.0


def get_languages_worker(filename):
    basename = INPUT_OCR_ROOT / filename.stem
    try:
        languages = read_page_languages(str(basename), DETECTED_LANGUAGES, algorithm='OLDA')
    except IOError:
        basename = INPUT_OCR_ROOT / filename.parent / filename.stem
        try:
            languages = read_page_languages(str(basename), DETECTED_LANGUAGES, algorithm='OLDA')
        except IOError:
            return 'not-exists'
    return (filename, languages)


def get_languages():
    logger = logging.getLogger('get_languages')

    num_successful = 0
    num_nonexistent = 0

    filenames = INPUT_FILENAMES.open('rt')
    filenames = map(lambda s: s.rstrip('\r\n'), filenames)
    filenames = map(Path, filenames)
    filenames = list(filenames)
    with Pool(None) as pool:
        languages = pool.imap(get_languages_worker, filenames)
        languages = tqdm(languages, desc='Reading detected page languages', total=len(filenames))
        for result in languages:
            if result == 'not-exists':
                num_nonexistent += 1
                continue
            filename, languages = result
            yield (filename, languages)
            num_successful += 1

    logger.info(
        'Read {} detected page languages, not found {}'.format(
            num_successful,
            num_nonexistent,
        )
    )


def extract():
    logger = logging.getLogger('extract')

    num_nonempty_pages = 0
    num_empty_pages = 0
    with OUTPUT_FILENAME.open('wt') as f:
        for filename, languages in get_languages():
            filename = str(filename)
            languages = sorted(languages.items(), key=lambda x: DETECTED_LANGUAGES.index(x[0]))
            languages = list(filter(lambda x: x[1] >= THRESHOLD, languages))
            if languages:
                languages = '+'.join(language for language, _ in languages)
                num_nonempty_pages += 1
            else:
                languages = 'None'
                num_empty_pages += 1
            print(filename, file=f)
            print(languages, file=f)

    logger.info(
        'Extracted languages for {} pages, skipped {} pages with no detected languages'.format(
            num_nonempty_pages,
            num_empty_pages,
        )
    )


if __name__ == '__main__':
    extract()
