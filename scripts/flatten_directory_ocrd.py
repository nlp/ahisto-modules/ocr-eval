# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import shutil
import sys

from tqdm import tqdm

from .common import read_facts


INPUT_ROOT = Path(sys.argv[1])
OUTPUT_ROOT = Path(sys.argv[2])
INPUT_UPSCALED_FILENAMES = list(read_facts(sys.argv[3]))


def copy(args):
    output_filename, input_filename = args
    input_filename = INPUT_ROOT / input_filename
    output_filename = OUTPUT_ROOT / output_filename
    output_filename.parent.mkdir(exist_ok=True)
    shutil.copy(str(input_filename), str(output_filename))


def main():
    OUTPUT_ROOT.mkdir(exist_ok=True)
    filenames = INPUT_UPSCALED_FILENAMES
    filenames = tqdm(filenames, desc='Flattening directory {} to {}'.format(INPUT_ROOT, OUTPUT_ROOT))
    with Pool(None) as pool:
        for _ in pool.imap_unordered(copy, filenames):
            pass


if __name__ == '__main__':
    main()
