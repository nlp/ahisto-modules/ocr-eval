# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


import sys

from .common import dump_all_ocr_texts


SQL_PARAMETERS = {
    'user': sys.argv[1],
    'password': sys.argv[2],
    'db': sys.argv[3],
}
OUTPUT_FILENAME = sys.argv[4]


def main():
    dump_all_ocr_texts(SQL_PARAMETERS, OUTPUT_FILENAME)


if __name__ == '__main__':
    main()
