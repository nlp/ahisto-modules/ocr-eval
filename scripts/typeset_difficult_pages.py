# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


import json
import os
from pathlib import Path
import shutil
from string import punctuation
import sys

from tqdm import tqdm

from .common import read_facts, image_filename_to_book, get_page_url


SQL_PARAMETERS = {
    'user': sys.argv[1],
    'password': sys.argv[2],
    'db': sys.argv[3],
}
INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES = sys.argv[4]
INPUT_MOST_DIFFICULT_PAGES_FILENAME = Path(sys.argv[5])
INPUT_OCR_ROOT = {
    'relevant_pages': Path(sys.argv[6]),
    'irrelevant_pages': Path(sys.argv[7]),
}
INPUT_OCR_ROOT_FALLBACK = INPUT_OCR_ROOT['relevant_pages']
INPUT_IMAGE_ROOT = Path(sys.argv[8])
OUTPUT_ROOT = Path(sys.argv[9])
CATEGORY_MAPPING = {
    'relevant_pages': 'Hussitica, {} stránek',
    'irrelevant_pages': 'Mimo Hussicitu, {} stránek',
}

INPUT_UPSCALED_HIGH_CONFIDENCE = list(read_facts(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES))
with INPUT_MOST_DIFFICULT_PAGES_FILENAME.open('rt') as f:
    INPUT_MOST_DIFFICULT_PAGES = json.load(f)


def get_fenced_code_separator(text):
    separator_length = 3
    while ('`' * separator_length) in text:
        separator_length += 1
    return '`' * separator_length


def is_punctuation(character):
    return character in punctuation


def is_cyrillic(ocr_text, book_id, max_punctuation_ratio=0.1):
    punctuation_ratio = 1.0 * len(list(filter(is_punctuation, ocr_text))) / len(ocr_text)
    is_cyrillic = punctuation_ratio > max_punctuation_ratio or book_id == 1178
    return is_cyrillic


def main(export_images=False):
    logger = logging.getLogger('main')

    OUTPUT_ROOT.mkdir(exist_ok=True)

    categories_and_input_ocr_roots = dict()
    for category, books_pages in INPUT_MOST_DIFFICULT_PAGES.items():
        input_ocr_root = INPUT_OCR_ROOT[category]
        category = CATEGORY_MAPPING[category].format(len(books_pages))
        (OUTPUT_ROOT / category).mkdir(exist_ok=True)
        for book_page in books_pages:
            book_id, page = book_page['book_id'], book_page['page']
            assert (book_id, page) not in categories_and_input_ocr_roots
            categories_and_input_ocr_roots[book_id, page] = (category, input_ocr_root)

    filenames_categories_and_input_ocr_roots = []
    num_fallbacks = 0
    for downscaled_input_filename, input_filename in INPUT_UPSCALED_HIGH_CONFIDENCE:
        book_id, page = image_filename_to_book(downscaled_input_filename)
        if (book_id, page) not in categories_and_input_ocr_roots:
            continue
        category, input_ocr_root = categories_and_input_ocr_roots[book_id, page]
        ocr_text = (input_ocr_root / input_filename).with_suffix('.txt').open('rt').read()
        if is_cyrillic(ocr_text, book_id):
            if input_ocr_root != INPUT_OCR_ROOT_FALLBACK:
                num_fallbacks += 1
                input_ocr_root = INPUT_OCR_ROOT_FALLBACK
        filenames_categories_and_input_ocr_roots.append(
            (downscaled_input_filename, input_filename, category, input_ocr_root),
        )
    filenames_categories_and_input_ocr_roots = tqdm(
        filenames_categories_and_input_ocr_roots,
        'Typesetting difficult pages',
    )

    for result in filenames_categories_and_input_ocr_roots:
        downscaled_input_filename, input_filename, category, input_ocr_root = result
        book_id, page = image_filename_to_book(downscaled_input_filename)

        if export_images:
            input_image_filename = INPUT_IMAGE_ROOT / input_filename
            output_image_filename = '{}-{}{}'.format(book_id, page, input_image_filename.suffixes[-1])
            output_image_filename = OUTPUT_ROOT / category / output_image_filename
            shutil.copy(str(input_image_filename), str(output_image_filename))

        ocr_text = (input_ocr_root / input_filename).with_suffix('.txt').open('rt').read()
        fenced_code_separator = get_fenced_code_separator(ocr_text)
        page_url = get_page_url(book_id, page, SQL_PARAMETERS)
        output_markdown_filename = '{}-{}.md'.format(book_id, page)
        output_markdown_filename = OUTPUT_ROOT / category / output_markdown_filename
        with output_markdown_filename.open('wt') as f:
            print('# [Kniha {}, strana {}]({})'.format(book_id, page, page_url), file=f)
            print('*Instrukce:*\n', file=f)
            print(
                '#. *Obrázek stránky najdete [pod tímto odkazem]({}) na portálu CMS Online.*'.format(page_url),
                file=f,
            )
            print(
                '#. *Rozpoznaný text stránky k opravě najdete pod těmito instrukcemi.*',
                file=f,
            )
            print(
                '#. *Záhlaví stránky (typicky obsahuje název kapitoly a/nebo číslo strany) by se mělo '
                'nacházet na začátku rozpoznaného textu.*',
                file=f,
            )
            print(
                '#. *Pokud strana obsahuje více sloupců textu, rozpoznaný text by měl odpovídat směru '
                'čtení: po sloupcích zleva doprava a v každém sloupci po řádcích shora dolů.*',
                file=f,
            )
            print(
                '#. *Poznámkový aparát a zápatí stránky (typicky obsahuje číslo strany) by se mělo '
                'nacházet na konci rozpoznaného textu.*',
                file=f,
            )
            print(
                '#. *Po dokončení oprav přejmenujte dokument z '
                '`{}` na `{} HOTOVO`.*'.format(
                    output_markdown_filename.stem,
                    output_markdown_filename.stem,
                ),
                file=f,
            )
            print('\n***', file=f)
            print('{}\n{}\n{}'.format(fenced_code_separator, ocr_text, fenced_code_separator), file=f)

        output_docx_filename = output_markdown_filename.with_suffix('.docx')
        command = 'pandoc -f markdown -t docx -i "{}" -o "{}"'
        command = command.format(output_markdown_filename, output_docx_filename)
        os.system(command)
        output_markdown_filename.unlink()

    logger.info(
        'Fell back on input OCR root {} for {} pages containing Cyrillic'.format(
            INPUT_OCR_ROOT_FALLBACK,
            num_fallbacks,
        )
    )


if __name__ == '__main__':
    main()
