# -*- coding:utf-8 -*-

from collections import defaultdict
import csv
from itertools import repeat, chain
import json
import logging
from math import isnan
from multiprocessing import Pool
from pathlib import Path
import re
from statistics import median
import warnings

from edit_distance import SequenceMatcher
import cv2 as cv
from lxml import etree
import numpy as np
import pycountry

from .configuration import CSV_PARAMETERS, SQL_PARAMETERS, JSON_PARAMETERS, PREPROCESSED_IMAGE_WIDTH, PREPROCESSED_IMAGE_HEIGHT, ANNOY_N_TREES


DATABASE = None
BBOX_REGEX = re.compile(r'.*(^|;)\s*bbox\s+(?P<x0>[0-9]+)\s+(?P<y0>[0-9]+)\s+(?P<x1>[0-9]+)\s+(?P<y1>[0-9]+)\s*($|;)')
URL_REGEX = re.compile(r'https://sources.cms.flu.cas.cz/.*&bookid=(?P<book_id>[0-9]+).*')
IMAGE_FILENAME_REGEX = re.compile(r'\./(?P<book_id>[0-9]+)(_delete)?/(?P<page>[0-9]+)\.(jpg|png|tif)')
RELEVANT_PAGE_ANNOTATION_REGEX = re.compile(r'\* *(?P<pages>([0-9]+(-[0-9]+)?(; *)?)+)')
PAGE_URL = 'https://sources.cms.flu.cas.cz/src/index.php?s=v&cat={category}&bookid={book_id}&page={page}'


def read_relevant_page_csv(filename):
    logger = logging.getLogger('read_relevant_page_csv')

    successfully_read_books = 0
    successfully_read_pages = 0
    skipped = 0
    with open(filename, 'rt') as f:
        reader = csv.reader(f, **CSV_PARAMETERS)
        next(reader)
        for line_number, row in enumerate(reader):
            if not ''.join(row):
                warning = 'Line {} in CSV file {} is empty, skipping'.format(
                    line_number,
                    filename,
                )
                logger.warning(warning)
                skipped += 1
                continue
            line_number += 2
            url = row[14].strip()
            url_match = re.fullmatch(URL_REGEX, url)
            if url_match is None:
                if not url:
                    warning = 'Line {} in CSV file {} contains an empty URL, skipping'.format(
                        line_number,
                        filename,
                    )
                    logger.warning(warning)
                    skipped += 1
                    continue
                else:
                    error = 'Line {} in CSV file {} contains a malformed URL "{}"'.format(
                        line_number,
                        filename,
                        url,
                    )
                    raise ValueError(error)
            book_id = int(url_match.group('book_id'))
            annotation = row[9].strip().lower()
            if not annotation:
                warning = 'Line {} in CSV file {} contains an empty annotation, skipping'.format(
                    line_number,
                    filename,
                )
                logger.warning(warning)
                skipped += 1
                continue
            if annotation.startswith('x') or annotation.startswith('-'):
                continue
            annotation_match = re.fullmatch(RELEVANT_PAGE_ANNOTATION_REGEX, annotation)
            if annotation_match is None:
                error = 'Line {} in CSV file {} contains a malformed annotation "{}"'.format(
                    line_number,
                    filename,
                    annotation,
                )
                raise ValueError(error)
            pages = annotation_match.group('pages')
            pages = filter(lambda s: s, re.split(r'; *', pages))  # remove empty sections
            pages = (  # normalize to page range specifications
                page.split('-') if '-' in page else (page, page)
                for page in pages
            )
            pages = (  # normalize to page ranges
                list(range(int(start_page), int(end_page) + 1))
                for start_page, end_page in pages
            )
            pages = list(chain(*pages))
            successfully_read_books += 1
            successfully_read_pages += len(pages)
            yield(book_id, pages)

    info = 'Read {} books with {} relevant pages from CSV file {}, skipped {}'.format(
        successfully_read_books,
        successfully_read_pages,
        filename,
        skipped,
    )
    logger.info(info)


def read_book_csv(filename):
    logger = logging.getLogger('read_book_csv')

    successfully_read = 0
    skipped = 0
    with open(filename, 'rt') as f:
        reader = csv.reader(f, **CSV_PARAMETERS)
        next(reader)
        for line_number, row in enumerate(reader):
            line_number += 2
            signature = row[4]
            directory = row[5]
            url = row[-1]
            url_match = re.fullmatch(URL_REGEX, url)
            if url_match is None:
                if not url:
                    warning = 'Line {} in CSV file {} contains an empty URL, skipping'.format(
                        line_number,
                        filename,
                    )
                    logger.warning(warning)
                    skipped += 1
                    continue
                else:
                    error = 'Line {} in CSV file {} contains a malformed URL "{}"'.format(
                        line_number,
                        filename,
                        url,
                    )
                    raise ValueError(error)
            book_id = int(url_match.group('book_id'))
            successfully_read += 1
            yield (book_id, {'signature': signature, 'directory': directory})

    info = 'Read {} filenames from CSV file {}, skipped {}'.format(
        successfully_read,
        filename,
        skipped,
    )
    logger.info(info)


def _connect_sql(sql_parameters):
    global DATABASE
    if DATABASE is None:
        from MySQLdb import connect  # type: ignore
        DATABASE = connect(**{**sql_parameters, **SQL_PARAMETERS})
    return DATABASE


def read_sql(sql_parameters):
    logger = logging.getLogger('read_sql')

    database = _connect_sql(sql_parameters)
    cursor = database.cursor()
    cursor.execute('''
        SELECT kniha_id, COUNT(stranka_cislo) AS pocet_stran
        FROM OCR_stranka
        GROUP BY kniha_id
        ORDER BY kniha_id
    ''')

    successfully_read = 0
    row_numbers = range(cursor.rowcount)
    for row_number in row_numbers:
        row = cursor.fetchone()
        book_id = int(row[0])
        number_of_pages = int(row[1])
        if number_of_pages < 1:
            warning = 'Book with ID {} has {} pages'.format(
                book_id,
                number_of_pages,
            )
            logger.warn(warning)
        successfully_read += 1
        yield (book_id, {'number_of_pages': number_of_pages})

    info = 'Read {} records from SQL database'.format(
        successfully_read,
    )
    logger.info(info)


def get_all_ocr_texts(sql_parameters):
    database = _connect_sql(sql_parameters)
    cursor = database.cursor()
    cursor.execute('''
        SELECT kniha_id, stranka_cislo, obsah
        FROM OCR_stranka
    ''')
    row_numbers = range(cursor.rowcount)
    for row_number in row_numbers:
        row = cursor.fetchone()
        book_id = int(row[0])
        page_number = int(row[1])
        text = row[2]
        yield (book_id, page_number, text)


def get_page_numbers(book_id, sql_parameters):
    database = _connect_sql(sql_parameters)
    cursor = database.cursor()
    cursor.execute('''
        SELECT stranka_cislo
        FROM OCR_stranka
        WHERE kniha_id = %s
        ORDER BY stranka_cislo ASC
    ''', (book_id,))

    row_numbers = range(cursor.rowcount)
    for row_number in row_numbers:
        row = cursor.fetchone()
        page_number = int(row[0])
        yield page_number


def get_page_offsets(book_id, sql_parameters):
    database = _connect_sql(sql_parameters)
    cursor = database.cursor()
    cursor.execute('''
        SELECT stranky_titulu, stranky_predmluvy, stranky_edice_od
        FROM knihy
        WHERE id = %s
    ''', (book_id,))

    assert cursor.rowcount <= 1
    if cursor.rowcount == 0:
        return None
    row = cursor.fetchone()
    title_offset = int(row[0])
    preface_offset = int(row[1])
    edition_starting_page = int(row[2])
    offset = title_offset + preface_offset
    return (offset, edition_starting_page)


def get_book_category(book_id, sql_parameters):
    database = _connect_sql(sql_parameters)
    cursor = database.cursor()
    cursor.execute('''
        SELECT cat
        FROM knihy
        WHERE id = %s
    ''', (book_id,))

    assert cursor.rowcount == 1
    row = cursor.fetchone()
    category = int(row[0])
    return category


def get_page_url(book_id, page, sql_parameters):
    category = get_book_category(book_id, sql_parameters)
    url = PAGE_URL.format(book_id=book_id, page=page, category=category)
    return url


def dump_relevant_pages(sql_parameters, csv_filename, output_filename):
    logger = logging.getLogger('dump_relevant_pages')

    books = dict()
    num_books = 0
    num_books_skipped = 0
    num_pages = 0

    for book_id, pages in read_relevant_page_csv(csv_filename):
        page_offsets = get_page_offsets(book_id, sql_parameters)
        if page_offsets is None:
            num_books_skipped += 1
            continue
        offset, edition_starting_page = page_offsets
        pages = set(pages)
        for page in get_page_numbers(book_id, sql_parameters):
            if page - offset <= 0:  # a page from title or preface
                continue
            offset_page = page - offset + edition_starting_page
            if offset_page in pages:
                if book_id not in books:
                    books[book_id] = list()
                    num_books += 1
                books[book_id].append(page)
                num_pages += 1

    with open(output_filename, 'wt') as f:
        json.dump(books, f, **JSON_PARAMETERS)

    info = 'Dumped {} books with {} relevant pages into JSON file {}, skipping {} books'.format(
        num_books,
        num_pages,
        output_filename,
        num_books_skipped,
    )
    logger.info(info)


def dump_all_ocr_texts(sql_parameters, output_filename):
    logger = logging.getLogger('dump_all_ocr_texts')

    texts = dict()
    num_pages = 0
    for book_id, page_number, text in get_all_ocr_texts(sql_parameters):
        if book_id not in texts:
            texts[book_id] = dict()
        texts[book_id][page_number] = text
        num_pages += 1
    with open(output_filename, 'wt') as f:
        json.dump(texts, f, **JSON_PARAMETERS)

    logger.info(
        'Dumped OCR texts in {} pages of {} books into JSON file {}'.format(
            num_pages,
            len(texts),
            output_filename,
        )
    )


def load_all_ocr_texts(filename):
    logger = logging.getLogger('load_all_ocr_texts')

    with open(filename, 'rt') as f:
        dump = json.load(f).items()
    texts = dict()
    num_pages = 0
    for book_id, pages in dump:
        book_id = int(book_id)
        texts[book_id] = dict()
        for page_number, text in pages.items():
            page_number = int(page_number)
            texts[book_id][page_number] = text
            num_pages += 1

    logger.info(
        'Read OCR texts in {} pages of {} books from JSON file {}'.format(
            num_pages,
            len(texts),
            filename,
        )
    )

    return texts


def get_ocr_texts_worker(args):
    database, (book_id, pages) = args
    cursor = database.cursor()
    cursor.execute('''
        SELECT stranka_cislo, obsah
        FROM OCR_stranka
        WHERE kniha_id = %s
    ''', (book_id,))
    row_numbers = range(cursor.rowcount)
    ocr_texts = []
    for row_number in row_numbers:
        row = cursor.fetchone()
        page_number = int(row[0])
        assert page_number in pages, \
            'Requested book {}, but page {} has no corresponding image filename'.format(
                book_id,
                page_number,
            )
        ocr_text = row[1]
        filename = pages[page_number]
        ocr_texts.append((filename, ocr_text))
    return ocr_texts


def get_ocr_texts(books, sql_parameters):
    database = _connect_sql(sql_parameters)
    args = zip(repeat(database), books.items())
    ocr_text_batches = map(get_ocr_texts_worker, args)
    for ocr_texts in ocr_text_batches:
        for filename, ocr_text in ocr_texts:
            yield (filename, ocr_text)


def image_filename_to_book(filename):
    filename_match = re.fullmatch(IMAGE_FILENAME_REGEX, filename)
    assert filename_match is not None
    book_id = int(filename_match.group('book_id'))
    page = int(filename_match.group('page'))
    return book_id, page


def book_to_url(book_id, page, url_type='portal'):
    assert url_type in ('portal', 'image')
    if url_type == 'portal':
        return f'https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book={book_id}&page={page}'
    elif url_type == 'image':
        return f'https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/{book_id}/{page}.jpg'


def read_filenames(filename):
    logger = logging.getLogger('read_filenames')

    successfully_read = 0
    with open(filename, 'rt') as f:
        for line in f:
            line = line.rstrip('\r\n')
            successfully_read += 1
            yield line
    info = 'Read {} filenames from text file {}'.format(
        successfully_read,
        filename,
    )
    logger.info(info)


def read_facts(filename, separator=r'=>'):
    logger = logging.getLogger('read_facts')

    separator = r'\t{}\t'.format(separator)
    separator = re.compile(separator)

    num_successful = 0
    num_skipped = 0

    with open(filename, 'rt') as f:
        for line in f:
            line = line.rstrip('\r\n')
            if not re.search(separator, line):
                num_skipped += 1
                continue
            lhs, rhs = re.split(separator, line)
            num_successful += 1
            yield (lhs, rhs)

    info = 'Read {} facts from text file {}, skipping {}'.format(
        num_successful,
        filename,
        num_skipped,
    )
    logger.info(info)


def read_book_ids(root_dirname, filename):
    logger = logging.getLogger('read_book_ids')

    book_id_map = dict()
    all_book_ids = set()

    for book_id, information in read_book_csv(filename):
        basename = information['directory']
        basename = deaccent(basename)
        dirnames = root_dirname.glob('{}_*/'.format(basename[:2]))
        dirnames = list(dirnames)
        assert len(dirnames) < 2
        if not dirnames:
            continue
        dirname, = dirnames
        basename = dirname.name
        if basename not in book_id_map:
            book_id_map[basename] = set()
        book_id_map[basename].add(book_id)
        all_book_ids.add(book_id)
    info = 'Read {} basenames with {} book ids from CSV file {}'.format(
        len(book_id_map),
        len(all_book_ids),
        filename,
    )
    logger.info(info)

    return book_id_map, all_book_ids


def read_joblog(filename):
    logger = logging.getLogger('read_joblog')

    successfully_read = 0
    with open(filename, 'rt') as f:
        lines = iter(f)
        next(lines)
        for line in lines:
            line = line.rstrip('\r\n').split('\t')
            runtime = float(line[3])
            exit_code = int(line[6])
            command = line[-1]
            successfully_read += 1
            yield (command, runtime, exit_code)
    info = 'Read {} records from joblog {}'.format(
        successfully_read,
        filename
    )
    logger.info(info)


def preview(iterable, preview_size=5, separator=', '):
    preview = separator.join(
        [
            str(element)
            for element
            in sorted(iterable)
        ][:preview_size] + ['…']
    )
    return preview


def preprocess_image(filename, width=PREPROCESSED_IMAGE_WIDTH, height=PREPROCESSED_IMAGE_HEIGHT):
    image = cv.imread(filename, cv.IMREAD_GRAYSCALE)
    image = cv.resize(image, (width, height), interpolation=cv.INTER_LINEAR)
    _, image = cv.threshold(image, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
    return np.ravel(image)


def preprocess_images(input_root, filenames):
    effective_filenames = map(lambda x: input_root / x, filenames)
    effective_filenames = map(str, effective_filenames)
    images_dict = dict()
    with Pool(None) as pool:
        images = pool.imap(preprocess_image, effective_filenames)
        images = zip(filenames, images)
        for filename, image in images:
            images_dict[filename] = image
            yield (filename, image)


def index_images(input_root, filenames, width=PREPROCESSED_IMAGE_WIDTH, height=PREPROCESSED_IMAGE_HEIGHT):
    from annoy import AnnoyIndex
    index = AnnoyIndex(width * height, 'hamming')
    filename_map = dict()
    images = preprocess_images(input_root, filenames)
    images = enumerate(images)
    for identifier, (filename, image) in images:
        filename_map[identifier] = filename
        index.add_item(identifier, image)
    index.build(ANNOY_N_TREES)
    return (filename_map, index)


def deaccent(text):
    from gensim.utils import deaccent as _deaccent
    return _deaccent(text)


def tokenize(text):
    from gensim.utils import tokenize as _tokenize
    return list(_tokenize(text, lowercase=True, deacc=True))


def get_word_error_rate(first_text, second_text):
    first_sequence = tokenize(first_text)
    second_sequence = tokenize(second_text)
    word_error_rate = get_character_error_rate(first_sequence, second_sequence)
    return word_error_rate


def get_character_error_rate(first_sequence, second_sequence):
    distance = SequenceMatcher(a=first_sequence, b=second_sequence).distance()
    max_distance = max(len(first_sequence), len(second_sequence))
    character_error_rate = 100.0 * distance / max_distance
    return character_error_rate


def get_tfidf(texts):
    from gensim.corpora.dictionary import Dictionary
    from gensim.models.tfidfmodel import TfidfModel

    with Pool(None) as pool:
        texts = pool.imap_unordered(tokenize, texts)
        dictionary = Dictionary(texts, prune_at=None)
    tfidf = TfidfModel(dictionary=dictionary)
    return (dictionary, tfidf)


def get_cossims(dictionary, query, corpus):
    from gensim.similarities import SparseMatrixSimilarity

    index = SparseMatrixSimilarity(corpus, num_features=len(dictionary), num_docs=len(corpus))
    sims = index[query]
    return sims


def get_confidence_interval(sample, confidence):
    import scipy.stats as st

    mean = np.mean(sample)
    interval = st.t.interval(
        confidence / 100.0,
        len(sample) - 1,
        loc=mean,
        scale=st.sem(sample),
    )
    return (mean, interval)


def print_confidence_interval(file, sample, name=None, unit=None, confidence=95.0):
    mean, interval = get_confidence_interval(sample, confidence)
    if name:
        name = ' {}{} '.format(name[0].upper(), name[1:])
    else:
        name = ' '
    if not unit:
        unit = ''
    print(
        'Mean{}point estimate: {:.2f}{}'.format(name, mean, unit),
        file=file,
    )
    print(
        'Mean{}{:.2f}% CI estimate: [{:.2f}{}; {:.2f}{}]'.format(
            name,
            confidence,
            interval[0],
            unit,
            interval[1],
            unit,
        ),
        file=file,
    )
    print(file=file)


def is_multicolumn(filename, algorithm='computational_geometry'):
    get_number_of_columns = {
        'machine_learning': _get_number_of_columns_machine_learning,
        'computational_geometry': _get_number_of_columns_computational_geometry,
    }
    with filename.open('rt') as f:
        content = f.read()
        if not content.strip():
            raise IOError(f'File {filename} is empty')
        html5_parser = etree.HTMLParser(huge_tree=True)
        xml_document = etree.fromstring(content.encode('utf-8'), html5_parser)
    return get_number_of_columns[algorithm](xml_document) >= 2


def get_element_text(element):
    return ''.join(element.itertext()).strip()


def _get_number_of_columns_computational_geometry(xml_document, num_beams=5):
    page, = xml_document.xpath('//div[@class="ocr_page"]')
    page_match = re.match(BBOX_REGEX, page.attrib['title'])
    assert page_match is not None
    page_height = float(page_match.group('y1')) - float(page_match.group('y0'))

    from shapely.geometry import Polygon, LineString

    paragraphs = []
    for paragraph_number, paragraph in enumerate(page.xpath('//p[@class="ocr_par" and @title]')):
        if not get_element_text(paragraph):
            continue
        paragraph_match = re.match(BBOX_REGEX, paragraph.attrib['title'])
        assert paragraph_match is not None
        coords = [
            (float(paragraph_match.group('x0')), float(paragraph_match.group('y0'))),
            (float(paragraph_match.group('x0')), float(paragraph_match.group('y1'))),
            (float(paragraph_match.group('x1')), float(paragraph_match.group('y1'))),
            (float(paragraph_match.group('x1')), float(paragraph_match.group('y0'))),
        ]
        polygon = Polygon(coords)
        paragraphs.append(polygon)

    beams_ys = ((beam_number + 1.0) / (num_beams + 1.0) for beam_number in range(num_beams))
    beams_coords = (
        [
            (float(page_match.group('x0')), float(page_match.group('y0')) + y * page_height),
            (float(page_match.group('x1')), float(page_match.group('y0')) + y * page_height),
        ]
        for y
        in beams_ys
    )
    beams = map(LineString, beams_coords)
    intersections = (
        (
            paragraph.intersection(beam)
            for paragraph
            in paragraphs
        )
        for beam
        in beams
    )

    def magnitude(intersection):
        return sum(0 if obj.is_empty else 1 for obj in intersection)

    number_of_columns = int(round(median(map(magnitude, intersections))))
    return number_of_columns


def _get_number_of_columns_machine_learning(xml_document, ks=range(2, 10)):
    from sklearn.svm import OneClassSVM
    from sklearn.cluster import KMeans
    from sklearn.metrics import silhouette_score

    left_boundaries, right_boundaries = [], []
    for line in xml_document.xpath('//span[@class="ocr_line" and @title]'):
        match = re.match(BBOX_REGEX, line.attrib['title'])
        assert match is not None, line.attrib['title']
        left_boundary, right_boundary = float(match.group('x0')), float(match.group('x1'))
        left_boundaries.append(left_boundary)
        right_boundaries.append(right_boundary)

    boundaries = left_boundaries + right_boundaries
    num_unique_boundaries = len(set(boundaries))
    if num_unique_boundaries <= ks.start:
        return num_unique_boundaries - 1
    are_outliers = OneClassSVM().fit_predict(np.array(boundaries).reshape(-1, 1))
    boundaries = [
        boundary
        for boundary, is_outlier
        in zip(boundaries, are_outliers)
        if is_outlier == 1
    ]
    num_unique_boundaries = len(set(boundaries))
    if num_unique_boundaries <= ks.start:
        return num_unique_boundaries - 1
    X = np.array(boundaries).reshape(-1, 1)
    best_k, best_silhouette = 1, float('-inf')
    for k in ks:
        if k >= num_unique_boundaries:
            break
        y = KMeans(n_clusters=k).fit_predict(X)
        silhouette = silhouette_score(X, y)
        if silhouette > best_silhouette:
            best_k, best_silhouette = k, silhouette

    return best_k - 1


def normalize_language_code(language_code):
    try:
        language = pycountry.languages.lookup(language_code)
    except LookupError:
        return None
    return language.alpha_3


def normalize_language_codes(language_codes):
    normalized_language_codes = dict()
    for language_code, value in language_codes.items():
        language_code = normalize_language_code(language_code)
        if language_code is None:
            continue
        assert language_code not in normalized_language_codes
        normalized_language_codes[language_code] = value
    return normalized_language_codes


def limit_language_codes(language_codes, limit):
    limit = set(
        normalize_language_code(language_code)
        for language_code
        in limit
    )
    limit.discard(None)
    language_codes = {
        language_code: value
        for language_code, value
        in language_codes.items()
        if language_code in limit
    }
    return language_codes


def l1_normalize(dictionary):
    value_sum = sum(dictionary.values())
    if value_sum == 0.0:
        return dictionary
    return {
        key: float(value) / value_sum
        for key, value
        in dictionary.items()
        if value > 0.0
    }


def read_page_languages(basename, limit=None, **kwargs):
    readers = {
        '.hocr': _read_page_languages_hocr,
        '.json': _read_page_languages_json,
    }
    for suffix, reader in readers.items():
        try:
            f = Path('{}{}'.format(basename, suffix)).open('rt')
        except IOError:
            continue
        languages = reader(f, **kwargs)
        f.close()
        languages = normalize_language_codes(languages)
        if limit is not None:
            languages = limit_language_codes(languages, limit)
        languages = l1_normalize(languages)
        return languages
    raise IOError('Found no file with basename {} containing detected languages'.format(basename))


def _read_page_languages_json(f, **kwargs):
    try:
        document = json.load(f)
    except json.decoder.JSONDecodeError:
        raise IOError('Malformed JSON document {}'.format(f))
    responses = document['responses']
    assert len(responses) == 1
    pages = responses[0]['fullTextAnnotation']['pages']
    assert len(pages) == 1
    page = pages[0]
    if 'property' not in page:
        return dict()
    detected_languages = page['property']['detectedLanguages']
    languages = dict()
    for language in detected_languages:
        language_code = language['languageCode']
        assert language_code not in languages
        confidence = language['confidence']
        languages[language_code] = confidence
    return languages


def _read_page_languages_hocr(f, algorithm='NLDA', **kwargs):
    content = f.read()
    if not content.strip():  # the file is empty
        return dict()
    html5_parser = etree.HTMLParser(huge_tree=True)
    xml_document = etree.fromstring(content.encode('utf-8'), html5_parser)

    languages = defaultdict(lambda: 0.0)

    def get_confidence(element):
        return float(len(get_element_text(element)))

    assert algorithm in ('OLDA', 'NLDA', 'paragraph', 'word', 'annotated')
    if algorithm == 'annotated':
        # The annotated HOCR files produced by
        # https://gitlab.fi.muni.cz/nlp/ahisto-language-detection do not have
        # paragraph-level language annotations, so we will only consider
        # word-level annotations.
        for word in xml_document.xpath('//span[@class="ocrx_word" and @lang]'):
            word_language_code = word.attrib['lang']
            word_confidence = get_confidence(word)
            if not word_confidence:
                continue
            languages[word_language_code] += word_confidence
    else:
        for paragraph in xml_document.xpath('//p[@lang]'):
            paragraph_language_code = paragraph.attrib['lang']
            paragraph_confidence = get_confidence(paragraph)
            if algorithm == 'OLDA' or algorithm == 'paragraph':
                # With the old paragraph-only algorithm, we only take
                # paragraph-level language annotations into account.
                languages[paragraph_language_code] += paragraph_confidence
            elif algorithm == 'NLDA' or algorithm == 'word':
                # With the new paragraph-and-word algorithm, we take
                # both paragraph-level and word-level language annotations
                # into account.
                if not paragraph_confidence:
                    continue
                for word in paragraph.xpath('.//span[@class="ocrx_word" and @lang]'):
                    word_language_code = word.attrib['lang']
                    word_confidence = get_confidence(word)
                    if not word_confidence:
                        continue
                    languages[word_language_code] += word_confidence
                    paragraph_confidence -= word_confidence
                assert paragraph_confidence >= 0.0
                languages[paragraph_language_code] += paragraph_confidence
    return languages


def iterate_lines_hocr(f):
    html5_parser = etree.HTMLParser(huge_tree=True)
    xml_document = etree.parse(f, html5_parser)
    for paragraph in xml_document.xpath('//p[@lang]'):
        language_code = paragraph.attrib['lang']
        language_code = normalize_language_code(language_code)
        for line in paragraph.xpath('span[@class="ocr_line"]'):
            words = (word for word in line.xpath('span[@class="ocrx_word"]'))
            words = filter(lambda word: word.text is not None and word.text.strip(), words)
            words = tuple(word.text for word in words)
            if words:
                yield (language_code, words)


def get_max_key(dictionary):
    if not dictionary:
        return None
    max_key, _ = max(dictionary.items(), key=lambda item: item[1])
    return max_key


def get_accuracy_at_one(first_dict, second_dict):
    first_max_key = get_max_key(first_dict)
    second_max_key = get_max_key(second_dict)
    if first_max_key is None and second_max_key is None:
        return None
    accuracy_at_one = 100.0 * (first_max_key == second_max_key)
    return accuracy_at_one


def get_jaccard_index(first_dict, second_dict):
    union = 0.0
    for key in first_dict.keys() | second_dict.keys():
        first_value = first_dict[key] if key in first_dict else 0.0
        second_value = second_dict[key] if key in second_dict else 0.0
        union += max(first_value, second_value)
    if union == 0.0:
        return None
    intersection = 0.0
    for key in first_dict.keys() & second_dict.keys():
        first_value = first_dict[key]
        second_value = second_dict[key]
        intersection += min(first_value, second_value)
    jaccard_index = 100.0 * intersection / union
    return jaccard_index


def get_spearman_r(first_dict, second_dict):
    import scipy.stats as st

    first_values = []
    second_values = []
    for key in first_dict.keys() | second_dict.keys():
        first_value = first_dict[key] if key in first_dict else 0.0
        second_value = second_dict[key] if key in second_dict else 0.0
        first_values.append(first_value)
        second_values.append(second_value)
    if not first_values:  # or not second_values
        return None
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        spearman_r, _ = st.spearmanr(first_values, second_values)
    if isnan(spearman_r):
        return None
    return spearman_r


class Evaluation(object):
    def __init__(self, input_relevant_pages, output_most_difficult_pages_filename,
                 output_log_filename, min_len_correct_text=1000):
        self.input_relevant_pages = input_relevant_pages
        self.output_most_difficult_pages_filename = output_most_difficult_pages_filename
        self.output_log_filename = output_log_filename
        self.min_len_correct_text = min_len_correct_text
        self.worst_cer, self.worst_cer_book_id, self.worst_cer_page = float('-inf'), None, None
        self.worst_cer_correct_text, self.worst_cer_predicted_text = None, None
        self.best_cer, self.best_cer_book_id, self.best_cer_page = float('inf'), None, None
        self.best_cer_correct_text, self.best_cer_predicted_text = None, None
        self.worst_wer, self.worst_wer_book_id, self.worst_wer_page = float('-inf'), None, None
        self.worst_wer_correct_text, self.worst_wer_predicted_text = None, None
        self.best_wer, self.best_wer_book_id, self.best_wer_page = float('inf'), None, None
        self.best_wer_correct_text, self.best_wer_predicted_text = None, None
        self.character_error_rates = []
        self.word_error_rates = []
        self.page_error_rates = {}

    def update(self, *result):
        book_id, page, character_error_rate, word_error_rate, correct_text, predicted_text = result
        self.character_error_rates.append(character_error_rate)
        self.word_error_rates.append(word_error_rate)
        if len(correct_text) < self.min_len_correct_text:
            return
        self.page_error_rates[(book_id, page)] = (character_error_rate, word_error_rate)
        if character_error_rate > self.worst_cer:
            self.worst_cer, self.worst_cer_book_id, self.worst_cer_page = \
                character_error_rate, book_id, page
            self.worst_cer_correct_text, self.worst_cer_predicted_text = \
                correct_text, predicted_text
        if character_error_rate < self.best_cer:
            self.best_cer, self.best_cer_book_id, self.best_cer_page = \
                character_error_rate, book_id, page
            self.best_cer_correct_text, self.best_cer_predicted_text = \
                correct_text, predicted_text
        if word_error_rate > self.worst_wer:
            self.worst_wer, self.worst_wer_book_id, self.worst_wer_page = \
                word_error_rate, book_id, page
            self.worst_wer_correct_text, self.worst_wer_predicted_text = \
                correct_text, predicted_text
        if word_error_rate < self.best_wer:
            self.best_wer, self.best_wer_book_id, self.best_wer_page = \
                word_error_rate, book_id, page
            self.best_wer_correct_text, self.best_wer_predicted_text = \
                correct_text, predicted_text

    def finish(self):
        self.dump_most_difficult_pages()
        self.log_results()

    def dump_most_difficult_pages(self, top_relevant=120, top_irrelevant=30):
        logger = logging.getLogger('dump_most_difficult_pages')

        def is_relevant(args):
            _, book_id, page = args
            return book_id in self.input_relevant_pages and page in self.input_relevant_pages[book_id]

        def is_irrelevant(args):
            return not is_relevant(args)

        def limit_pages_per_book(pages, max_pages_per_book=10):
            num_pages = dict()
            for page in pages:
                book_id = page['book_id']
                if book_id not in num_pages:
                    num_pages[book_id] = 0
                num_pages[book_id] += 1
                if num_pages[book_id] <= max_pages_per_book:
                    yield page

        page_word_error_rates = [
            (word_error_rate, book_id, page)
            for (book_id, page), (_, word_error_rate)
            in self.page_error_rates.items()
        ]
        page_word_error_rates = sorted(page_word_error_rates, reverse=True)
        relevant_pages = list(limit_pages_per_book(
            {'book_id': book_id, 'page': page, 'word_error_rate': word_error_rate}
            for word_error_rate, book_id, page
            in filter(is_relevant, page_word_error_rates)
        ))[:top_relevant]
        irrelevant_pages = list(limit_pages_per_book(
            {'book_id': book_id, 'page': page, 'word_error_rate': word_error_rate}
            for word_error_rate, book_id, page
            in filter(is_irrelevant, page_word_error_rates)
        ))[:top_irrelevant]

        if len(relevant_pages) < top_relevant:
            logger.warn(
                'Requested {} relevant most difficult pages, but found only {}'.format(
                    top_relevant,
                    len(relevant_pages),
                )
            )
        if len(irrelevant_pages) < top_irrelevant:
            logger.warn(
                'Requested {} irrelevant most difficult pages, but found only {}'.format(
                    top_irrelevant,
                    len(irrelevant_pages),
                )
            )

        most_difficult_pages = {'relevant_pages': relevant_pages, 'irrelevant_pages': irrelevant_pages}
        with self.output_most_difficult_pages_filename.open('wt') as f:
            json.dump(most_difficult_pages, f, **JSON_PARAMETERS)

        total_num_relevant_pages = sum(len(pages) for book_id, pages in self.input_relevant_pages.items())
        considered_relevant_pages = len(list(filter(is_relevant, page_word_error_rates)))
        skipped_relevant_pages = total_num_relevant_pages - considered_relevant_pages
        logger.info(
            'Considered {} out of {} relevant pages as the most difficult, skipping {}'.format(
                considered_relevant_pages,
                total_num_relevant_pages,
                skipped_relevant_pages,
            )
        )
        logger.info(
            'Dumped {} relevant and {} irrelevant most difficult pages to JSON file {}'.format(
                len(relevant_pages),
                len(irrelevant_pages),
                self.output_most_difficult_pages_filename,
            )
        )

    def log_results(self):
        with self.output_log_filename.open('wt') as f:
            print_confidence_interval(f, self.character_error_rates, 'CER', '%')
            print(
                'Worst CER: {:.2f}%, book id {}, page {}:'.format(
                    self.worst_cer,
                    self.worst_cer_book_id,
                    self.worst_cer_page,
                ),
                file=f,
            )
            print(
                '\nCorrect text:\n\n{}\n\nPredicted text:\n\n{}'.format(
                    self.worst_cer_correct_text,
                    self.worst_cer_predicted_text,
                ),
                file=f,
            )
            print(file=f)
            print(
                'Best CER: {:.2f}%, book id {}, page {}:'.format(
                    self.best_cer,
                    self.best_cer_book_id,
                    self.best_cer_page,
                ),
                file=f,
            )
            print(
                '\nCorrect text:\n\n{}\n\nPredicted text:\n\n{}'.format(
                    self.best_cer_correct_text,
                    self.best_cer_predicted_text,
                ),
                file=f,
            )
            print(file=f)
            print(file=f)
            print_confidence_interval(f, self.word_error_rates, 'WER', '%')
            print(
                'Worst WER: {:.2f}%, book id {}, page {}:'.format(
                    self.worst_wer,
                    self.worst_wer_book_id,
                    self.worst_wer_page,
                ),
                file=f,
            )
            print(
                '\nCorrect text:\n\n{}\n\nPredicted text:\n\n{}'.format(
                    self.worst_wer_correct_text,
                    self.worst_wer_predicted_text,
                ),
                file=f,
            )
            print(file=f)
            print(
                'Best WER: {:.2f}%, book id {}, page {}:'.format(
                    self.best_wer,
                    self.best_wer_book_id,
                    self.best_wer_page,
                ),
                file=f,
            )
            print(
                '\nCorrect text:\n\n{}\n\nPredicted text:\n\n{}'.format(
                    self.best_wer_correct_text,
                    self.best_wer_predicted_text,
                ),
                file=f,
            )
            print(file=f)
            print(file=f)
            print(
                'Worst and best CER and WER are reported on texts of character length >= {}'.format(
                    self.min_len_correct_text,
                ),
                file=f,
            )
