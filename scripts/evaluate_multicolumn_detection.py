# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from collections import OrderedDict
from multiprocessing import Pool
from pathlib import Path
import sys

from tqdm import tqdm

from .common import is_multicolumn, read_facts, image_filename_to_book, book_to_url


INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITH_COLUMNS = sys.argv[1]
INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITHOUT_COLUMNS = sys.argv[2]
INPUT_OCR_ROOT = Path(sys.argv[3])
OUTPUT_REPORT = Path(sys.argv[4])
ALGORITHM = sys.argv[5] if len(sys.argv) > 5 else 'computational_geometry'


def is_multicolumn_worker(args):
    downscaled_input_filename, input_basename = args
    input_hocr_filename = (INPUT_OCR_ROOT / input_basename).with_suffix('.hocr')
    try:
        result = is_multicolumn(input_hocr_filename, ALGORITHM)
    except IOError:
        return args, None
    return args, result


def evaluate_one(facts_filename, expected_value):
    logger = logging.getLogger('evaluate_one')

    num_successful = 0
    num_no_hocr = 0

    correct = []
    incorrect = []

    facts = list(read_facts(facts_filename))
    with Pool(None) as pool:
        results = pool.imap_unordered(is_multicolumn_worker, facts)
        results = tqdm(results, desc=f'Detecting layout for pages {facts_filename}', total=len(facts))
        for args, result in results:
            if result is None:
                num_no_hocr += 1
            else:
                if result == expected_value:
                    correct.append(args)
                else:
                    incorrect.append(args)
                num_successful += 1

    logger.info(
        'Detected layout in {} OCR texts, not found {} HOCR files'.format(
            num_successful,
            num_no_hocr,
        )
    )
    logger.info(
        'Out of the {} OCR texts, {} ({:.2f}%) were correctly detected and {} ({:.2f}%) were incorrectly detected'.format(
            num_successful,
            len(correct),
            100.0 * len(correct) / num_successful,
            len(incorrect),
            100.0 * len(incorrect) / num_successful,
        )
    )

    return correct, incorrect


def evaluate():
    tp, fn = evaluate_one(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITH_COLUMNS, True)
    tn, fp = evaluate_one(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITHOUT_COLUMNS, False)
    cases = [('True positives', tp), ('True negatives', tn), ('False positives', fp), ('False negatives', fn)]
    cases = OrderedDict(cases)
    total = sum(cases.values(), [])
    with OUTPUT_REPORT.open('wt') as f:
        for case, args_list in cases.items():
            print(f'# {case} ({len(args_list)} / {len(total)} = {100.0 * len(args_list) / len(total):.2f}%)\n', file=f)
            for args in args_list:
                downscaled_input_filename, input_basename = args
                url = book_to_url(*image_filename_to_book(downscaled_input_filename))
                print(f' - [{input_basename}]({url})', file=f)
            print(file=f)


if __name__ == '__main__':
    evaluate()
