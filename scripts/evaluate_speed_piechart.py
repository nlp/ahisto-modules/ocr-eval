# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING, format=LOGGING_FORMAT)


from itertools import dropwhile
import json
import re
import sys

import matplotlib.pyplot as plt

from .common import read_joblog


JOBLOG_FILENAME = sys.argv[1]
JOBLOG = list(read_joblog(JOBLOG_FILENAME))
COMMAND_REGEX_FILENAME = sys.argv[2]
LABEL_MAP_FILENAME = sys.argv[3]
OUTPUT_FILENAME = sys.argv[4]

COMMAND_REGEX = re.compile(open(COMMAND_REGEX_FILENAME, 'rt').read().strip())
LABEL_MAP = json.load(open(LABEL_MAP_FILENAME, 'rt'))


def evaluate():
    logger = logging.getLogger('evaluate')

    runtimes = {}
    missing_commands = set()
    for command, runtime, exit_code in JOBLOG:
        if exit_code != 0:
            continue
        match = re.fullmatch(COMMAND_REGEX, command)
        assert match is not None
        command = list(dropwhile(lambda x: x is None, match.groups()))[0]
        if command not in LABEL_MAP:
            if command not in missing_commands:
                logger.warning('Command {} not in label map'.format(command))
                missing_commands.add(command)
            continue
        label = LABEL_MAP[command]
        if label not in runtimes:
            runtimes[label] = []
        runtimes[label].append(runtime)
    return runtimes


def pie_chart(runtimes):
    labels, sizes = zip(*runtimes.items())
    labels_iterator = iter(labels)
    sizes = list(map(sum, sizes))
    colors = plt.get_cmap('tab20').colors

    def autopct(pct):
        label = next(labels_iterator)
        if pct < 5.0:
            return ''
        absolute = int(pct / 100.0 * sum(sizes) / 86400)
        return '{:s}\n{:d} days'.format(label, absolute)

    patches, texts, autotexts = plt.pie(sizes, colors=colors, autopct=autopct)
    plt.setp(autotexts, size=8, weight='bold', color='k')
    plt.axis('equal')
    plt.tight_layout()
    plt.savefig(OUTPUT_FILENAME)


def main():
    runtimes = evaluate()
    pie_chart(runtimes)


if __name__ == '__main__':
    main()
