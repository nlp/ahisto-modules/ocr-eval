# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.ERROR, format=LOGGING_FORMAT)


from statistics import mean
import sys

from .common import read_joblog


JOBLOG_FILENAME = sys.argv[1]
JOBLOG = list(read_joblog(JOBLOG_FILENAME))


def evaluate():
    runtimes = []
    for _, runtime, exit_code in JOBLOG:
        if exit_code == 0:
            runtimes.append(runtime)
    print(
        'The jobs would complete in {:g} days on a single machine, {} seconds per job'.format(
            sum(runtimes) / 86400.0,
            mean(runtimes),
        )
    )


def main():
    evaluate()


if __name__ == '__main__':
    main()
