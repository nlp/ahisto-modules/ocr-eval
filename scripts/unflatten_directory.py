# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import shutil
import sys

from tqdm import tqdm

from .common import read_facts


INPUT_ROOT = Path(sys.argv[1])
INPUT_DOWNSCALED_FILENAMES = list(read_facts(sys.argv[2]))
OUTPUT_ROOT = Path(sys.argv[3])


def copy(args):
    downscaled_filename, upscaled_filename = args
    input_filename = INPUT_ROOT / downscaled_filename
    if input_filename.exists():
        output_filename = OUTPUT_ROOT / upscaled_filename
        output_dirname = output_filename.parent
        output_dirname.mkdir(parents=True, exist_ok=True)
        shutil.copy(str(input_filename), str(output_filename))
        return True
    else:
        return False


def main():
    logger = logging.getLogger('main')

    OUTPUT_ROOT.mkdir(exist_ok=True)
    filenames = INPUT_DOWNSCALED_FILENAMES
    filenames = tqdm(filenames, 'Unflattening files {}/**/*.jpg to {}/**/*'.format(
        INPUT_ROOT,
        OUTPUT_ROOT,
    ))
    with Pool(None) as pool:
        successfully_copied = 0
        total = 0
        for success in pool.imap_unordered(copy, filenames):
            if success:
                successfully_copied += 1
            total += 1

    logger.info('Successfully copied {} out of {} files'.format(successfully_copied, total))


if __name__ == '__main__':
    main()
