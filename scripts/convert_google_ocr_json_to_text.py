# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


import json
from multiprocessing import Pool
from pathlib import Path
import sys

from tqdm import tqdm

from .common import read_facts


INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES = sys.argv[1] if sys.argv[1] != 'None' else None
OUTPUT_OCR_ROOT = Path(sys.argv[2])


def read_jsons_worker(input_filename):
    input_basename = OUTPUT_OCR_ROOT / input_filename
    json_filename = input_basename.with_suffix('.json')
    text_filename = input_basename.with_suffix('.txt')
    if not json_filename.exists():
        return 'not-exists'
    try:
        ocr_output = json.load(json_filename.open('rt'))
    except json.JSONDecodeError:
        return 'malformed'
    if not ocr_output['responses'][0]:
        return 'no-response'
    ocr_output = ocr_output['responses'][0]['fullTextAnnotation']['text']
    return (text_filename, ocr_output)


def read_filenames():
    if INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES is not None:
        for _, filename in read_facts(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES):
            yield filename
    else:
        for filename in sys.stdin:
            yield filename


def read_jsons():
    logger = logging.getLogger('read_jsons')

    num_successful = 0
    num_nonexistent = 0
    num_malformed = 0
    num_no_response = 0

    filenames = list(read_filenames())
    with Pool(None) as pool:
        texts = pool.imap_unordered(read_jsons_worker, filenames)
        texts = tqdm(texts, desc='Reading OCR texts in JSON format', total=len(filenames))
        for result in texts:
            if result == 'not-exist':
                num_nonexistent += 1
                continue
            if result == 'malformed':
                num_malformed += 1
                continue
            if result == 'no-response':
                num_no_response += 1
                continue
            text_filename, ocr_output = result
            yield (text_filename, ocr_output)
            num_successful += 1

    logger.info(
        'Read {} OCR texts in JSON format, not found {}, {} were malformed, {} had no response'.format(
            num_successful,
            num_nonexistent,
            num_malformed,
            num_no_response,
        )
    )


def write_texts_worker(args):
    text_filename, ocr_output = args
    with text_filename.open('wt') as f:
        f.write(ocr_output)


def write_texts(texts):
    with Pool(None) as pool:
        for _ in pool.imap_unordered(write_texts_worker, texts):
            pass


def main():
    texts = read_jsons()
    write_texts(texts)


if __name__ == '__main__':
    main()
