# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


import json
from pathlib import Path
import random
import shutil
import sys

from tqdm import tqdm

from .common import read_facts, iterate_lines_hocr, image_filename_to_book, book_to_url


INPUT_FILENAME = Path(sys.argv[1])
INPUT_ROOT = Path(sys.argv[2])
OUTPUT_ROOT_INPUT = Path(sys.argv[3])
OUTPUT_ROOT_OUTPUT = Path(sys.argv[4])
SAMPLE_SIZE = int(sys.argv[5])
INPUT_UPSCALED_FILENAMES = sys.argv[6]

SEED = 21


def fence(text):
    separator_length = 1
    while ('`' * separator_length) in text:
        separator_length += 1
    prefix = '`' * separator_length + (' ' if text.startswith('`') else '')
    suffix = (' ' if text.endswith('`') else '') + '`' * separator_length
    return f'{prefix}{text}{suffix}'


def main(seed=SEED, sample_size=SAMPLE_SIZE):
    logger = logging.getLogger('main')

    known_pages = {
        image_filename_to_book(output_filename): input_basename
        for output_filename, input_basename
        in read_facts(INPUT_UPSCALED_FILENAMES)
    }

    with INPUT_FILENAME.open('rt') as f:
        relevant_pages = sorted(set(
            (int(book_id), int(page_id))
            for book_id, page_ids in json.load(f).items()
            for page_id in page_ids
        ) & known_pages.keys())

    random.seed(seed)
    sample = random.sample(relevant_pages, sample_size)

    OUTPUT_ROOT_INPUT.mkdir(exist_ok=True)
    OUTPUT_ROOT_OUTPUT.mkdir(exist_ok=True)
    for book_id, page_id in tqdm(sample, desc='Producing a sample'):
        input_basename = known_pages[book_id, page_id]
        input_filename = (INPUT_ROOT / input_basename).with_suffix('.hocr')
        output_basename = f'{book_id}-{page_id}'
        output_hocr_filename = (OUTPUT_ROOT_INPUT / output_basename).with_suffix('.hocr')
        output_md_filename = (OUTPUT_ROOT_OUTPUT / output_basename).with_suffix('.md')
        shutil.copy(str(input_filename), str(output_hocr_filename))
        with output_md_filename.open('wt') as wf:
            image_url = book_to_url(book_id, page_id, url_type='image')
            portal_url = book_to_url(book_id, page_id, url_type='portal')
            print(f'[![page image][image]][portal]\n\n[image]: {image_url}\n[portal]: {portal_url}\n\n***\n', file=wf)
            with input_filename.open('rb') as rf:
                for language_code, words in iterate_lines_hocr(rf):
                    line = fence(' '.join(words))
                    if language_code is not None:
                        line = f'{line}{{.unchecked lang={language_code}}}   '
                    else:
                        line = f'{line}{{.unchecked}}   '
                    print(line, file=wf)

    message = 'Successfully produced a sample of {} relevant pages from {} to {} and to {}'
    logger.info(message.format(sample_size, INPUT_ROOT, OUTPUT_ROOT_INPUT, OUTPUT_ROOT_OUTPUT))


if __name__ == '__main__':
    main()
