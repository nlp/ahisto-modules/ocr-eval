# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import sys

from tqdm import tqdm

from .common import read_facts, read_page_languages, get_jaccard_index, get_spearman_r, get_accuracy_at_one, print_confidence_interval


INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES = sys.argv[1]
INPUT_OCR_ROOT = Path(sys.argv[2])
OUTPUT_OCR_ROOT = Path(sys.argv[3])
DETECTED_LANGUAGES = sys.argv[4].split('+') if sys.argv[4] != 'None' else None
OUTPUT_LOG_FILENAME = Path(sys.argv[5])
INPUT_ALGORITHM = sys.argv[6] if len(sys.argv) > 6 else 'NLDA'
OUTPUT_ALGORITHM = sys.argv[7] if len(sys.argv) > 7 else 'NLDA'


def get_measures_worker(args):
    downscaled_input_filename, input_filename = args
    input_ocr_basename = str((INPUT_OCR_ROOT / input_filename).with_suffix(''))
    try:
        ground_truth = read_page_languages(input_ocr_basename, DETECTED_LANGUAGES, algorithm=INPUT_ALGORITHM)
    except IOError:
        return 'skip'
    output_ocr_basename = str((OUTPUT_OCR_ROOT / input_filename).with_suffix(''))
    try:
        ocr_output = read_page_languages(output_ocr_basename, DETECTED_LANGUAGES, algorithm=OUTPUT_ALGORITHM)
    except IOError:
        return 'not-exists'
    jaccard_index = get_jaccard_index(ground_truth, ocr_output)
    spearman_r = get_spearman_r(ground_truth, ocr_output)
    accuracy_at_one = get_accuracy_at_one(ground_truth, ocr_output)
    return (jaccard_index, spearman_r, accuracy_at_one)


def get_measures():
    logger = logging.getLogger('get_measures')

    num_successful = 0
    num_skipped = 0
    num_nonexistent = 0

    facts = list(read_facts(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES))
    with Pool(None) as pool:
        texts = pool.imap_unordered(get_measures_worker, facts)
        texts = tqdm(texts, desc='Reading detected page languages', total=len(facts))
        for result in texts:
            if result == 'skip':
                num_skipped += 1
                continue
            if result == 'not-exists':
                num_nonexistent += 1
                continue
            jaccard_index, spearman_r, accuracy_at_one = result
            yield (jaccard_index, spearman_r, accuracy_at_one)
            num_successful += 1

    logger.info(
        'Read {} ground truth--OCR detected page languages, skipping {}, not found {}'.format(
            num_successful,
            num_skipped,
            num_nonexistent,
        )
    )


def evaluate():
    jaccard_indices = []
    spearman_rs = []
    accuracies_at_one = []
    for jaccard_index, spearman_r, accuracy_at_one in get_measures():
        if jaccard_index is not None:
            jaccard_indices.append(jaccard_index)
        if spearman_r is not None:
            spearman_rs.append(spearman_r)
        if accuracy_at_one is not None:
            accuracies_at_one.append(accuracy_at_one)
    with OUTPUT_LOG_FILENAME.open('wt') as f:
        print_confidence_interval(f, jaccard_indices, 'mean IOU', '%')
        print_confidence_interval(f, spearman_rs, 'mean Spearman\'s rho')
        print_confidence_interval(f, accuracies_at_one, 'accuracy@1', '%')


if __name__ == '__main__':
    evaluate()
