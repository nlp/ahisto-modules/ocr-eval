# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
import sys
import re
import subprocess
from lxml import etree
from pathlib import Path

from tqdm import tqdm

from .common import read_facts


INPUT_ROOT = Path(sys.argv[1])
INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES = sys.argv[2]
OUTPUT_ROOT = Path(sys.argv[3])
OUTPUT_FILENAME = Path(sys.argv[4])
WHITELIST = re.compile(sys.argv[5]) if sys.argv[5] != 'None' else None
BLACKLIST = re.compile(sys.argv[6]) if sys.argv[6] != 'None' else None

INPUT_UPSCALED_HIGH_CONFIDENCE = dict(read_facts(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES))

ARROWS = {
    'Hussitica, 120 stránek': '=>',
    'Mimo Hussicitu, 30 stránek': '->',
}


def input_filename_to_book_id_and_page(input_filename):
    normalized_filename = input_filename.stem.strip().upper()
    match = re.match(r'(?P<book_id>[0-9]+)-(?P<page>[0-9]+) .*HOTOVO.*', normalized_filename)
    if not match:
        return None
    book_id = match.group('book_id')
    book_id = int(book_id)
    page = match.group('page')
    page = int(page)
    return (book_id, page)


def html_text_to_text(html_text):
    parser = etree.HTMLParser()
    tree = etree.fromstring(html_text, parser)
    paragraphs = []
    for paragraph in tree.xpath('//ol/following::p'):
        lines = []
        texts = []
        consecutive_line_breaks = 0
        for text in paragraph.xpath('text()|br'):
            if not isinstance(text, str):
                consecutive_line_breaks += 1
                continue
            text = text.strip()
            if not text:
                continue
            if consecutive_line_breaks:
                lines.append(' '.join(texts))
                texts.clear()
                if consecutive_line_breaks > 1:
                    paragraphs.append('\n'.join(lines))
                    lines.clear()
                consecutive_line_breaks = 0
            texts.append(text)
        lines.append(' '.join(texts))
        paragraphs.append('\n'.join(lines))
    return '\n\n'.join(paragraphs)


def input_filename_to_arrow(input_filename):
    input_dirname = input_filename.parent.name
    arrow = ARROWS[input_dirname]
    return arrow


def input_filename_to_text(input_filename):
    command = ['pandoc', '-f', 'docx', '-t', 'html', '-i', str(input_filename), '-o', '-']
    process = subprocess.run(command, text=True, capture_output=True)
    html_text = process.stdout
    text = html_text_to_text(html_text)
    return text


def book_id_and_page_to_output_filenames(book_id, page):
    downscaled_output_filename = './{}/{}.jpg'.format(book_id, page)
    output_filename = INPUT_UPSCALED_HIGH_CONFIDENCE[downscaled_output_filename]
    return (downscaled_output_filename, output_filename)


def read_texts_worker(input_filename):
    page_information = input_filename_to_book_id_and_page(input_filename)
    if page_information is None:
        return None
    book_id, page = page_information
    downscaled_output_filename, output_filename = book_id_and_page_to_output_filenames(book_id, page)
    text = input_filename_to_text(input_filename)
    arrow = input_filename_to_arrow(input_filename)
    return (downscaled_output_filename, output_filename, text, arrow)


def main():
    logger = logging.getLogger('main')

    OUTPUT_ROOT.mkdir(exist_ok=True)
    input_filenames = INPUT_ROOT.glob('*/*.docx')
    if WHITELIST is not None:
        input_filenames = filter(lambda filename: WHITELIST.search(str(filename)), input_filenames)
    if BLACKLIST is not None:
        input_filenames = filter(lambda filename: not BLACKLIST.search(str(filename)), input_filenames)
    input_filenames = list(input_filenames)
    with Pool(None) as pool, OUTPUT_FILENAME.open('wt') as facts_f:
        texts = pool.imap_unordered(read_texts_worker, input_filenames)
        message = 'Untypesetting {}/*/*.docx'.format(INPUT_ROOT)
        texts = tqdm(texts, total=len(input_filenames), desc=message)
        num_total = 0
        num_successful = 0
        num_skipped = 0
        num_arrows = dict()
        for text in texts:
            num_total += 1
            if text is None:
                num_skipped += 1
                continue
            downscaled_output_filename, output_filename, text, arrow = text
            if arrow not in num_arrows:
                num_arrows[arrow] = 0
            num_arrows[arrow] += 1
            fact = '{}\t{}\t{}'.format(downscaled_output_filename, arrow, output_filename)
            print(fact, file=facts_f)
            output_filename = (OUTPUT_ROOT / output_filename).with_suffix('.txt')
            output_dirname = output_filename.parent
            output_dirname.mkdir(parents=True, exist_ok=True)
            with output_filename.open('wt') as text_f:
                print(text, file=text_f)
            num_successful += 1

    num_arrows = ', '.join('{}: {}'.format(*item) for item in sorted(num_arrows.items()))
    message = 'Untypeset {} pages ({}) out of {}, skipping {}'
    message = message.format(num_successful, num_arrows, num_total, num_skipped)
    logger.info(message)


if __name__ == '__main__':
    main()
