# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


import sys

from .common import read_facts


INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES = sys.argv[1]


if __name__ == '__main__':
    facts = read_facts(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
    for filename, _ in facts:
        print(filename)
