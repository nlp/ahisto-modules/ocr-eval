#!/bin/bash
# Converts a single image using the images.annotate method of the Google Cloud Vision AI.
# <https://cloud.google.com/vision/docs/reference/rest/v1/images/annotate>

set -e

if (( $# < 4 ))
then
  printf 'Usage: %s API_KEY INPUT_IMAGE OUTPUT_SUCCESS OUTPUT_ERROR'
  exit 1
fi

TEMPDIR=`mktemp -d`
trap 'rm -rf $TEMPDIR' EXIT
convert "$2" -filter Lanczos -resize '75000000@>' $TEMPDIR/image.png
cat <<EOF > $TEMPDIR/request.json
{
  "requests": [
    {
      "image": {
        "content": "$(base64 -w 0 $TEMPDIR/image.png)"
      },
      "features": [
        {
          "type": "DOCUMENT_TEXT_DETECTION"
        }
      ]
    }
  ]
}
EOF
curl -s -X POST \
  -H "Content-Type: application/json; charset=utf-8" \
  -d @$TEMPDIR/request.json \
  https://eu-vision.googleapis.com/v1/images:annotate?key="$(cat "$1")" \
  > $TEMPDIR/response.json
if grep -q '^\s*"error":' $TEMPDIR/response.json
then
  cat $TEMPDIR/response.json | tee "$4" 1>&2
  exit 2
fi
cp $TEMPDIR/response.json "$3"
