# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


import json
from multiprocessing import Pool
from pathlib import Path
import sys

from tqdm import tqdm

from .common import get_character_error_rate, get_word_error_rate, read_facts, image_filename_to_book, Evaluation


INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES = sys.argv[1]
INPUT_RELEVANT_PAGES_FILENAME = Path(sys.argv[2])
INPUT_OCR_ROOT = Path(sys.argv[3])
OUTPUT_OCR_ROOT = Path(sys.argv[4])
OUTPUT_LOG_FILENAME = Path(sys.argv[5])
OUTPUT_MOST_DIFFICULT_PAGES_FILENAME = Path(sys.argv[6])

with INPUT_RELEVANT_PAGES_FILENAME.open('rt') as f:
    INPUT_RELEVANT_PAGES = {
        int(book_id): pages
        for book_id, pages
        in json.load(f).items()
    }


def read_texts_worker(args):
    downscaled_input_filename, input_filename = args
    input_ocr_filename = (INPUT_OCR_ROOT / input_filename).with_suffix('.txt')
    try:
        ground_truth = input_ocr_filename.open('rt').read()
    except IOError:
        return 'skip'
    output_ocr_filename = (OUTPUT_OCR_ROOT / input_filename).with_suffix('.txt')
    try:
        ocr_output = output_ocr_filename.open('rt').read()
    except IOError:
        return 'not-exists'
    book_id, page = image_filename_to_book(downscaled_input_filename)
    return (book_id, page, ground_truth, ocr_output)


def read_texts():
    logger = logging.getLogger('read_texts')

    num_successful = 0
    num_skipped = 0
    num_nonexistent = 0

    facts = list(read_facts(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES))
    with Pool(None) as pool:
        texts = pool.imap_unordered(read_texts_worker, facts)
        texts = tqdm(texts, desc='Reading OCR outputs', total=len(facts))
        for result in texts:
            if result == 'skip':
                num_skipped += 1
                continue
            if result == 'not-exists':
                num_nonexistent += 1
                continue
            book_id, page, ground_truth, ocr_output = result
            yield (book_id, page, ground_truth, ocr_output)
            num_successful += 1

    logger.info(
        'Read {} ground truth--OCR text pairs, skipping {}, not found {}'.format(
            num_successful,
            num_skipped,
            num_nonexistent,
        )
    )


def evaluate_worker(args):
    book_id, page, correct_text, predicted_text = args
    character_error_rate = get_character_error_rate(correct_text, predicted_text)
    word_error_rate = get_word_error_rate(correct_text, predicted_text)
    return (book_id, page, character_error_rate, word_error_rate, correct_text, predicted_text)


def evaluate():
    texts = list(read_texts())
    evaluation = Evaluation(INPUT_RELEVANT_PAGES, OUTPUT_MOST_DIFFICULT_PAGES_FILENAME, OUTPUT_LOG_FILENAME)
    with Pool(None) as pool:
        results = pool.imap(evaluate_worker, texts)
        results = tqdm(results, desc='Evaluating', total=len(texts))
        for result in results:
            evaluation.update(*result)
    evaluation.finish()


if __name__ == '__main__':
    evaluate()
