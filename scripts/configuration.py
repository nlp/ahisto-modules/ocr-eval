# -*- coding:utf-8 -*-

import csv
import ctypes


csv.field_size_limit(int(ctypes.c_ulong(-1).value // 2))

CSV_PARAMETERS = {
    'delimiter': ',',
    'quotechar': '"',
    'quoting': csv.QUOTE_MINIMAL,
}

SQL_PARAMETERS = {
    'charset': 'utf8',
    'host': '127.0.0.1',
}

JSON_PARAMETERS = {
    'sort_keys': True,
    'indent': 4,
}

PREPROCESSED_IMAGE_WIDTH = 512
PREPROCESSED_IMAGE_HEIGHT = 512
ANNOY_N_TREES = 1000
