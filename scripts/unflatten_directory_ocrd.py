# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import shutil
import sys

from tqdm import tqdm

from .common import read_facts


INPUT_ROOT = Path(sys.argv[1])
INPUT_SUBDIRECTORY = sys.argv[2]
INPUT_PREFIX = sys.argv[3]
INPUT_SUFFIX = sys.argv[4]
INPUT_UPSCALED_FILENAMES = list(read_facts(sys.argv[5]))
OUTPUT_ROOT = Path(sys.argv[6])
OUTPUT_SUFFIX = sys.argv[7]


def copy(args):
    input_filename, output_filename = args
    input_filename = Path(input_filename)
    input_dirname = (INPUT_ROOT / input_filename).parent / INPUT_SUBDIRECTORY
    input_filename = '{}{}{}'.format(INPUT_PREFIX, input_filename.stem, INPUT_SUFFIX)
    input_filename = input_dirname / input_filename
    if input_filename.exists():
        output_filename = Path(output_filename)
        output_dirname = (OUTPUT_ROOT / output_filename).parent
        output_dirname.mkdir(parents=True, exist_ok=True)
        output_filename = '{}{}'.format(output_filename.stem, OUTPUT_SUFFIX)
        output_filename = output_dirname / output_filename
        shutil.copy(str(input_filename), str(output_filename))
        return True
    else:
        return False


def main():
    logger = logging.getLogger('main')

    OUTPUT_ROOT.mkdir(exist_ok=True)
    filenames = INPUT_UPSCALED_FILENAMES
    filenames = tqdm(filenames, 'Unflattening files {}/*/{}/{}*{} to {}/**/*{}'.format(
        INPUT_ROOT,
        INPUT_SUBDIRECTORY,
        INPUT_PREFIX,
        INPUT_SUFFIX,
        OUTPUT_ROOT,
        OUTPUT_SUFFIX,
    ))
    with Pool(None) as pool:
        successfully_copied = 0
        total = 0
        for success in pool.imap_unordered(copy, filenames):
            if success:
                successfully_copied += 1
            total += 1

    logger.info('Successfully copied {} out of {} files'.format(successfully_copied, total))


if __name__ == '__main__':
    main()
