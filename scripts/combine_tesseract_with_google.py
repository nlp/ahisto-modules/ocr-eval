# -*- coding:utf-8 -*-

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from multiprocessing import Pool
from pathlib import Path
import sys

from tqdm import tqdm

from .common import is_multicolumn, read_facts


INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES = sys.argv[1]
INPUT_OCR_ROOT_TESSERACT = Path(sys.argv[2])
INPUT_OCR_ROOT_GOOGLE = Path(sys.argv[3])
OUTPUT_OCR_ROOT = Path(sys.argv[4])


def read_texts_worker(args):
    downscaled_input_filename, input_basename = args
    input_hocr_filename = (INPUT_OCR_ROOT_TESSERACT / input_basename).with_suffix('.hocr')
    try:
        use_tesseract = is_multicolumn(input_hocr_filename)
    except IOError:
        return 'no-hocr'
    if use_tesseract:
        input_filename = (INPUT_OCR_ROOT_TESSERACT / input_basename).with_suffix('.txt')
    else:
        input_filename = (INPUT_OCR_ROOT_GOOGLE / input_basename).with_suffix('.txt')
    try:
        ocr_output = input_filename.open('rt').read()
    except IOError:
        return 'no-txt-tesseract' if use_tesseract else 'no-txt-google'
    return (input_basename, ocr_output, use_tesseract)


def read_texts():
    logger = logging.getLogger('read_texts')

    num_successful = 0
    num_no_hocr = 0
    num_no_txt_tesseract = 0
    num_no_txt_google = 0

    num_tesseract = 0
    num_google = 0

    facts = list(read_facts(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES))
    with Pool(None) as pool:
        texts = pool.imap_unordered(read_texts_worker, facts)
        texts = tqdm(texts, desc='Reading OCR outputs', total=len(facts))
        for result in texts:
            if result == 'no-hocr':
                num_no_hocr += 1
                continue
            if result == 'no-txt-tesseract':
                num_no_txt_tesseract += 1
                continue
            if result == 'no-txt-google':
                num_no_txt_google += 1
                continue
            basename, ocr_output, use_tesseract = result
            if use_tesseract:
                num_tesseract += 1
            else:
                num_google += 1
            yield (basename, ocr_output)
            num_successful += 1

    logger.info(
        'Read {} OCR texts, not found {} HOCR and {} TXT files ({} Tesseract + {} Google)'.format(
            num_successful,
            num_no_hocr,
            num_no_txt_tesseract + num_no_txt_google,
            num_no_txt_tesseract,
            num_no_txt_google,
        )
    )
    logger.info(
        'Out of the {} OCR texts, {} ({:.2f}%) were by Tesseract and {} ({:.2f}%) were by Google Vision AI'.format(
            num_successful,
            num_tesseract,
            100.0 * num_tesseract / num_successful,
            num_google,
            100.0 * num_google / num_successful,
        )
    )


def write_texts_worker(args):
    output_basename, ocr_output = args
    output_filename = (OUTPUT_OCR_ROOT / output_basename).with_suffix('.txt')
    output_dirname = output_filename.parent
    output_dirname.mkdir(parents=True, exist_ok=True)
    with output_filename.open('wt') as f:
        print(ocr_output, file=f)


def write_texts(texts):
    OUTPUT_OCR_ROOT.mkdir(exist_ok=True)
    with Pool(None) as pool:
        for _ in pool.imap_unordered(write_texts_worker, texts):
            pass


def combine():
    texts = read_texts()
    write_texts(texts)


if __name__ == '__main__':
    combine()
