192
1423 říjen 4.
čís. 310, 311.
Zikmund, král římský, uherský a český, potvrzuje
z moci českého krále Albrechtovi, vévodovi rakouskému
a jeho choti Alžbětě, své dceři, držení markrabství Mo-
ravského jako léna České koruny.

Orig. perg. 46x28–7 cm, něm. K listině jest při-
věšena na černožlutých hedvábných nitích ku-
latá majestátní pečeť krále Zikmunda z vosku
přirozené barvy.
Na plice: Ad mandatum domini regis Johannes, episcopus Za-
grabiensis, cancellarius.
Na rubu: R. Henricus Fye. — Collatio marchionatus Moraviae
a rege Sigismundo Alberto, duci Austriae, et Elisabethe, uxori suae,
facta. N. 2. — T. B. Fol. 9.
Knihy priv. A II., fol. 9-9 p. v., B II., fol. 9–9 p. V.
Rosenthal KA, lit, A, č. 11 a. – Vídeňské rep. č. 1249.
Vidimus cís. Marie Terezie ze dne 11. května 1754 (Praž. rep.
č. inv. 746, č. rep. 467).
Mechioris Goldasti Heimisfeldii Collectio variorum consilio-
rum. Tomus II. (Francofurti ad Moenum 1719), p. 275—278.—
Dobner, Mon. Hist. Boem. IV., pag. 414 (regest). — Regesta impe-
rii XI., str. 397, č. 5625. - Lünig, Reichs Arch.v P. spec Cont. I.
(pokr.), str. 260. - Lünig, Corp. iur. feud. 2, str. 49.— Fejér, Codex
Hungariae X. 6., str. 541, č. CCXXVI. — Aschbach, Zikmund, III. díl,
str. 448 (regest). — Lichnowski, Gesch. Alb. Linie, Anhang,
str. CXCIII., č. 2147 (regest). — Sedláček, Zbytky reg., str. 172,
č. 1239.
311.
1423, říjen 4. Budín.
(Geben zu Ofen nach Crists gebürt vierczehen-
hundert jare und dornach in dem dreyundczwein-
czigisten jare am nechsten montage nach Sant
Michels tag, unserr riche des Hungrischen etc. in
dem sibenunddrissigisten, des Romischen in dem
vierczehenden und des Behemischen in dem vier-
den jaren.)

Zikmund, král římský, uherský a český, potvrzuje
z moci krále římského Albrechtovi, vévodovi rakouskému,
a jeho choti Alžbětě, své dceři, držení markrabství Morav-
ského jako léna České koruny.

Orig. per g. 38x27–8 cm, něm. K listině jest přivě-
šena na černožlutých hedvábných nitích poněkud
poškozená kulatá majestátní pečeť krále Zik-
munda z vosku přirozené barvy.
