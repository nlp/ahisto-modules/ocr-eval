34 Urkunden zum Jahre 1420.
[1420]. Ende April bis Anfang August.
Einzelausgaben der Stadt Görlitz für den Feldzug nach
Böhmen.
Görl. Rr. V. Bl. 37a ff. 1)
[Bl. 37 a] Dis nochgeschrebene geschefte ist gegangen of die
herfart:
Zu der rynner 2) banyr zwu elen roter seide 1/2 mr., vor
funf virteil weissir zeide 16 gr., deme moler zu lyme 10 gr., vor
60 elen lymet 1/2 sch.; vor 7 phund odern 3) 14 gr.; vor steyn-
kulen zu hauen zu den buchsen 9 fert.; vor eyne trone zu ge-
rethe 4 gr.; vor zwene kessel zu der herfart, die haben 1 1/2 stein
koppher minus 2 phund, dorzu ist unsers kopphers 1/2 steyn
2 phund, 7 fert.; vor einen iserinnen twirl 1 1/2 gr.; vor 6 exse
16 gr. 4); vor 6 kilhauen unde rodehauen 18 gr., vor 2 1/2 schok
hufysen 1 sch. 15 gr., item vor oberige hufnaile zu den ysen
18 gr.; item vor röste, dreiffüsse, stocczen 5) unde eine iserinne
spille zu deme gezelde 1/2 mr.; vor 4 neue schyben 6) zu deme
buchsenwayne 18 gr., vor waynschenen 7) zu denselben raden
34 gr., item dieselben rade zu beslohen 1/2 mr.; vor buchsenachsen,
schossbaume 8) zu deme buchsenwayne 6 gr.; item vor binden unde
börsten 9) die selben waynerade 2 1/2 gr. 10); deme buchsenmeister
vor eine trone zu den buchsen 6 gr.; item vor ein schok vor-
slege 11) zu den buchsen 5 gr.; [Bl. 370] Item vor 4 schirmen
ader 12) setczetarczhen zu machen 13) 12 gr.; vor haspen zu den
-
1) Die folgenden Eintragungen finden sich auch Bl. 48 b ff., so daß Bl. 37a bis
39b und Bl. 48 b bis 51 b des 5. Bandes der Ratsrechnungen bis auf kleine unwesent-
liche Verschiedenheiten dasselbe bringen. Die Ausgabeposten Bl. 48 b ff. sind gestrichen.
Alle Varianten anzugeben halte ich für unnütz
2) Mittelhochdeutsch rennaere Reitknecht.
3) Steht wohl für ader = Bogensehne.
4) 48 b kosten dieselben Ärte 21 gr.
5) Klötze.
6) Räder.
7) Wagenschienen.
8) Sind wahrscheinlich die Hemmbäume (Schuhe), die quer unter dem Wagenkasten
angebracht sind, vergl. mittelniederd. schot = Riegel, Verschluß behufs Hemmung.
9) Räder binden = mit Reifen versehen; das alliterirende borsten ist mir
nicht klar, es kommt des Öfteren in den Rr. vor.
10) 48 b steht 3 gr.
11) Vorschlag ist das Ortscheit am Büchsenwagen, f. Schmoller, bayr. Wörterb. II. S.18.
12) 49a steht unde.
13) 49a setzt hinzu mit eyme cymmermanne. Über Setzetarschen — eine
bretterne Wand zum Schutz des Büchsenmeisters — s. Anzeiger für Kunde der deutschen
Vorzeit 17. Bd. 1870 Sp. 356.
