72 Urkunden zum Jahre 1421/22.
tilgen unde dy fursten uffnehmen etc. Gegeben am montage
vor Hedwigis zu felde vor Mittenwalde.

Iterum postscriptum. Wir müssen keuffen 1/4 byers vor
1 mark, vor 41/2 schilling unde auch teuer und neher 1) und
mogen mit 4 fudir biers den tag nicht zukommen unde ynanda 2)
musen wir haben 3/4 virtel, auch hat man ir 4/4 eynen tag aus-
getruncken. Noch mussen wir horen, Swobe sey wedir uffge-
standen. Ouch, liben herren, haben wir zu speisen 20 menschen
unde 200 adir me, das last euch auch zu hertzen gan.
Herman Schultes 3), Hannus Pletzil unde Heinrich Ebirhard,
der slift 4), den irbern weysen burgermeister unde ratmann der
stat Gorlicz.

Grünhagen datiert das Hauptschreiben vom 10., Palacky vom 12. Oktober,
die Verschiedenheit kommt daher, daß der Tag des heiligen Burchard sowohl
auf den 11. als auf den 14. Oktober fällt (bei Palacky ist wohl irrtümlich
der 12. für den 13. Oktober gesetzt). Die Datierungen im ersten postscriptum
erweisen, daß das richtige Datum der 10. Oktober ist.

„1421. Dezember 31 bis 1422 August 5.“
Unter den Propinationes (Verehrungen in Getränken) der Stadt
Nürnberg vor und bei dem Reichstage daselbst (vom Juli bis
September 1422) findet sich: „Propinavimus den von Pawdisz-
heim und den von Görlitz und den von der Syttaw 10 qr.;
summa 1 lb. 9 sh. 2 hllr.
Aus Deutsche Reichstagsakten VIII. S. 230, 3 ff.
Von Görlitz war nur der Stadtdiener Hasse mit in Nürnberg; der Führer der
Gesandtschaft war Punzel von Bautzen, der gegen Ende Juli 1422 wegritt und
in der Woche nach dem 12. September 1422 zurückkam, f. unten Rr. Š. 88, 29
und S. 92, 14.

1421/1422 Oktober - Oktober.
Einnahme und Ausgabe der Stadt Görlitz.
Rr. V. 64a — 70b, 138 a.
1) Die Einnahmen des Jahres betrugen 2123 sch. minus
6 gr., die Summe der beiden Geschosse 1331 sch. 13 gr. und zwar
[Bl. 67a] Von deme geschosse noch circumdederunt [1422. Febr. 8]

1) Ist kaum richtig, „auch theuerer und mehr“?
2) Ist mir unklar.
3) Nicht wie Scultet. schreibt Schultz.
4) „qui dormiebat“; einen Beinamen „der Slif" mit Grünhagen und Palacky
möchte ich nicht annehmen, da in den ziemlich zahlreichen Urkunden, in denen H. Ebir-
hard vorkommt, sich ein solcher nicht findet.
