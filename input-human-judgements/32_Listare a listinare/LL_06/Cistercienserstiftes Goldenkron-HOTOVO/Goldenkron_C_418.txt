418
rum, et pene omnia privilegia ad manus ipsius generosi domini
Joannis data sunt fideliter servanda.
Nach einer Aufzeichnung auf Papier aus dem 17. Jahrhundert in
Stiftsarchive zu Hohenfurt.

CLXXV.
1425 cc., October 28, Goldenkron. — Abt Rüdiger von Goldenkron
an die von Budweis um schnelle Hilfe gegen die heranrückenden Hussiten.

Prudentibus viris magistro civium, consulibus et iuratis
in Budwaiss vicinis et amicis dilectis cum devotis in domino
orationibus pronam complacendi voluntatem! Vicini et amici
dilecti! Quia veridice sumus praemoniti, quod Wiklephistae
iam in monte congregati monasterium nostrum ahuc hodie vel
cras mane omnino invadere disposuerunt, petimus igitur amici-
tiam vestram, de qua plenam fiduciam habemus, quatenus
absque omni negligentia (et?) statim nobis cum aliquot bali-
stariis et armatis hominibus numero quo poteritis maiori sub-
venire velitis, sic quod iidem, quos duxeritis dirigendos, adhuc
hodie die et nocte festinando veniant et sint in monasterio
constituti. Hoc volumus in simili erga vestram amicitiam
promereri. Datum dominica die ante festum omnium sanctorum.
Ruthgerus in Sancta Corona vester.

Nach Abschriften aus dem 17. Jahrhundert im Stiftsarchive zu Hohen-
furt. Eine ebendaselbst befindliche Klostergeschichte aus demselben Zeitraum,
welche wir schon mehrmal zu citiren Veranlassung hatten, hat diesen Brief
gleichfalls aufgenommen und geradezu in das Jahr 1420 versetzt, offenbar
weil es hiess, dass in diesem Jahre das Kloster von den Hussiten zerstört
worden sei. Aber diese Zerstörung erfolgte bereits am 10. Mai (Palacky,
Gesch. v. Böhmen, III b. 99) und vorstehendes Schreiben datirt vom October.
Mit der Zerstörung kann daher dieses Schreiben in keine Verbindung ge-
bracht werden. Weil aber der Inhalt des Schreibens, dessen Original im Bud-
weiser Archive zu suchen wäre, keine Anhaltspunkte bietet, um überhaupt ein
Jahr mit Sicherheit für dasselbe zu bestimmen, so haben wir es bloss mit
Rücksicht auf die vorhergehende selbst allerlei Zweifeln Raum gebende Num-
mer hier eingereiht, auch das Tagesdatum hiernach reducirt und wollen uns
sonst gerne einer wirklichen Reparatur unterwerfen, wenn eine solche von

ist, welches zur Zeit Herrn Johanns errichtet worden ist; daher obige
Abweichungen. Die erwähnte Klostergeschichte lässt übrigens durch-
scheinen, dass der Schatz wenigstens nicht mehr ganz restituirt worden;
vergl. deshalb aber N. CCLXII.
