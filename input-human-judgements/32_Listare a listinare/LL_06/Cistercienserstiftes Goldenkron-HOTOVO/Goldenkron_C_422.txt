422
Aus dem Original auf Pergament im k. k. g. H. H. und St.-Archiv in
Wien. Das an der Umschrift verletzte Sigel hängt an einer pergamenen
Pressel, ist ziemlich gross und in ungefärbtes Wachs gedrückt. Auf dem
Throne, gehalten in Form einer gothischen Kapelle, der König sitzend, mit
langem Gewand und Mantel, mit Scepter und Reichsapfel, dann der Krone
(wie die böhmische, jedoch überragt von einem Bogen) auf dem Haupte. Zu
jeder Seite je zwei Wappenschilde und auch zu den Füssen zwei solche
Rechts ein Adler und der zweischwänzige Löwe, links das Doppelkreuz and
die Querbalken, zu den Füssen ein Löwe und eine nicht mehr erkennbare
Figur. Die Umschrift, theilweise aus den Sigeln an den Urkunden N. CLXX
und N. CLXXII ergänzt und in gebrochener Schrift lautet: ,†Sigismvadvs.
Dei. Gra. Romanor. Rex. Semp. Avgust. Ac. Vngar. Dalma. Croa..
Galicie. Lodomerie. Co....... bvrgens'. Necno. Bohem..
Von einem Archivar des 15. Jahrhunderts auf der Rückseite des Pergaments:
List na VLm Ssch na Korunske zbozie, N. 3.'

CLXXVIII.
1430, Mai 1, o. AO. — Der Goldenkroner Converse Johannes ver-
kauft unter Vorbehalt des Wiedereinlösungsrechtes für sein Kloster den
Kalschinger Richter Anderl eine bei Kalsching gelegene Wiese WR
10 Schock Groschen.

Ego Johannes conversus 1 monasterii Sanctae Coronae
recognosco tenore praesentium universis, quod ego de voluntate
et consensu venerabilis patris et domini Ruthgeri abbatis et
totius conventus monasterii supradicti pratum, quod opido
Chwalsyn adiacet, in wlgari Thetunico dictum ,bis in ders
hakengastgrub´ per me emptum et comparatum vendidi
provido viro Endrlyno de Chwalsyn protunc iudici ibidem
pro decem sexagenis grossorum, quas ab eo percepi pecunia
in parata, tali tamen conditione adiecta et expressa, quod id
pratum dominus abbas et conventus monasterii supradicti,
quandocumque ipsorum placuerit voluntati, decem sexagenas
grossorum Pragencium (sic) poterint qualibet sine renitentia
Endrlyny superius dicti et suorum sequacium redimere vice-
versa. Harum quibus praedictarum maius sigillum domini
abbatis ad meas instantes preces appensum est testimonio
litterarum. Datum anno domini M°CCCC°XXX°, ipso die beato-
rum apostolorum Philippi et Jacobi.

1 Zu den schon bekannten Conversenstatuten liefert auch Winter, die
Cistercienser des nö. Deutschl. III. 186 u. ff., ein solches.
