Č. 326–327: 18.-22. června 1437.
219
na to sbožie na Corunské; to sem TMti pravil na Crumlově, když sem byl u TMti.
Protož, milý pane, nezdáť mi se, bychť co proti TMti v tom učinil. A věřímť TMti, že
mi toho hájiti nebudeš, nebť sem za to své dědictvie dal, a na toť majestát jmám
i panské pečeti. Vedle toho dále rač věděti, jestližeť mi neučinie toho, což jmají
učiniti, žeť na ně míniem sáhnúti jakožto na své. A TMti věřím, že mi v tom za zlé
jmieti nebudeš, neb bych TMti nerad hněval, neb sem za to své dal. Také rač věděti, že
sem nalezl Pořešínské sbožie ve dskách. I věřím TMti, že mi o to spravedlivé učiníš,
neb Buon to vie, že bych TMti nerad hněval. Ráčíš-li se mnú o to kterak uhoditi,
to mi rač dáti věděti listem svým.1 Také rač věděti, žeť sú mi chtěli zapsati Sverazské
sbožie, a jáť sem toho proti TMti učiniti nechtěl, poněvadž sem na to prvé zápisu
nejměl.2 Než na Corunské sbožie prvé zápis jmám než TMt, i přiejal sem. Sem na
tom vždy, k tomu hledě, nechtě TMti hněvati. Datum Prachatic feria III. post Viti.
Jan z Cřemže seděním na Prachaticích.
Na rubu: Uroz. pánu, panu Oldřichovi z Rosenberka,
pánu na mě laskavému.
Na rubu pismem 15. stol. Secunda.

327.
22. června [1437].
Jan Smil z Křemže Oldřichovi z Rožmberka: znovu o svém zápisu na vesnice
kláštera Zlatokorunského a o zboží Pořešínském.
Krumlov, Schwarzenb. archiv: I 5 BPa 5i, or. pap.
AČ. XXI, 288 Č. 23. — Srv. k tomu předcház. list.

Službu svú vzkazuji TMti, urozený pane milý! Jakož mi TMt píše,3 že jmám na
to sbožie na Corunské od krále nebožtíka majestát prve než TMt: i ovšem pane, pravdať
jest, že jmám, a tot, dá-li buoh, chci pokazati, když toho potřebie bude, a nadtoť
od ciesařovy Mti majestát jmám s panskými pečetmi, [že] mi to Jeho Mt zapisuje za
mé dědictvie.4 A jakož VMt píše o opata a převora [... mněť jest jich ...]a potřebie
proč napomínati, neb sem já to držal i podnes držím, a oni sú vždy té chvíle vyhnáni,
že sem s nimi nejměl od mluviti, ani sú nebyli toho mocni té chvíle i podnes nejsú.
I věřím TMti pane, že mne od mého tisknúti nebudeš, a službu mú TMt sobě zachová,

a) vytržen kus listu.
1) srv. niže v č. 327. 2) k držení Sverazského zboží srv. výše pozn. 2 na str. 96 a číslo 194,
též falsum 2 12. května 1380 (niže Č. 361) a list ze 6. května 1422 (viz níže č. 382, dodatek). 3) list
ten se nezachoval. 4) srv. pozn. 4 na str. 218.

28*
