FALSA. Č. 360–361: 22. ledna 1360.-11. května 1380.
251
Rosenberg fierent et agerentur, una cum prenominatis testibus et Wenceslao dicto
Taxilla, notario publico, et Sebaldo, notario publico, subscriptis, meis in hac parte
collega, presentes interfuerunt eaque sic fieri vidi et audivi, manu propria con-
scripsi et in hanc publicam formam redegi signoque et nomine meis solitis et con-
suetis una cum appensione sigillorum prenominatorum dominorum Jodoci, Petri,
Johannis et Ulrici fratrum de Rosenberg consignavi, rogatus et requisitus in fidem
et testimonium omnium premissorum.
Notářské znamení. Et ego Wenceslaus dictus Taxilla, quondam Jacobi de Vniczow,
clericus Olom. dioc., publicus auctoritate imperiali notarius, suprascriptis disposi-
cioni, ordinacioni, mandatis et commissioni omnibus aliis et singulis, dum sic, ut
premittitur, per nobiles dominos Jodocum, Petrum, Johannem et Ulricum fratres de
Rosenberg fierent et agerentur, una cum prenominatis testibus et Johanne de Ermicio,
notario publico, suprascripto meo et Sebaldo, notario publico subscripto, meo in hac
parte collega, presens interfui eaque sic fieri vidi et audivi et per prefatum Johan-
nem in presenti membrana scriptis in hanc publicam formam redegi signoque et
nomine meis solitis et consuetis una cum appensione sigillorum dictorum domino-
rum Jodoci, Petri, Johannis et Ulrici fratrum de Rosenberg et ad peticionem ipsorum
instantem consignavi, rogatus et requisitus in fidem et testimonium omnium pre-
missorum.
Notářské znamení. Et ego Sebaldus Conradi de Sacz, clericus Prag. diocesis, publi-
cus atd. j. sh. s přísluš. změnami.
Na perg. proužcích přivěšeno 5 pečetí : 1) majest. cis. Karla z vosku přiroz. barvy, sekret
červ. 2) z červ. v., v peč. poli růže, nad helmou s pokryvadly růže, nápis: sigillum iodoci
de rosenberk. 3) z červ. v., v peč. poli růže, nápis; s.petri.de.rosenberk. 4) z červ, v.,
štítek čtvrcený, v 1. a 4. poli tři tyčky, proutím opletené, v 2. a 3. růže, nápis: sigillum.
johannis.de.rosenberk. 5) z červ. v., v peč. poli na štítku růže, helma s křídlem, nápis:
sigillum.ulrici.de.rosenberk.
Na rubu: písmem 15. stol.: Karolus imperator confirmat, ut senior dominus de Rosenberg aliis
iunioribus superintendat et eosdem dirigat ac gubernet iuxta contenta litterarum presencium.—
Z r. 1493: Presentes littere intabulate sunt ex mandato serenissimi principis et domini, domini Wla-
dislai, dei gracia Hungarie Bohemieque regis etc., Henrico de Novadomo supremo camerario regni
Bohemie de baronibus refferente, dominice nativitatis anno millesimo quatercentesimo nonagesimo
tercio f. V. post Mathie apostoli. — Z r. 1543: Léta božího M°V XLIII° ve čtvrtek po svaté panně
Katheřině tento list zase ve dsky zemské vložen a vepsán jest podle svolení sněmovního. Pozděj-
ším písmem: Majoratus domus Rosenbergicae a Carolo 4to confirmatur.

361.
Na Karlštejně, 11. května 1380.
Král Václav dává Janovi z Rožmberka a jeho nástupcům v dědičné držení
zboží kostelů Sverazského a Zátoňského.
a) tak or., správně „collegis". b) tak or., správně „presens interfui“.
c) tak, or., správně
,,scripta".
32*
