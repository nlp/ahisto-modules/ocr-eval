Wortverzeichnis 261

lich ist aber bestaeten gemeint)
3861.
besten 1043.

besuochen versuchen 4459; nach-
suchen 4716.

beswaerde. Phrase: einen ane
b. haben, ihn nicht bekümmern
3442.

betriegen, ohne Objekt? mit
Dativ? 12694.

bewenden. Phrase: ez ist mir
also bewant daz ... 3256.

bewarn mit Genetiv: bewart der
boteschaft 2139 (der ewigen
Seligkeit teilhaftig werden);
b. mit einer Sache, bewähren
2662 (Wb. 3, 509.47 geleugnet). |

bewisen mit doppeltem Akkus.
2932.

bezzeren, reflexiv 2809. 2814.
2984 ; transitiv 2810. 4037 (D).

bezzerunge holn 2289.

bi im lebenden = Zeit seines
Lebens 2075.

bieten. sin gerihte s. gerihte;
hof b. 15352 (zweimal).

binz Binse, hier aber nicht, wie
sonst (Wb. 1, 137), die Binsen
auf die Teppiche gestreut,
sondern umgekehrt: uf pinze
menic tepet breit 6064.

birsen subst. Inf. 10063.

bischof 1805. 1808. 14451. 14475.
14513. 14529. 14788. 14797.

bit = bete, Bitte 12797.

biten, eines Dinges von einem
3514.

biulen, swarze, vom Kampfe
6705. 7701.

blat, Plural bleter 6378.

blide 514. 928. 3170.

bliugen, bluc machen, schwächen
4054 („wie sehr sind durch
dich alle Frauen in den
Schatten gestellt!“ der Dichter
aber meint wohl eher ge-
blüejet, s. unter blüejen).

blüejen, gegen 2720f. 4422;
Konstr.: mit dir sint geblüejet
elliu wip 4054 (s. auch bliugen).

blügen s. blüejen und bliugen.

blüemen fig. 12296.

blukeit 13460. 13915.

bluome fig. 1031. 2033. 6848.
7431. 12550.

bluomenschin, Kompositum, fig.
1639. 6277.

bluotecvar 1027.

bochen, subst. Inf., das „Pochen‘“,
trotzen, seine Ansprüche mit
Gewalt geltend zu machen
(Wb. 1, 220), in DMW gleich
überliefert 395.

boherdieren = buhurdieren 5836.

bon = boum 6375. 7407.

borte 10904.

botenbrot 1651. 13111.

boteschaft die Verheissung des

ewigen Lebens 2138.

boum (bon) als Helmschmuck
6375. 7407.

bovel 11656.

breiten, daz maere 2296.

bringen. enein br. 8130.

brisen 10903.

brugge (bruge) 9177. 9188. 9221.
9229. 9237. 9254. 9263. 15548.

bruoderlich 12370. 12386.

büezen, Prät. buozet 4947.

bühel 766.

buherdieren 13896.

buhiert = buhurt 5791. 14635;
buhert = dass. 14746.

búmaren s. bemaeren.

buoch, daz = die Dichtung
15619. 15631.

burcgrabe 7339.

buzele s. puzele.

c s. unter k.

danken subst. Inf. 9996.

dancuaeme dankbar (mit dem
Nebengedanken “skrupellos bc-
gierig”) 2127.

daz — wan daz 10454.

demüete sin, einem 3412. 15461.

deus frzös. Dieu 2029.

dewederthalp 276.

dienen, einem an etwas 8004;
uf einen, ironisch („sie leisteten
sich gegenseitig Dienste“) oder
„sie bemühten sich, einander
gegenseitig zuvorzukommen?”
299.

*dienlich 9338.

diener 2378.

dienest = dienestes 7553. 8674.
15474.

dienestliche(n) 226. 3259. 4619.

dient = gedient 9773.

diser, der zunächst folgende: in
disen zwein tagen 1726.

disunt (M: disint, W: dizhalb)
diesseits 7243.

dri, mine, ohne Bezeichnung des
Geldstückes (Verlust beim Spiel)
9824.

dringen, vür einen 3624.

drithalp 682.

dritteil, der werlde 10265.

drivalt 4126.

drumen reflex. 15184.

drungen = gedrungen ? 3612 (D).

due frzös., Herzog, s. liduc.

dulden, einen vür guot 2163.

dunken, an einem 5229. 5231;
mit blossem Genet. 5232.

durchgründen, ein Erlebnis mit
maeren 6495.

durchlegen. durchleit = s. v. a.
durchslagen 6384.

durchliuhtec 5450.

dus frzös. doux 1851 (W).

ebbetisse (aepitischen, abetissene)
Äbtissin 11800. 12361.

ebengelje s. evangelje.

eigen Eigentum 274. 1755. 11821.
15395; Untertan 9967.

eigenliche hän, als Eigentum
10573.

ein vor dem Vokativ 5122. 5165;
jener (uns schon bekanmnte)
158. 1046. 9097; einförmig,
ungeteilt, im Gegensatze zu
Geteiltem 6002 („sechzig von
