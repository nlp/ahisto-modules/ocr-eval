110 1435.
suas certi tenoris litteras concessit, quod extunc in antea extra dic-
tum opidum super quacunque etiam actione ad ullum iudicium
provinciale evocari deberent, nisi querulantibus in eodem opido
per iudicem, qui pro tempore foret, ibidem iustitia denegaretur,
prout in predictis litteris dicebatur plenius contineri, idemque
cardinalis et legatus attendens eorundem proconsulum, consulum
et communitatis devotionem sinceram, qua Romanam reverebantur
ecclesiam, eisdem proconsulibus, consulibus ac opidanis auctori-
tate legationis, qua fungebatur, per suas litteras concessit, quod
ipsi in causis ad iuditium seculare mero et pleno iure spectan-
tibus extra dictum opidum per iudices ecclesiasticos non trahe-
rentur, nisi cause essent tales, que de iure, iuris approbatione
aut prescriptione vel privilegio ad forum ecclesiasticum specta-
rent seu consvevissent in foro ecclesiastico tractari, apostolicis
etiam iudicibus exceptis, ac decrevit exnunc irritum et inane,
quidquid contra concessionem huiusmodi contingeret quomodolibet
attemptari, prout in eiusdem cardinalis et legati litteris plenius
continetur. Cum autem pro parte proconsulum, consulum et com-
munitatis predictorum asserentium, quod ipsi a tempore con-
cessionis et indulti cardinalis et legati huiusmodi illis pacifice
usi fuerunt et utuntur, de presenti nobis supplicatum fuerit, qua-
tenus, ut etiam ipsis super usu et observatione concessionis et
indulti eorundem potior cautela subministret, illis nostri officii
auctoritatem adiicere dignaremur, nos, qui ipsis proconsulibus,
consulibus et opidanis propter diversa virtutum opera, que in
fidei catholice defensione adversus hereticorum in regno Bohemie
constitutorum persequutiones et invasiones commendabiliter gesse-
runt, specialibus afficimur favoribus, huiusmodi supplicationibus
inclinati, concessionem et indultum cardinalis et legati huiusmodi
legationis, qua nos fungimur, auctoritate tenore presentium inno-
vamus illaque decernimus irrefragabiliter fore perpetuo obser-
vanda ac irritum et inane, quidquid contra ea contigerit quo-
modolibet attemptari. Datum Basilee die secunda marcii, anno
Domini millesimo quadringentesimo tricesimo quinto, indictione
tertia decima, pontificatus sanctissimi in Christo patris et
domini nostri, domini Eugeni, divina providentia pape quarti,
anno quarto.
[Na ohbu:] Johannes de Lobenstein.
