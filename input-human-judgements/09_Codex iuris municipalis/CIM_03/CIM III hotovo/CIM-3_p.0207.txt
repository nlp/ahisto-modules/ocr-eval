1437. 207
solidata fortunis, expediens arbitramur et congruum, ut simus
subiectis nostris in iusticia faciles et ad graciam liberales. Sane
pro parte fidelium nostrorum dilectorum magistri civium, consu-
lum, iuratorum tociusque communitatis civitatis nostre Veronensis
maiestati nostre supplex oblata peticio continebat, quatenus ipsis
universa et singula eorum privilegia, litteras, iura, gracias, li-
bertates et concessiones ipsis et eorum predecessoribus per nostros
predecessores Bohemie reges et duces, et signanter per dive me-
morie gloriosissimos quondam principes Wenceslaum secundum,
Johannem, avum, dominum Carolum, Romanorum imperatorem,
genitorem, et Wenceslaum fratrem, nostros carissimos, Bohemie
reges, super eorum iuribus, libertatibus, graciis et emunitatibus
ac iudicio et theloneo indulta et concessa approbare, innovare,
ratificare et confirmare graciosius dignaremur. Nos vero conside-
ratis attente multiplicibus probitatum meritis, quibus ipsi cives
Veronenses nobis et regno Bohemie studiose placuerunt et placere
poterunt fervencius in futurum, quanto largioribus graciarum
muneribus se senserint a nostra clemencia consolatos, eorum sup-
plicacionibus racionabilibus favorabiliter inclinati, animo delibe-
rato sanoque baronum, nostrorum et regni Bohemie fidelium, ac-
cedente consilio et de certa nostra sciencia auctoritate regia Bo-
hemie ipsis civibus et eorum successoribus et civitati Veronensi
universa et singula eorum privilegia, litteras, gracias et libertates
et concessiones ipsis a predictis nostris predecessoribus Bohemie
regibus et ducibus data et concessa ac datas et concessas, in omni-
bus et singulis eorum sentenciis, clausulis, articulis verborum ex-
pressionibus atque punctis, ac si tenores omnium presentibus de
verbo ad verbum forent inserti, approbavimus, innovavimus, ra-
tificavimus et confirmavimus graciose, approbamus, innovamus,
ratificamus et tenore presencium benignius confirmamus, decer-
nentes expresse ea et eas obtinere perpetui roboris firmitatem. In-
super quoniam certam informacionem recepimus, qualiter cives
Veronenses a nostris predecessoribus, Bohemie regibus, libertatem
habuerunt in silva Huorka, dicta Tocznik, penes montem Plessi-
wecz venandi et capiendi lepores et perdices, quam quidem liber-
tatem venacionis eisdem civibus confirmamus. Item, prout ab
antiquo est observatum, ita et nos ipsis Veronensibus civibus et
civitati concedendo confirmamus et approbamus, quod infrascripte
