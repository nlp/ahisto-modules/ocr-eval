1436. 121
tečtí, »že drží Vrútek a Stankovice, a Podbořan a Libošovic nedrží, a že je drží
Burian.« (Arch. Č. II, 191).
* Čís. 77.
1436, 25. září. V Praze.
Císař Sigmund potvrzuje purkmistru, konšelům i vší obci
města Chrudimě výroční trh na den nanebevzetí panny Marie.

Sigismundus, dei gracia Romanorum imperator semper au-
gustus ac Hungarie, Boemie, Dalmacie, Croacie etc. rex. Notum
facimus tenore presencium, quibus expedit, universis, quod habi-
to respectu ad grata serviciorum studia ac constantis devoci-
onis puritatem fidelium nostrorum dilectorum magistri civium,
consulum ac tocius comunitatis civitatis nostre Chrudim, quibus
serenitati nostre multa solicitudine magnoque studio placuerunt,
continue placent et placere poterunt uberius in futurum, ipsis
non per errorem aut inprovide, sed animo deliberato, sano fi-
delium nostrorum accedente consilio et ex certa nostra sciencia
indulsimus hancque graciam fecimus et virtute presencium auc-
toritate regia Boemie facimus specialem, ut exnunc in antea
perpetuis temporibus annis singulis semel in anno circa festum
asssumpcionis sancte Marie usque ad septimum diem inclusive
nundine sive annale forum in civitate ipsorum Chrudim obser-
vari et celebrari valeant atque possint; quodque dicte nundine
sive annale forum ac universi et singuli mercatores et quelibet
alie persone ipsas visitantes, ibidem morantes et ab inde rece-
dentes omnibus iuribus, libertatibus, graciis, emunitatibus, con-
ductu, pace, tuicione prefatique cives de Chrudim theloneis, mu-
tis ac aliis consuetudinibus et iuribus utantur et gaudeant, qui-
bus nundine et annalia fora aliarum civitatum et opidorum in
regno nostro Boemie dicte civitati vicinarum nec non homines
ad eas accedentes, ibidem morantes ac vice versa ab inde rece-
dentes utuntur et gaudent quomodolibet, consuetudine vel de iure.
Inhibentes universis et singulis principibus ecclesiasticis et se-
cularibus, baronibus, nobilibus, militibus, clientibus, capitaneis,
officialibus, magistris civium, consulibus, scabinis, iuratis et com-
munitatibus ac civitatum, opidorum, villarum, castrorum et lo-
