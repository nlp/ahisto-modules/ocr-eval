380 D. V. Wýpisky práwní a saudní

152.
1437, 28 Jan. Quarto Matthias, A. 2.
»Temporibus Sigismundi Imperatoris.«
Anno domini MCCCCXXXVII, fer. se
cunda ante Purificat. Mariae, sermus prin-

ceps et dominus, D. Sigismundus Rom.
Imp. semper Augustus, Hungariae, Bo-
hemiae, Dalmatiae, Croatiae rex etc. fecit
et constituit beneficiarios Pragenses ma

jores et minores apud tabulas terrae, vi-

delicet D. Alšonem de Sternberg et de
Holic supremum camerarium, D. Nicolaum

Zajiec de Hasenburg et de Kosti supr.

judicem, Alšonem de Risenburg protono-

tarium; ad quae beneficia et officia sunt
de consensu dominorum baronum in IV
temporibus XLmae proximis voce publica
proclamati et přiwoláni coram terrigenis.

El infrascripta gesta terrigenarum acta

sunt et conscripta per me Matthiam de
Chřenow, vicenotarium tabularum terrae,

tempore ipsorum officii.
Domini Barones invenerunt in pleno
judicio, et Petrus de Zwieřetic et Hynko
de Črwenéhory dominorum gestum ex
porlaverunt:
Tak jakož jest na tom zuostáno na
ubecném sněmu, w kollegi in festo S.
Wenceslai nunc praeterito, Ciesařowú
Milostí a pánem p. Sigmundem Českým
králem, pány, rytieři, panošemi, zemany,

městy, aby wšecka zbožie mocí wzatá a

odjatá byla nawrácena, holdowé wšichni

aby byli prázdni, šacuňkowé newzatí aby
bráni nebyli: že to ustanowenie a pro-
wolánie jest mocné a má před sě jíti. A
ktož jest to přestúpil, anebo toho neuči-

nil, aby to okázal před Cies. Mti a před

pány w lawiciech na suché dni postnie

nynie najprw příštie, proč jest to usta-

wenie a prowolánie přestúpil; a ktož se

z toho řádně před pány newywede, ten

trp, což páni najdú.

Item o wězeniech, pro quibus locu-

tus est coram sermo principe et domino

D. Sigismundo etc. et dominis baronibus

Diwišius de Miletinka, videlicet pro Ab-

solone de Chřič et Johanne Manda de

Zbyslawě, kteříž jsú nařčeni falšem a sta-

weni pro listowý faleš na práwo purkrabí

Pražským, aby na rukojmě dáni byli: do-

mini in pleno judicio invenerunt, et Petrus

de Zwieřetic et Zbynko Zajiec de Hasen-

burg dominorum cosilium exportaverunt:

že takowí wězňowé pro takowé nařčenie

nemají dáni býti na rukojmě, než posta-

weni mají býti k súdu před pány. Actum

ut supra.

Item o ty listy, kteréž sú sobě w této

zemi páni , rytieři, panoše a zemunė

w těch časiech nepokojných, dokudž desk

zemských nebylo w swém řádu, dělali,

a swé zbožie a platy prodáwali a zapi-

sowali do wloženie we dsky zemské:

domini in pleno judicio invenerut pro

jure: že takowí listowé a zápisowé, kte-

říž jsú řádně a s wědomím panským, ry-

tierským neb panošským pod jich pečetmi

psáni aneb děláni, že takowí listowé moc

mají, na nichž jsú jistcowé žiwi, aby sobě

ty listy a zápisy we dsky zenské kladli

wedle řádu a práwa země této, počnúc

od tohoto času na prwé, na druhé, na

třetie, a konečně na čtwrté suché dni

pořád zběhlé etc. Pakliby jistci pomřeli etc.
