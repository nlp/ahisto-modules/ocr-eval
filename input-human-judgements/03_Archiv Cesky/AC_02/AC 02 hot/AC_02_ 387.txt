ze starých desk zemských králowstwí Českého r. 1437. 387

ke dskám od JMti poslán, a také usly-
šewše list swědomie úředników menších
desk zemských, že těch třidceti kop platu

na swrchupsaném zboží kláštera Plasského

jest jemu Prokopowi s powolením králo-

wým we dsky zemské wložen a wepsán,
a odpor páně Hanušuow slyšewše, že má
od Ciesařowy Mti list s majestátem na
swrchupsané zbožie Plasské; a poněwadž

jest Prokopuow trh a kúpenie staršie spo-

wolením swrchupsaným, nežli list s ma-
jestátem páně Hanušów, nalezli sú za

práwo a pan Hynek z Čerwené hory a
pan Jaroslaw Plichta z Žirotína panský
potaz wynesli, že swrchupsaný Prokop má
ostati při swrchupsaném platu wedle swých

listuow, a pan Hanuš jemu toho hájiti ne-
má. Actum an. dom. M°CCCC°XXXVII°,

fer. IIII post Oculi. Trebon. 405b. Mus.

175b. Talmb, 76b.

168.

1437, 26 Febr.

Juxta obligationem, ubi Maršo de
Braškow obligavit in majoribus obligatio-
num Benešio de Wrbna, et Benešius ulte-
rius etc. Ibi pueri praedicti Benešii pe-

tiverunt camerarium a dominis super in-

tromissione hereditatum in Braškow. Juxta
eandem obligationem, quae testatur patri
eorum, invenerunt pro jure, et Jaroslaus
Plichta de Žirotin et Hanušius de Kolo-

wrat dominorum consilium exportaverunt:

že jakož stoji zápis Benešowi z Wrbna,

aby sě mohl komorníkem uwázati podlé
zápisu, a toho uwázanie učiniti nemohl

w těchto nepokojích, neb desk nebylo:
že to sirotkóm Benešowým škoditi nemá,

že má jim komorník wydán býti na uwá-

zánie podle jich desk. Anno domini

M°CCCC°XXXVII°, feria III post Remi-

discere, praesentibus dominis baronibus

in pleno judicio, videlicet serenissimo

principe domino domino Sigismundo Ro-

manorum imperatore et Bohemiae rege,

Alšone de Sternberg supremo camerario,

Nicolao de Kosti supremo judice, Alšone

de Risemburg protonotario tabularum,

Menhardo de Novadomo supremo pur-

gravio castri Pragensis, Ulrico de Rosem-

berg, Alberto de Koldic etc. (videantur

juxta obligationem in tabulis omnes qui

judicio praesiderunt, dempto uno, vide-

licet Georgio de Kunstat, quia tunc tem-

poris non interfuit,) item Hanušio de Ko-

lowrat, Nicolao de Borotin, Jaroslao Plichta

de Žirotin, Johanne de Stráže, et famo-

sis clientibus Unka de Neustupowa, Sti-

borio de Wolfstein, Alšone Hurt de Pozd-

nie, Zawišio de Jimlína, Wilhelmo Chrt

de Zahrádky, Nicolao de Ledče, Petro

de Plesu et Matthia Holec de Nemošic,

hi omnes interfuerunt. Treb. l. c. Mus. 176b.

169.

1437, 8 Mart. Quarto Matthiae, A. 27.

Serenissimus princeps et dominus,

D. Sigismundus Roman. Imp. et Bohe-

miae rex, cum dominis baronibus in pleno

judicio, ustanowili a za právo nalezli, et

Petrus de Zwieřetic et Jaroslaus Plichta

de Žirotin dominorum consilium expor-

taverunt:

O wšecky listy , že mnoho jest li-

stuow starych i nowych w této zemi:

49*
