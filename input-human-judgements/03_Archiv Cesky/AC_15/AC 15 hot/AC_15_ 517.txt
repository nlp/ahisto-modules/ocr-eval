z let 1409-1437.
517
6.
Ctibor od desk, perkmistr hor vinničných, k žádosti Vaňka bratra Martina někdy mistra
špitálského, dává vepsati do knih vinničných majestát císaře Zikmunda daný v Praze dne 6.
března 1437, kterýmž císař v 90 kopách gr. č. zapisuje témuž Martinovi vinnice kláštera
Kartouského.
1437, 6. března. — I. A. 29.
Známo buď všěm, kterak Vaněk, bratr Martina špitálského, přišed před mě i žádal
jest a prosil, aby majestát Sigmunda z božie milosti Římského ciesařie vzdy rozmnožitele říšě,
Uherského a Českého krále etc, kterýžto dal jest a učiniti kázal na vinnici někdy Zumbur-
keřstinu mnichóv Kartúských, jenž leží vedle Srpovic hory s jedné a Húžvicové strany druhé,
aby byl vepsán v knihy vinničné. Tehdy já Ctibor od desk, tehdy pergmistr hor vinničných,
jsa nakloněn k jeho hodným žádostem, i kázal sem jej vepsati v knihy vinničné; kterýžto
majestát stojí slovo od slova takto:
My Sigmund z božie milosti římský ciesař, vzdy rozmnožitel říšě, a uherský,
český, dalmatský, charvatský etc král, vyznáváme tiemto listem obecně přede všemi,
ktož jej uzřie anebo čtúce slyšeti budú, že přišěd před naši velebnost věrný náš
milý Vaněk, bratr Martinóv někdy mistra špitalského, zpravil nás, kterak jest od téhož
bratra svého vinnici kněží Kartúských, kteráž leží mezi vinnicemi Srpovic s jedné
a Húžvicové strany druhé, kúpil i zaplatil, proše nás, abychom jemu toho trhu
a vinnice ráčili potvrditi milostivě. A my patřiece k rozličným téhož Vaňka pohodlím
i službám, jimižto se jest nám zachoval i potom se zachovati miení, s dobrým roz-
myslem a radú naší k trhu a k prodaji vinnice svrchupsané vóli naši a povolenie při-
dali sme i přidáváme, jemu a jeho dědicóm budúcím toho mocí královskú v Čechách
potvrdili sme a potvrzujem tiemto listem, právo jim i moc plnú dávajíce, aby tu
vinnici jměli, držali i jie požívali bez našie i našich budúcích, králóv českých, i jiných
všěch každé odpory i přiekazy. Toto znamenitě z milosti našie zvláštnie přidávajíce,
kdyžbychom koli my, naši budúcí králové čeští, neboli ti, jimž by ta výplata spra-
vedlivě příslušala, chtěli tu vinnici k sobě snad obrátiti a ji jmieti, že oni jie ižád-
nému postúpiti vinni nebudú ani mají, donižby jim devaddesáti kop grošóv pražských
střiebrných a dobrých dáno a beze lsti zaplaceno nebylo peniezi hotovými, a nad to
užitky všěcky té vinnice týž Vaněk neb jeho dědicové a budúcí mají sobě sebrati
ku penězóm svrchupsaným, dřieve nežli jie postúpie. A ktož tento list mieti bude
s častopsaného Vaňka neb jeho dědicóv a budúcích dobrú volí a svobodnú, ten má
též právo i moc jmieti ke všěm věcem v tomto listu položeným a zapsaným, jakožto on
sám. Protož přikazujemy podkomořiemu, pergmistróm, městóm Pražským i jiným
všem našim a královstvie našeho Českého úředníkóm, věrným milým, nynějším i budúcím,
svitským i duchovním přísně a pevně, aby napřed jmenovanému Vaňkovi a jeho dědicóm
i budúcím na jmění, držení, vládnutí i požívání vinnice svrchupsané nepřekáželi ani
překážeti dopustili, ale radějše je, bude-li potřebie, při tom vzbraňovali a všelikak
