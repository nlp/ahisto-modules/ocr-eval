Paběrky písemností wšelikých z let 1421—1438.
441
a raddú naší městečku Jilowému swrchupsanému a měšťannom i bydliteluom jeho
wšech jich práw horničích, swobod, řáduow, milostí i obyčejuow jim od slawné pa-
měti předkuow našich králuow Českých daných a od týchž měšťan z starodáwna
z obyčeje nebo práwa požíwalých, jichžto i jiní také horníci w zemi České požíwali
sú a požíwají, obnowili a potwrdili sme, i tímto listem mocí císařskú a králowskú
v Čechách obnowujem a potwrzujem milostiwě tak w cele a plně, jakoby ta wšecka
práwa, wšecky swobody a milosti, i wšickni řádowé a dobří obyčejowé swrchupsaní
byla tuto a byli všecka a wšickni skrz od slowa k slowu wepsána a wepsáni. To
znamenitě jmenem našich i našich budúcích králuow Českých k wěčnosti a znowu
tak wyjasňujíce, jakož i dříwe z dáwna bylo jest: aby městečko již jmenowané
a měštané i bydlitelé jeho všech úrokuow, daní, berní i každých poplatkuow dě-
dinných prázdni wěčně byli a swobodni, nám ani našim budúcím králuom Českým
nikoli jich nejsúce zawázáni plniti nebo dlužni, kteréžto owšem což dědin se dotýče,
mocí naší swrchudotčenú swobodili sme a swobodíme konečně tímto listem. Dále
tak chcem a k wěčnému tak držení ustawujem, aby wšickni horníci, kteříž dělají
nebo dělati budúcně budí tudíž w Jilowém neb wokolo na horách, na rýžích, na
potocích, na řekách i w Kníně wšecko zlato cožkoli ho dobýwají a dobýwati budú,
nosili k úředníku od nás nebo našich budúcích králuow Českých usazenému do Ji-
lowého, a ten úředník aby nám a komoře králowské wěrně je dáwal, jakož práwo
jest i obyčej z starodáwna. A oni také Jílowští měšťané nynější i budúcí aby
súduow a práw swých k sobě i k jiným mohli a moc i práwo jměli požíwati, jakož
sú za předkuow našich králuow Českých zdáwna požíwali zuobyčeje nebo práwa.
Protož wšem úředníkuom, purkrabím i jiným našim a králowstwí našeho Českého
poddaným, wěrným našim milým, nynějším i budúcím přikazujem aby swrchupsaným
měšťanuom Jílowským w jich práwich, swobodách, řádích a obyčejích nahoře po-
ložených nepřekáželi ani překážeti nedopúštěli, jakož našeho i našich budúcích krá-
luow Českých welikého rozhněwání chtí ujíti. Toho na potwrzení náš císařský ma-
jestát kázali sme přiwěsiti k tomuto listu. Jenž jest dán w Praze, léta od narození
syna buožího tisícého iiic xxxvii, den sn Stanislawa biskupa a mučedlníka buožího,
lét králowstwí našich Uherského etc. li, Římského xxvii, Českého w xvii a císařstwí
w čtwrtém létě.
Ad relacionem domini Johannis de Ryzmberk
de Rábí Johannes Tušek.
56
