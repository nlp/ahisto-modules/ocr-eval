Paběrky písemností wšelikých z let 1421—1438.
411
mnohé a rozličné nesnáze a zádawy i lúpeže a bezpráwie, kteréž běží v tomto krá-
lowstwie, i jiné nehody, o němž nám jest rady a sjezdu velice potřebie: sněm
w Nimburce učiniti obecný den S. W. ježto nynie najprv přijde uložili sme, na
němž o všechny takowé pilné nedostatky země, a zwláště o zwelebení zákona bo-
žieho má mluweno býti. Protož wás prosíme a pro obecné dobré žádáme, napomí-
najíce, abyste tu na ten jistý den ráčili býti, anebo swú radu, jakož z práwa máte,
na to učiniti, abyšte ráčili dwa z konšelów a dwa z obce s plnú mocí wydati, zda-
liby pán bóh ráčil nám pójčiti w to uhoditi, kakby ta země w řád, w jednotu,
w milost a w lásku božskú mohla uvedena býti. Pakliby kto wěda to a moha tu
nebyl, tehdy bychom jiného newiděli nežli to, že jemu zwelebenie zákona božieho
a obecné dobré nenie milo.

14.
Pražané dáwají pánům a městům kraje Plzenského bezpečný gleit na 4 neděle, aby k sněmu
Swatohawelskému do Prahy přijeti mohli.
W Prase, m. Oct. 1423. (Z téhoš rkp. čílo 307.)
My purkmistr etc. wyznáwáme tiemto listem etc. že sme dali a mocí tohoto
listu dáwáme urozeným rytieřóm a panošem, Henrichowi z Plawna jinak z Kungs-
wardu, Fridrichowi z Kolowrat seděním na Libštyně, Janowi z Rizmberka seděním
na Šwihowě, Wilémowi z Rizmberka, Hynkowi ze Šwamberka etc. a purkmistróm,
konšelóm obecným Nowého Plzně, Střiebrského, Tachowského, Týna Horšowského
měst, i jiným wšem zemanóm, rytieřóm a panošem kraje a lantkraje Plzenského náš
jistý swobodný a bezpečný gleit, i jich všem, ktož s nimi a wedle nich k tomu
roku a sjezdu obecnému přijedú, kterýž má nynie den S. Hawla nynie najprwé
příštie u nás v městě Pražském býti, od dánie tohoto listu za čtyři neděle pořád
počítajíc před mocí i před práwem bez přerušenie trwající, k nám k tomu jistému
sněmu přijeti, s námi přebyti a rozmluviti, a od nás sě zasě swobodně bez úrazu
osob, zbožie i wěcí jich wšech do swých přiebytków nawrátiti, za nás, za wšecky
naše i za ty, ktožby pro nás chtěli učiniti nebo nechati, dobrú naší wěrú a bezewšie
zlé lsti; slibujem tento jistý gleit zdržeti a zachowati beze wšeho přerušenie swrchu-
psaným osobám i jich wšem, kteříž s nimi a wedle nich v té mieře pojedú, až do
času swrchupsaného, s takowúto wymienkú, aby ižádného měštěnína našeho wyběh-
lého nebo pro dobré příčiny od nás wyhnaného s sebú nepojímali, neb takowého
gleitu nedáwáme. Datum etc.
52*
