obecní, r. 1419. 377

Thame Luticz.
Nykl von Blastorff.
Jurge von Blastorff.
Caspar von Pankow.
Reyncz von Budissin.
Reyncz der ander von Budissin.
Jan von Luticz.
Nykl von Luticz.
Horek von Luticz.
Jurg von Kopricz.
Nykl von Luticz.
Henrich von Panewicz der Junger.
Jan von Korbis.
Gabriel Soběhrd.
Nykl von Torczensdorff.
Heyncze von der Polsnicz.
Otto Czeczwicz.
Henrich Rodwicz.
Fridrich von Meczerad.
Francz Meczerad.

Fridrich von Meczerad.
Hanuss Knapp
Bernhard Rodwicz.
My Wáclaw z Dubé z Leštna.
Henrich z Elsterberka.
Jan z Těchlowic.
Petr Hlas a WÍtek.
Wáclaw z Moraw seděním na Borku.
Petr z Sulowic odjinud z Řehlowic.
Hanuš Polenck mladší.
Mikuláš z Mlýnec řečený Bronec.
Skrip z Žichowa.
Heralt z Hrbowic.
Otík z Skřipa z Ščohla.
Mikuláš Šerméř z Prahy.
Janek z Krásného dwora.
Fridrich Lange.
Janek z Božeowic.
Petr Luta z Draškowic.

Já Beneš z Wartemberka odjinud z Děčína.
Já Jaroslaw z Dubé seděním na Mišteně (sic).
Petr z Warlowic.
Janek Uhr mladý.
Rynek z Drachlowy.
Já Fridrich a Hanuš bratřie z Kolowrat seděním na Liwšteině a na Krašowě.
Já Jindřich Berka z Dubé řečený Hlawáč seděním na Jestřebie.
Já Jan z Sulejwic.
Já Jan Ptaček z Pirknšteina seděním na Ratajiech.
My richtář, purgmistr, konšelé i wšecka obec města Kúřimského.
Já Franc z Rosentala hofinistr na Horách.
Já Jindřich Hlawáč Berka z Dubé seděním na Jestřebie.
Hynek z Klinšteina.
Jindřich z Mnichowa.
Ctibor ze Chcebuzi,
Janek ze Chcebuzi.
Jachman z Mnichowa,
Bohuslaw z Chlumu i se wšemi našemi pacholky.
