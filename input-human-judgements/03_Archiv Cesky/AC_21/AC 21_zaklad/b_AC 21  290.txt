290 A. XXXIV. Druhý dodavek k dopisům rodu Rosenberského
25.
Jan hrabě ze Šaumburka Oldřichovi z Rožmberka: o průvod pro svého zetě Ladislava z Maroth
do Jindřichova Hradce. — Ve Vídni 3. září 1437.
Edeler lieber sweher vnd freund, vnser freuntlich dinst beuor. Als der edel
vnser lieber sun vnd aydem her Ladissla von Maroth ytz zu Egenburg vnd weg-
uerttig ist, zu vnserm allergnedigisten hern dem kayser gen Brag zu ziehen, der
aber gewornt ist, wie im vnderwegen schüll fürgehallten werden: bitt wir ewr freunt-
schaft mit gantzem fleiss, ir wellet demselben vnserm sun ettleich der ewrn zue-
schiken, die in dy strassen bis gein Newhaus zu belaitten helffen, vnd ob er der-
selben der ewrn verrer wurd begern, indes dann vmb vnsern willen auch nicht
zu vertzeihen. Daran beweist ir vns sölh freuntschaft, dy wir gen ew vnd den ewrn
auch dinstlich wellen verschulden, wenn sich das gepüret. Geben zu Wienn an eritag
nach Egidi anno domini millesimo quadringentesimo tricesimo septimo.
Johanns graue zu Schawnberg lanndmarschall in Osterreich.
Dem edeln vnserm lieben sweher vnd freunde hern Vlrichen von Rosemberg.
Orig. pap. v Třeboň. arch. hist. Nr. 394. - (A.)

26.
Jan hrabě ze Šaumburka Oldřichovi z Rožmberka: o domnělém smíru jeho se Smilem
z Křemže čili z Prachatic. — Ve Vídni 8. září 1437.
Edler lieber sweher vnd freunde, vnser freuntlich dinst beuor. Als ir vns
geschriben habt von frides wegen mit dem Zmyl von Brachaditz aufzenemen, also
hat vnser genediger her der herczog yns ew vnd auch vnserm lieben sweher dem
von Wallsse geschriben, denselben brif wir ew hiemit sennden, wol vernemen werdet
vnd darauf ewr freuntschaft also frid aufgenemen mag. Als ir dann begert ew aus
genotigen sachen mit vns zu vnderreden, nu haben wir willen, nach sand Michels
tag churczlich daoben ze sein vnd dann versuhen vns vor vnser widerrais herab
zu einander zu fugen. — Gebn zu Wienn an unser lieben frawn tag nat. Anno
etc XXXVII . Johanns Graue zu Schawnberg landmarschalh in Osterrich.
Dem edlen vnserm lieben sweher vnd freunde hern Vlrichen von Rosenwerkg.
Orig. pap. v Třeboň. arch. hist. Nr. 241 p. — Výklad k tomuto dopisu viz ve Wagnerově rozpravě:
Jan Smil z Křemže, v ČČMus. 1888 str. 180. — (A.)

27.
Purkmistr a rada města Vodňan Janovi z Kraselova, purkrabí na Helfmburce: o lámání ka-
mene vápenného, o jízdě s Táborskými etc. — B. m. 4. prosince (1437?).
Služba našě napřěd času přímirného, pane purkrabie! Jakož nám píšoš o lá-
mání kamene váponného, že jsmy o to neobeslali páně Mti etc: mnohéť časy lámámy
