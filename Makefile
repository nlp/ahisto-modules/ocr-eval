.PHONY: import-sql-dump remount-nymfe remount-epimetheus install-ocrd write-ocrd-tasks

SHELL = /bin/bash

NICENESS = 10

DETECTED_LANGUAGES = ces+deu+lat

LANGUAGE_DETECTION_CODE = $(SCRIPT_DIRNAME)/language_detection
LANGUAGE_DETECTION_ANNOTATIONS = $(SCRIPT_DIRNAME)/language_detection_annotations

TESSERACT_RUN = $(TESSERACT)
TESSERACT_OPTIONS = --psm 3 --tessdata-dir $(TESSERACT_DATA) $(TESSERACT_OUTPUT_FORMATS)
TESSERACT_OPTIONS_OCRD = -P␣sparse_text␣false␣-P␣model␣$(DETECTED_LANGUAGES)
TESSERACT_OPTIONS3 = --oem 0 $(TESSERACT_OPTIONS)
TESSERACT_OPTIONS3_OCRD = -P␣tesseract_parameters␣\{\\\"tessedit_ocr_engine_mode\\\":\\\"0\\\"\}␣$(TESSERACT_OPTIONS_OCRD)
TESSERACT_OPTIONS4 = --oem 1 $(TESSERACT_OPTIONS)
TESSERACT_OPTIONS4_OCRD = -P␣tesseract_parameters␣\{\\\"tessedit_ocr_engine_mode\\\":\\\"1\\\"\}␣$(TESSERACT_OPTIONS_OCRD)
TESSERACT_OPTIONS34 = --oem 2  $(TESSERACT_OPTIONS)
TESSERACT_OPTIONS34_OCRD = -P␣tesseract_parameters␣\{\\\"tessedit_ocr_engine_mode\\\":\\\"2\\\"\}␣$(TESSERACT_OPTIONS_OCRD)
TESSERACT = tesseract
TESSERACT_DATA = $(SCRIPT_DIRNAME)/tessdata
TESSERACT_OUTPUT_FORMATS = hocr txt tsv makebox
TESSERACT_TIMEOUT = 600

DOCKER_RUN = docker run --rm

CALAMARI_MODELS = $(SCRIPT_DIRNAME)/calamari_models
OCRD_WORKSPACE_DIRNAME = /var/tmp/ocrd-workspace
OCRD_WORKSPACE_DIRNAME_SOURCE = mir:/mnt/nvme-storage/novotny/ocrd-workspace
OCRD_TASKS = $(SCRIPT_DIRNAME)/ocrd-tasks
OCRD_CLEANUP = rm -rf $(CURRENT_DIRNAME){mets.xml,ocrd.log,*/}
OCRD_IMPORT = $(OCRD_RUN) nice -n $(NICENESS) ocrd-import -P -i -r 300 /data
OCRD_PROCESS = $(OCRD_RUN) nice -n $(NICENESS) ocrd process $(subst ␣, ,$(CURRENT_STEP))
OCRD_PIPELINE = \
  "olena-binarize␣-I␣OCR-D-IMG␣-O␣OCR-D-BIN␣-P␣impl␣sauvola" \
  "anybaseocr-crop␣-I␣OCR-D-BIN␣-O␣OCR-D-CROP" \
  "olena-binarize␣-I␣OCR-D-CROP␣-O␣OCR-D-BIN2␣-P␣impl␣kim" \
  "cis-ocropy-denoise␣-I␣OCR-D-BIN2␣-O␣OCR-D-BIN-DENOISE␣-P␣level-of-operation␣page" \
  "cis-ocropy-deskew␣-I␣OCR-D-BIN-DENOISE␣-O␣OCR-D-BIN-DENOISE-DESKEW␣-P␣level-of-operation␣page" \
  "tesserocr-segment-region␣-I␣OCR-D-BIN-DENOISE-DESKEW␣-O␣OCR-D-SEG-REG" \
  "segment-repair␣-I␣OCR-D-SEG-REG␣-O␣OCR-D-SEG-REPAIR␣-P␣plausibilize␣true" \
  "cis-ocropy-deskew␣-I␣OCR-D-SEG-REPAIR␣-O␣OCR-D-SEG-REG-DESKEW␣-P␣level-of-operation␣region" \
  "cis-ocropy-clip␣-I␣OCR-D-SEG-REG-DESKEW␣-O␣OCR-D-SEG-REG-DESKEW-CLIP␣-P␣level-of-operation␣region" \
  "cis-ocropy-segment␣-I␣OCR-D-SEG-REG-DESKEW-CLIP␣-O␣OCR-D-SEG-LINE␣-P␣level-of-operation␣region" \
  "segment-repair␣-I␣OCR-D-SEG-LINE␣-O␣OCR-D-SEG-REPAIR-LINE␣-P␣sanitize␣true" \
  "cis-ocropy-dewarp␣-I␣OCR-D-SEG-REPAIR-LINE␣-O␣OCR-D-SEG-LINE-RESEG-DEWARP" \
  "calamari-recognize␣-I␣OCR-D-SEG-LINE-RESEG-DEWARP␣-O␣OCR-D-OCR␣-P␣checkpoint␣$(OCRD_CALAMARI_MODELS)" \
	"fileformat-transform␣-I␣OCR-D-OCR␣-O␣OCR-D-TEXT-CALAMARI␣-P␣from-to␣page\␣text" \
	"fileformat-transform␣-I␣OCR-D-OCR␣-O␣OCR-D-HOCR-CALAMARI␣-P␣from-to␣page\␣hocr" \
  "tesserocr-recognize␣-I␣OCR-D-SEG-LINE-RESEG-DEWARP␣-O␣OCR-D-OCR3␣␣$(TESSERACT_OPTIONS3_OCRD)"  \
  "tesserocr-recognize␣-I␣OCR-D-SEG-LINE-RESEG-DEWARP␣-O␣OCR-D-OCR4␣␣$(TESSERACT_OPTIONS4_OCRD)"  \
  "tesserocr-recognize␣-I␣OCR-D-SEG-LINE-RESEG-DEWARP␣-O␣OCR-D-OCR34␣$(TESSERACT_OPTIONS34_OCRD)" \
	"cis-align␣-I␣OCR-D-OCR,OCR-D-OCR3␣␣-O␣OCR-D-ALIGN3"  \
	"cis-align␣-I␣OCR-D-OCR,OCR-D-OCR4␣␣-O␣OCR-D-ALIGN4"  \
	"cis-align␣-I␣OCR-D-OCR,OCR-D-OCR34␣-O␣OCR-D-ALIGN34" \
	"fileformat-transform␣-I␣OCR-D-OCR3␣␣-O␣OCR-D-TEXT3␣␣-P␣from-to␣page\␣text" \
	"fileformat-transform␣-I␣OCR-D-OCR3␣␣-O␣OCR-D-HOCR3␣␣-P␣from-to␣page\␣hocr" \
	"fileformat-transform␣-I␣OCR-D-OCR4␣␣-O␣OCR-D-TEXT4␣␣-P␣from-to␣page\␣text" \
	"fileformat-transform␣-I␣OCR-D-OCR4␣␣-O␣OCR-D-HOCR4␣␣-P␣from-to␣page\␣hocr" \
	"fileformat-transform␣-I␣OCR-D-OCR34␣-O␣OCR-D-TEXT34␣-P␣from-to␣page\␣text" \
	"fileformat-transform␣-I␣OCR-D-OCR34␣-O␣OCR-D-HOCR34␣-P␣from-to␣page\␣hocr" \
	"cis-postcorrect␣-I␣OCR-D-ALIGN3␣␣-O␣OCR-D-CORRECT3␣␣-P␣profilerPath␣$(OCRD_CIS_POSTCORRECT_PROFILER)␣-P␣profilerConfig␣ignored␣-P␣nOCR␣2␣-P␣model␣$(OCRD_CIS_POSTCORRECT_MODEL)" \
	"cis-postcorrect␣-I␣OCR-D-ALIGN4␣␣-O␣OCR-D-CORRECT4␣␣-P␣profilerPath␣$(OCRD_CIS_POSTCORRECT_PROFILER)␣-P␣profilerConfig␣ignored␣-P␣nOCR␣2␣-P␣model␣$(OCRD_CIS_POSTCORRECT_MODEL)" \
	"cis-postcorrect␣-I␣OCR-D-ALIGN34␣-O␣OCR-D-CORRECT34␣-P␣profilerPath␣$(OCRD_CIS_POSTCORRECT_PROFILER)␣-P␣profilerConfig␣ignored␣-P␣nOCR␣2␣-P␣model␣$(OCRD_CIS_POSTCORRECT_MODEL)" \
	"fileformat-transform␣-I␣OCR-D-CORRECT3␣␣-O␣OCR-D-TEXTC3␣␣-P␣from-to␣page\␣text" \
	"fileformat-transform␣-I␣OCR-D-CORRECT3␣␣-O␣OCR-D-HOCRC3␣␣-P␣from-to␣page\␣hocr" \
	"fileformat-transform␣-I␣OCR-D-CORRECT4␣␣-O␣OCR-D-TEXTC4␣␣-P␣from-to␣page\␣text" \
	"fileformat-transform␣-I␣OCR-D-CORRECT4␣␣-O␣OCR-D-HOCRC4␣␣-P␣from-to␣page\␣hocr" \
	"fileformat-transform␣-I␣OCR-D-CORRECT34␣-O␣OCR-D-TEXTC34␣-P␣from-to␣page\␣text" \
	"fileformat-transform␣-I␣OCR-D-CORRECT34␣-O␣OCR-D-HOCRC34␣-P␣from-to␣page\␣hocr"
OCRD_CIS_POSTCORRECT_DIRNAME = $(SCRIPT_DIRNAME)/ocrd-cis-postcorrect
OCRD_CIS_POSTCORRECT_MODEL = /build/ocrd_cis/ocrd_cis/data/model.zip
OCRD_CIS_POSTCORRECT_PROFILER = /ocrd-cis-postcorrect/profiler.bash
OCRD_CALAMARI_MODELS = /models/\*.ckpt.json
OCRD_IMAGE = ocrd/all:maximum
OCRD_RUN = $(DOCKER_RUN) -u `id -u` $(OCRD_MOUNTS) $(OCRD_ENVS) -- $(OCRD_IMAGE)
OCRD_MOUNTS = -v $(CURRENT_DIRNAME):/data -w /data -v $(CALAMARI_MODELS):/models -v $(TESSERACT_DATA):/tessdata -v $(OCRD_CIS_POSTCORRECT_DIRNAME):/ocrd-cis-postcorrect
OCRD_ENVS = --env TESSDATA_PREFIX=/tessdata
OCRD_DIRNAMES = $(wildcard $(OCRD_WORKSPACE_DIRNAME)/*/)

WAIFU2X_RUN = $(DOCKER_RUN) --gpus all $(WAIFU2X_MOUNTS) -- $(WAIFU2X_IMAGE) th /root/waifu2x/waifu2x.lua $(WAIFU2X_OPTIONS)
WAIFU2X_OPTIONS = -force_cudnn 1 -model_dir /model -m noise_scale -scale 2
WAIFU2X_MODEL = $(WAIFU2X_DATA)/models/anime_style_art_rgb
WAIFU2X_MOUNTS = -v $(DOWNSCALED_INPUT_DIRNAME):/images -w /images -v $(WAIFU2X_MODEL):/model -v $(SCRIPT_DIRNAME):/script
WAIFU2X_DATA = $(SCRIPT_DIRNAME)/waifu2x
WAIFU2X_IMAGE = nagadomi/waifu2x:latest
WAIFU2X_LOW_NOISE = 0
WAIFU2X_HIGH_NOISE = 3

POTRACE_RUN = convert $</{} pnm:- | mkbitmap | potrace $(POTRACE_OPTIONS) | convert pgm:- png:- > $@/{}
POTRACE_OPTIONS = -g -W $(POTRACE_IMAGE_WIDTH) -H $(POTRACE_IMAGE_HEIGHT) -o -
POTRACE_IMAGE_WIDTH  = $$((2 * $$(identify - < $</{} | awk "{ print \$$3 }" | sed "s/x.*//")))
POTRACE_IMAGE_HEIGHT = $$((2 * $$(identify - < $</{} | awk "{ print \$$3 }" | sed "s/.*x//")))

GOOGLE_API_KEY = $(SCRIPT_DIRNAME)/google-api-key
GOOGLE_ANNOTATE_RUN = bash $(SCRIPT_DIRNAME)/scripts/google-vision-annotate.sh

INPUT_RELEVANT_PAGES = $(SCRIPT_DIRNAME)/'Sources Hussitica.csv'
OUTPUT_RELEVANT_PAGES = $(SCRIPT_DIRNAME)/relevant-pages
OUTPUT_DIFFICULT_PAGES_DIRNAME = $(SCRIPT_DIRNAME)/difficult-pages

INPUT_HUMAN_JUDGEMENTS_DIRNAME = $(SCRIPT_DIRNAME)/input-human-judgements
INPUT_HUMAN_JUDGEMENTS_WITH_COLUMNS_DIRNAME = $(SCRIPT_DIRNAME)/input-human-judgements_with-columns
INPUT_HUMAN_JUDGEMENTS_WITHOUT_COLUMNS_DIRNAME = $(SCRIPT_DIRNAME)/input-human-judgements_without-columns

INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES = $(SCRIPT_DIRNAME)/input-human-judgements-upscaled_high-confidence_filenames
INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITH_COLUMNS = $(SCRIPT_DIRNAME)/input-human-judgements-upscaled_high-confidence_filenames_with-columns
INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITHOUT_COLUMNS = $(SCRIPT_DIRNAME)/input-human-judgements-upscaled_high-confidence_filenames_without-columns

CONVERT_RUN = $(CONVERT)
CONVERT_OPTIONS = -deskew 45% -quality 100% +repage
CONVERT = convert

IDENTIFY_RUN = $(IDENTIFY)
IDENTIFY = identify

PARALLEL = parallel
PARALLEL_ON_EVERY_NODE_OPTIONS = --halt soon,fail=100% --jobs 12 --nonall --sshlogin -
PARALLEL_SINGLE_NODE_OPTIONS = --bar --nice $(PARALLEL_NICENESS) --halt soon,fail=100% --jobs 48 --joblog $@.joblog
PARALLEL_MANY_NODES_OPTIONS = --bar --nice $(PARALLEL_NICENESS) --halt soon,fail=100% --jobs 25% --sshloginfile $(PARALLEL_SSH_LOGIN) --sshdelay 0.1 --joblog $@.joblog --controlmaster
PARALLEL_NICENESS = $(NICENESS)
PARALLEL_NUM_NODES = $$(grep -v '^\s*\#' $(PARALLEL_SSH_LOGIN) | wc -l)
PARALLEL_NUM_JOBS = $$(printf '%s 0\n' "$$(echo $$(grep -v '^\s*\#' $(PARALLEL_SSH_LOGIN) | sed -r 's\#([0-9]*)/.*\#\1 + \#'))" | bc)
PARALLEL_SSH_LOGIN = $(SCRIPT_DIRNAME)/parallel_sshlogin
PARALLEL_SSH_LOGIN_FILTERED = <(grep -v '^\s*\#' $(PARALLEL_SSH_LOGIN) | sed 's\#[0-9]*/\#\#')
PARALLEL_SCRIPTS_REMOTELY_MOUNTED = $(SCRIPT_DIRNAME)/parallel_tesseract_remotely_mounted
PARALLEL_SCRIPTS_REMOTELY_MOUNTED_FILTERED = <(grep -v '^\s*\#' $(PARALLEL_SCRIPTS_REMOTELY_MOUNTED))
PARALLEL_DATA_REMOTELY_MOUNTED = $(SCRIPT_DIRNAME)/parallel_ahisto_remotely_mounted
PARALLEL_DATA_REMOTELY_MOUNTED_FILTERED = <(grep -v '^\s*\#' $(PARALLEL_DATA_REMOTELY_MOUNTED))
PARALLEL_RUN = $(PARALLEL)

MYSQL = mysql
MYSQL_OPTIONS = --user=$(MYSQL_USER) --password=$(MYSQL_PASSWORD)
MYSQL_USER = user
MYSQL_PASSWORD = password
MYSQL_DB = CMS_archiv
MYSQL_PARAMETERS = $(MYSQL_USER) $(MYSQL_PASSWORD) $(MYSQL_DB)
MYSQL_RUN = $(MYSQL)

SSHFS = sshfs
SSHFS_OPTIONS = reconnect
SSHFS_RUN = $(SSHFS)
SSHFS_TIMEOUT = 15

SCRIPT_DIRNAME = /var/tmp/tesseract
SCRIPT_DIRNAME_SOURCE = asteria04:$(SCRIPT_DIRNAME)
DATA_DIRNAME = /var/tmp/ahisto-2020-08-11
DATA_DIRNAME_SOURCE = mir:/mnt/storage/ahisto-2020-08-11

INPUT_DIRNAME = $(DATA_DIRNAME)/Knihovna
INPUT_DIRNAMES = $(SCRIPT_DIRNAME)/input_dirnames
INPUT_FILENAMES = $(SCRIPT_DIRNAME)/input_filenames
INPUT_FILENAMES_FILTER = $(SCRIPT_DIRNAME)/input_filenames_filter
INPUT_FILENAMES_FILTERED = $(SCRIPT_DIRNAME)/input_filenames_filtered
INPUT_UPSCALED_LOW_CONFIDENCE_FILENAMES = $(SCRIPT_DIRNAME)/input-upscaled_low-confidence_filenames
INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES = $(SCRIPT_DIRNAME)/input-upscaled_high-confidence_filenames
INPUT_FILENAMES_DOWNSCALED = $(SCRIPT_DIRNAME)/input_filenames_downscaled
INPUT_FILENAMES_UPSCALED = $(SCRIPT_DIRNAME)/input_filenames_upscaled
INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED = $(SCRIPT_DIRNAME)/input-human-judgements_filenames_upscaled
INPUT_HUMAN_JUDGEMENTS_FILENAMES_DOWNSCALED = $(SCRIPT_DIRNAME)/input-human-judgements_filenames_downscaled
INPUT_RESOLUTIONS = $(SCRIPT_DIRNAME)/input_resolutions

DOWNSCALED_INPUT_DIRNAME = $(DATA_DIRNAME)/'Knihovna podle ID'
DOWNSCALED_INPUT_DIRNAMES = $(SCRIPT_DIRNAME)/downscaled_input_dirnames
DOWNSCALED_INPUT_FILENAMES = $(SCRIPT_DIRNAME)/downscaled_input_filenames
DOWNSCALED_INPUT_FILENAMES_FILTER = $(SCRIPT_DIRNAME)/downscaled_input_filenames_filter
DOWNSCALED_INPUT_FILENAMES_FILTERED = $(SCRIPT_DIRNAME)/downscaled_input_filenames_filtered
DOWNSCALED_INPUT_RESOLUTIONS = $(SCRIPT_DIRNAME)/downscaled_input_resolutions

DOWNSCALED_UNFLATTENED_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled
BILINEAR_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-bilinear
WAIFU2X_LOW_NOISE_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-waifu2x-low-noise
WAIFU2X_HIGH_NOISE_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-waifu2x-high-noise
POTRACE_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-potrace
SRGAN_SCANNED_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-srgan
SRGAN_CDBT6_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-srgan_cdbt6
SRGAN_CDBT6_SP_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-srgan_cdbt6_sp
SRGAN_CDBT6_ROTATE_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-srgan_cdbt6_rotate
SRGAN_CDBT6_SPROTATE_INPUT_DIRNAME = $(SCRIPT_DIRNAME)/input-downscaled-srgan_cdbt6_sp+rotate

INPUT_SQL_DUMP = $(DATA_DIRNAME)/CMS_archiv_SQL_20200528.sql.gz
INPUT_CSV_DUMP = $(DATA_DIRNAME)/'Obsah CMS Sources online - zakladni data a signatury.csv'
INPUT_JSON_DUMP = $(SCRIPT_DIRNAME)/ocr-texts

OUTPUT_DESKEWED_SCRIPT_DIRNAME = $(SCRIPT_DIRNAME)/output-deskewed
OUTPUT_OCR_TESSERACT3_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr3
OUTPUT_DESKEWED_OCR_TESSERACT3_DIRNAME = $(SCRIPT_DIRNAME)/output-deskewed-ocr3
OUTPUT_OCR_TESSERACT34_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr3+4
OUTPUT_DESKEWED_OCR_TESSERACT34_DIRNAME = $(SCRIPT_DIRNAME)/output-deskewed-ocr3+4
OUTPUT_OCR_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr4
OUTPUT_OCR_TESSERACT4_TWOPASS_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr4-twopass
OUTPUT_DESKEWED_OCR_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-deskewed-ocr4

OUTPUT_OCR_DOWNSCALED_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-ocr4
OUTPUT_OCR_BILINEAR_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-bilinear-ocr4
OUTPUT_OCR_WAIFU2X_LOW_NOISE_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-waifu2x-low-noise-ocr4
OUTPUT_OCR_WAIFU2X_HIGH_NOISE_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-waifu2x-high-noise-ocr4
OUTPUT_OCR_WAIFU2X_HIGH_NOISE_TESSERACT4_TWOPASS_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-waifu2x-high-noise-ocr4-twopass
OUTPUT_OCR_POTRACE_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-potrace-ocr4
OUTPUT_OCR_SRGAN_SCANNED_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-srgan-ocr4
OUTPUT_OCR_SRGAN_CDBT6_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-srgan_cdbt6-ocr4
OUTPUT_OCR_SRGAN_CDBT6_SP_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-srgan_cdbt6_sp-ocr4
OUTPUT_OCR_SRGAN_CDBT6_ROTATE_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-srgan_cdbt6_rotate-ocr4
OUTPUT_OCR_SRGAN_CDBT6_SPROTATE_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-srgan_cdbt6_sp+rotate-ocr4

OUTPUT_OCR_OCRD_TESSERACT3_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-ocrd-ocr3
OUTPUT_OCR_OCRD_TESSERACT34_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-ocrd-ocr3+4
OUTPUT_OCR_OCRD_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-ocrd-ocr4
OUTPUT_OCR_OCRD_CALAMARI_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-ocrd-calamari
OUTPUT_OCR_OCRD_CALAMARI_TESSERACT3_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-ocrd-calamari-ocr3
OUTPUT_OCR_OCRD_CALAMARI_TESSERACT34_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-ocrd-calamari-ocr3+4
OUTPUT_OCR_OCRD_CALAMARI_TESSERACT4_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-ocrd-calamari-ocr4

OUTPUT_OCR_GOOGLE_LOWRES_FLATTENED_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-google-lowres-flattened
OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-google-lowres
OUTPUT_OCR_TESSERACT_4_GOOGLE_LOWRES_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-ocr4-google-lowres
OUTPUT_OCR_GOOGLE_HIRES_DIRNAME = $(SCRIPT_DIRNAME)/output-ocr-google-hires

OUTPUT_OCR_BILINEAR_GOOGLE_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-bilinear-google
OUTPUT_OCR_WAIFU2X_LOW_NOISE_GOOGLE_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-waifu2x-low-noise-google
OUTPUT_OCR_WAIFU2X_HIGH_NOISE_GOOGLE_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-waifu2x-high-noise-google
OUTPUT_OCR_POTRACE_GOOGLE_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-potrace-google
OUTPUT_OCR_SRGAN_SCANNED_GOOGLE_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-srgan-google
OUTPUT_OCR_SRGAN_CDBT6_GOOGLE_DIRNAME = $(SCRIPT_DIRNAME)/output-downscaled-srgan_cdbt6-google

THRESHOLDS = 00 05 10 15 20 25 50 75 100

PYTHON = python3
PYTHON_RUN = nice -n $(NICENESS) $(PYTHON) -m
PYTHON_VENV = virtualenv-$(shell hostname)

install-ocrd:
	docker pull $(OCRD_IMAGE)

import-sql-dump:
	(echo 'USE CMS_archiv;'; gzip -d < <(pv $(INPUT_SQL_DUMP))) | $(MYSQL_RUN) $(MYSQL_OPTIONS)

remount-epimetheus:
	# remounting at $(shell LC_ALL=C date)
	@ $(PARALLEL_RUN) $(PARALLEL_ON_EVERY_NODE_OPTIONS) --timeout $(SSHFS_TIMEOUT) -- " \
	    if [[ \$$(ls $(DATA_DIRNAME)/ | wc -l) = 0 ]]; \
	    then \
	        $(SSHFS_RUN) -o ro,$(SSHFS_OPTIONS) $(DATA_DIRNAME_SOURCE)/ $(DATA_DIRNAME)/; \
	    fi; \
	    if [[ \$$(ls $(OCRD_WORKSPACE_DIRNAME)/ | wc -l) = 0 ]]; \
	    then \
	        $(SSHFS_RUN) -o allow_other,$(SSHFS_OPTIONS) $(OCRD_WORKSPACE_DIRNAME_SOURCE)/ $(OCRD_WORKSPACE_DIRNAME)/; \
	    fi; \
	" < $(PARALLEL_SSH_LOGIN_FILTERED)
	# done remounting

remount-nymfe:
	# remounting at $(shell LC_ALL=C date)
	@ $(PARALLEL_RUN) $(PARALLEL_ON_EVERY_NODE_OPTIONS) --timeout $(SSHFS_TIMEOUT) -- " \
	    if [[ \$$(ls $(SCRIPT_DIRNAME)/ | wc -l) = 0 ]]; \
	    then \
	        fusermount -u $(SCRIPT_DIRNAME)/ || /usr/bin/fusermount3 -u $(SCRIPT_DIRNAME)/ || true; \
	        $(SSHFS_RUN) -o $(SSHFS_OPTIONS) $(SCRIPT_DIRNAME_SOURCE)/ $(SCRIPT_DIRNAME)/; \
	    fi; \
	    if [[ \$$(ls $(DATA_DIRNAME)/ | wc -l) = 0 ]]; \
	    then \
	        fusermount -u $(DATA_DIRNAME)/ || /usr/bin/fusermount3 -u $(DATA_DIRNAME)/ || true; \
	        $(SSHFS_RUN) -o ro,$(SSHFS_OPTIONS) $(DATA_DIRNAME_SOURCE)/ $(DATA_DIRNAME)/; \
	    fi; \
	" < $(PARALLEL_SSH_LOGIN_FILTERED)
	# done remounting

$(INPUT_JSON_DUMP):
	$(PYTHON_RUN) scripts.dump_ocr_texts $(MYSQL_PARAMETERS) $@

$(OUTPUT_RELEVANT_PAGES):
	$(PYTHON_RUN) scripts.dump_relevant_pages $(MYSQL_PARAMETERS) $(INPUT_RELEVANT_PAGES) $@

$(INPUT_UPSCALED_LOW_CONFIDENCE_FILENAMES): $(INPUT_FILENAMES_FILTERED) $(DOWNSCALED_INPUT_FILENAMES_FILTERED) $(INPUT_JSON_DUMP)
	$(PYTHON_RUN) scripts.upscale_downscaled_low_confidence $(INPUT_DIRNAME) $(INPUT_FILENAMES_FILTERED) $(DOWNSCALED_INPUT_DIRNAME) $(DOWNSCALED_INPUT_FILENAMES_FILTERED) $(INPUT_JSON_DUMP) $(INPUT_CSV_DUMP) $@

$(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES): $(INPUT_FILENAMES_FILTERED) $(DOWNSCALED_INPUT_FILENAMES_FILTERED) $(OUTPUT_OCR_TESSERACT4_DIRNAME) $(INPUT_JSON_DUMP)
	$(PYTHON_RUN) scripts.upscale_downscaled_high_confidence $(INPUT_DIRNAME) $(INPUT_FILENAMES_FILTERED) $(DOWNSCALED_INPUT_DIRNAME) $(DOWNSCALED_INPUT_FILENAMES_FILTERED) $(OUTPUT_OCR_TESSERACT4_DIRNAME) $(INPUT_JSON_DUMP) $(INPUT_CSV_DUMP) $@

$(LANGUAGE_DETECTION_CODE):
	git clone https://gitlab.fi.muni.cz/nlp/ahisto-language-detection.git $@
	$(PYTHON_RUN) venv $@/$(PYTHON_VENV)
	cd $@ && source $(PYTHON_VENV)/bin/activate && pip install -r requirements.txt && $(PYTHON_RUN) scripts.create_annotated_hocr

$(LANGUAGE_DETECTION_ANNOTATIONS): $(LANGUAGE_DETECTION_CODE) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.unflatten_directory_annotated $</output $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(TESSERACT_DATA):
	git clone --recurse-submodules https://github.com/tesseract-ocr/tessdata.git $@

$(CALAMARI_MODELS):
	mkdir -p $@
	curl https://ocr-d-repo.scc.kit.edu/models/calamari/GT4HistOCR/model.tar.xz | tar xJC $@

$(WAIFU2X_DATA):
	git clone https://github.com/nagadomi/waifu2x.git $@

$(INPUT_DIRNAMES):
	(cd $(INPUT_DIRNAME) && find -type d) > $@

$(INPUT_FILENAMES):
	( \
	    cd $(INPUT_DIRNAME) && find -type f | \
	    grep -E '\.(jpg|tif|png)$$' | \
	    grep -F -v '/28_Husitica/HUS_64/Urkundliche Beitrage I_ OCR/' | \
	    grep -F -v '/cache/' \
	) | sort > $@

$(INPUT_FILENAMES_FILTERED): $(INPUT_FILENAMES) $(INPUT_FILENAMES_FILTER)
	sort $(INPUT_FILENAMES) $(INPUT_FILENAMES_FILTER) $(INPUT_FILENAMES_FILTER) | $(PYTHON_RUN) scripts.filter_duplicate_basenames | sort > $@

$(INPUT_RESOLUTIONS): $(INPUT_FILENAMES_FILTERED)
	(cd $(INPUT_DIRNAME) && $(PARALLEL_RUN) $(PARALLEL_SINGLE_NODE_OPTIONS) -- '$(IDENTIFY_RUN) {}' :::: $<) > $@

$(DOWNSCALED_INPUT_DIRNAMES):
	(cd $(DOWNSCALED_INPUT_DIRNAME) && find -type d) > $@

$(DOWNSCALED_INPUT_FILENAMES):
	( \
	    cd $(DOWNSCALED_INPUT_DIRNAME) && find -type f | \
	    grep -E '/[0-9]*\.(jpg|tif|png)$$' \
	) | sort > $@

$(DOWNSCALED_INPUT_FILENAMES_FILTERED): $(DOWNSCALED_INPUT_FILENAMES) $(DOWNSCALED_INPUT_FILENAMES_FILTER)
	sort $(DOWNSCALED_INPUT_FILENAMES) $(DOWNSCALED_INPUT_FILENAMES_FILTER) $(DOWNSCALED_INPUT_FILENAMES_FILTER) | uniq -u > $@

$(DOWNSCALED_INPUT_RESOLUTIONS): $(DOWNSCALED_INPUT_FILENAMES_FILTERED)
	(cd $(DOWNSCALED_INPUT_DIRNAME) && $(PARALLEL_RUN) $(PARALLEL_SINGLE_NODE_OPTIONS) -- '$(IDENTIFY_RUN) {}' :::: $<) > $@

$(DOWNSCALED_UNFLATTENED_INPUT_DIRNAME): $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.unflatten_directory $(DOWNSCALED_INPUT_DIRNAME) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(INPUT_FILENAMES_DOWNSCALED): $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.get_downscaled_filenames $< > $@

$(INPUT_FILENAMES_UPSCALED): $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.get_upscaled_filenames $< > $@

$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED): $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.get_upscaled_filenames $< > $@

$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_DOWNSCALED): $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.get_downscaled_filenames $< > $@

$(BILINEAR_INPUT_DIRNAME): $(DOWNSCALED_UNFLATTENED_INPUT_DIRNAME) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.bilinear_upscale $^ $@

$(POTRACE_INPUT_DIRNAME): $(DOWNSCALED_UNFLATTENED_INPUT_DIRNAME) $(INPUT_FILENAMES_UPSCALED) $(INPUT_DIRNAMES)
	$(call create-directories,$(INPUT_DIRNAMES))
	$(PARALLEL_RUN) $(PARALLEL_MANY_NODES_OPTIONS) -- '$(POTRACE_RUN)' :::: $(INPUT_FILENAMES_UPSCALED)

define waifu2x
(cd $(DOWNSCALED_INPUT_DIRNAME) && $(WAIFU2X_RUN) -noise_level $(1) -l /script/$(notdir $(2)))
$(PYTHON_RUN) scripts.unflatten_directory_waifu2x $< $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@
endef

$(WAIFU2X_LOW_NOISE_INPUT_DIRNAME): $(DOWNSCALED_INPUT_FILENAMES_FILTERED) $(INPUT_FILENAMES_DOWNSCALED) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(call waifu2x,$(WAIFU2X_LOW_NOISE),$(INPUT_FILENAMES_DOWNSCALED))

$(WAIFU2X_HIGH_NOISE_INPUT_DIRNAME): $(DOWNSCALED_INPUT_FILENAMES_FILTERED) $(INPUT_FILENAMES_DOWNSCALED) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(call waifu2x,$(WAIFU2X_HIGH_NOISE),$(INPUT_FILENAMES_DOWNSCALED))

$(OUTPUT_DESKEWED_SCRIPT_DIRNAME): $(INPUT_DIRNAME) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_FILTERED)
	mkdir -p $@
	(cd $@ && $(PARALLEL_RUN) $(PARALLEL_SINGLE_NODE_OPTIONS) -- 'mkdir -p {}') < $(INPUT_DIRNAMES)
	make remount
	$(PARALLEL_RUN) $(PARALLEL_MANY_NODES_OPTIONS) -- '$set -e; cd $(SCRIPT_DIRNAME); (CONVERT_RUN) $</{} $(CONVERT_OPTIONS) $@/{}' :::: $(INPUT_FILENAMES_FILTERED)

define create-directories
# create directory structure
mkdir -p $@
(cd $@ && $(PARALLEL_RUN) $(PARALLEL_SINGLE_NODE_OPTIONS) -- 'mkdir -p {}') < $(1)
endef

define symlink-directories
# symlink directory structure
rm -rf $@
cp -as $(1) $@
endef

define ocr-tesseract =
$(call create-directories,$(INPUT_DIRNAMES))
# run the ocr
$(PARALLEL_RUN) $(PARALLEL_MANY_NODES_OPTIONS) --timeout $(TESSERACT_TIMEOUT) -- '$(TESSERACT_RUN) $(2)/{} $@/{.} -l $(DETECTED_LANGUAGES) $(1)' :::: $(3)
endef

define resume-ocr-tesseract =
# resume the ocr
$(PARALLEL_RUN) $(PARALLEL_MANY_NODES_OPTIONS) --timeout $(TESSERACT_TIMEOUT) --resume-failed -- '$(TESSERACT_RUN) $(2)/{} $@/{.} -l $(DETECTED_LANGUAGES) $(1)' :::: $(3)
endef

define ocr-tesseract-twopass =
$(call create-directories,$(INPUT_DIRNAMES))
# run the ocr
$(PARALLEL_RUN) $(PARALLEL_MANY_NODES_OPTIONS) --timeout $(TESSERACT_TIMEOUT) -N 2 -- 'if [ {2} = None ]; then echo | tee $@/{1.}.box $@/{1.}.hocr $@/{1.}.tsv $@/{1.}.txt; else $(TESSERACT_RUN) $(2)/{1} $@/{1.} -l {2} $(1); fi' :::: $(3)
endef

$(OCRD_TASKS): $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	rm -rf $(OCRD_WORKSPACE_DIRNAME)/*
	$(PYTHON_RUN) scripts.flatten_directory_ocrd $(INPUT_DIRNAME) $(OCRD_WORKSPACE_DIRNAME) $<
	make write-ocrd-tasks

write-ocrd-tasks:
	$(file >$(OCRD_TASKS))
	$(foreach CURRENT_DIRNAME,$(OCRD_DIRNAMES),$(file >>$(OCRD_TASKS),$(OCRD_IMPORT)))
	$(foreach CURRENT_STEP,$(OCRD_PIPELINE),$(foreach CURRENT_DIRNAME,$(OCRD_DIRNAMES),$(file >>$(OCRD_TASKS),$(OCRD_PROCESS))))

define ocr-google =
$(call create-directories,$(3))
$(PARALLEL_RUN) $(PARALLEL_SINGLE_NODE_OPTIONS) -- "$(GOOGLE_ANNOTATE_RUN) $(GOOGLE_API_KEY) $(2)/{} $@/{.}.json $@/{.}.error" :::: $(1)
endef

define resume-ocr-google =
$(PARALLEL_RUN) $(PARALLEL_SINGLE_NODE_OPTIONS) --resume-failed -- "$(GOOGLE_ANNOTATE_RUN) $(GOOGLE_API_KEY) $(2)/{} $@/{.}.json $@/{.}.error" :::: $(1)
endef

define ocr-ocrd = 
$(PARALLEL_RUN) $(PARALLEL_MANY_NODES_OPTIONS) :::: $(OCRD_TASKS)
$(PYTHON_RUN) scripts.unflatten_directory_ocrd $(OCRD_WORKSPACE_DIRNAME) $(1) $(1)$(4)_f .xml  $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@ .xml
$(PYTHON_RUN) scripts.unflatten_directory_ocrd $(OCRD_WORKSPACE_DIRNAME) $(2) $(2)$(4)_f .txt  $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@ .txt
$(PYTHON_RUN) scripts.unflatten_directory_ocrd $(OCRD_WORKSPACE_DIRNAME) $(3) $(3)$(4)_f .html $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@ .hocr
endef

%.multicolumn-detection-results-cg.md: $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITH_COLUMNS) $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITHOUT_COLUMNS) %
	$(PYTHON_RUN) scripts.evaluate_multicolumn_detection $^ $@ computational_geometry

%.multicolumn-detection-results-ml.md: $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITH_COLUMNS) $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITHOUT_COLUMNS) %
	$(PYTHON_RUN) scripts.evaluate_multicolumn_detection $^ $@ machine_learning

%.accuracy-results-lowres: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_RELEVANT_PAGES) $(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_accuracy $^ $@ $@.difficult-pages

%.accuracy-results-hires: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_RELEVANT_PAGES) $(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_accuracy $^ $@ $@.difficult-pages

%.accuracy-results-human: $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_RELEVANT_PAGES) $(INPUT_HUMAN_JUDGEMENTS_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_accuracy $^ $@ $@.difficult-pages

%.accuracy-results-human-with-columns: $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITH_COLUMNS) $(OUTPUT_RELEVANT_PAGES) $(INPUT_HUMAN_JUDGEMENTS_WITH_COLUMNS_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_accuracy $^ $@ $@.difficult-pages

%.accuracy-results-human-without-columns: $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITHOUT_COLUMNS) $(OUTPUT_RELEVANT_PAGES) $(INPUT_HUMAN_JUDGEMENTS_WITHOUT_COLUMNS_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_accuracy $^ $@ $@.difficult-pages

output-ocr-google-lowres.language-detection-page-results-lowres-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME) output-ocr-google-lowres
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ OLDA OLDA

output-ocr-google-lowres.language-detection-page-results-hires-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME) output-ocr-google-lowres
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ OLDA OLDA

output-ocr-google-lowres.language-detection-page-results-human-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(LANGUAGE_DETECTION_ANNOTATIONS) output-ocr-google-lowres
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ annotated OLDA

output-ocr-google-lowres.language-detection-page-results-lowres-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME) output-ocr-google-lowres
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ OLDA NLDA

output-ocr-google-lowres.language-detection-page-results-hires-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME) output-ocr-google-lowres
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ OLDA NLDA

output-ocr-google-lowres.language-detection-page-results-human-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(LANGUAGE_DETECTION_ANNOTATIONS) output-ocr-google-lowres
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ annotated NLDA

output-ocr-google-hires.language-detection-page-results-hires-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME) output-ocr-google-hires
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ OLDA OLDA

output-ocr-google-hires.language-detection-page-results-hires-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME) output-ocr-google-hires
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ OLDA OLDA

output-ocr-google-hires.language-detection-page-results-human-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(LANGUAGE_DETECTION_ANNOTATIONS) output-ocr-google-hires
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ annotated OLDA

output-ocr-google-hires.language-detection-page-results-hires-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME) output-ocr-google-hires
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ OLDA NLDA

output-ocr-google-hires.language-detection-page-results-hires-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME) output-ocr-google-hires
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ OLDA NLDA

output-ocr-google-hires.language-detection-page-results-human-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(LANGUAGE_DETECTION_ANNOTATIONS) output-ocr-google-hires
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ None $@ annotated NLDA

%.language-detection-page-results-lowres-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ $(DETECTED_LANGUAGES) $@ OLDA OLDA

%.language-detection-page-results-hires-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ $(DETECTED_LANGUAGES) $@ OLDA OLDA

%.language-detection-page-results-human-olda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(LANGUAGE_DETECTION_ANNOTATIONS) %
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ $(DETECTED_LANGUAGES) $@ annotated OLDA

%.language-detection-page-results-lowres-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ $(DETECTED_LANGUAGES) $@ OLDA NLDA

%.language-detection-page-results-hires-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME) %
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ $(DETECTED_LANGUAGES) $@ OLDA NLDA

%.language-detection-page-results-human-nlda: $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(LANGUAGE_DETECTION_ANNOTATIONS) %
	$(PYTHON_RUN) scripts.evaluate_language_detection_page $^ $(DETECTED_LANGUAGES) $@ annotated NLDA

$(addprefix %.detected-languages.,$(THRESHOLDS)): $(INPUT_FILENAMES_FILTERED) %
	$(PYTHON_RUN) scripts.extract_detected_languages $^ $(DETECTED_LANGUAGES) $@ $(subst .,,$(suffix $@))

$(OUTPUT_DIFFICULT_PAGES_DIRNAME): $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_TESSERACT4_DIRNAME).accuracy-results-hires $(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME) $(OUTPUT_OCR_TESSERACT4_DIRNAME)
	$(PYTHON_RUN) scripts.typeset_difficult_pages $(MYSQL_PARAMETERS) $(word 1,$^) $(word 2,$^).difficult-pages $(word 3,$^) $(word 4,$^) $(INPUT_DIRNAME) $@

$(INPUT_HUMAN_JUDGEMENTS_DIRNAME): $(OUTPUT_DIFFICULT_PAGES_DIRNAME) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.untypeset_difficult_pages $^ $@ $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES) None None

$(INPUT_HUMAN_JUDGEMENTS_WITH_COLUMNS_DIRNAME): $(OUTPUT_DIFFICULT_PAGES_DIRNAME) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.untypeset_difficult_pages $^ $@ $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITH_COLUMNS) sloupce None

$(INPUT_HUMAN_JUDGEMENTS_WITHOUT_COLUMNS_DIRNAME): $(OUTPUT_DIFFICULT_PAGES_DIRNAME) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.untypeset_difficult_pages $^ $@ $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES_WITHOUT_COLUMNS) None sloupce

%.speed-results: %.joblog
	$(PYTHON_RUN) scripts.evaluate_speed $< > $@

%.pie-chart.pdf: %.joblog %.pie-chart-regex %.pie-chart-labels
	$(PYTHON_RUN) scripts.evaluate_speed_piechart $^ $@

$(OUTPUT_OCR_TESSERACT3_DIRNAME): $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_FILTERED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS3),$(INPUT_DIRNAME),$(INPUT_FILENAMES_FILTERED))

$(OUTPUT_OCR_TESSERACT34_DIRNAME): $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_FILTERED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS34),$(INPUT_DIRNAME),$(INPUT_FILENAMES_FILTERED))

$(OUTPUT_OCR_TESSERACT4_DIRNAME): $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_FILTERED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$(INPUT_DIRNAME),$(INPUT_FILENAMES_FILTERED))

$(OUTPUT_OCR_TESSERACT4_TWOPASS_DIRNAME).%: $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(OUTPUT_OCR_TESSERACT4_DIRNAME).detected-languages.%
	$(call ocr-tesseract-twopass,$(TESSERACT_OPTIONS4),$(INPUT_DIRNAME),$(OUTPUT_OCR_TESSERACT4_DIRNAME).detected-languages.$(subst .,,$(suffix $@)))

$(OUTPUT_OCR_OCRD_CALAMARI_DIRNAME): $(TESSERACT_DATA) $(OCRD_TASKS) $(CALAMARI_MODELS)
	$(call ocr-ocrd,OCR-D-OCR,OCR-D-TEXT,OCR-D-HOCR,)

$(OUTPUT_OCR_OCRD_TESSERACT3_DIRNAME): $(TESSERACT_DATA) $(OCRD_TASKS) $(CALAMARI_MODELS)
	$(call ocr-ocrd,OCR-D-OCR3,OCR-D-TEXT3,OCR-D-HOCR3,)

$(OUTPUT_OCR_OCRD_TESSERACT4_DIRNAME): $(TESSERACT_DATA) $(OCRD_TASKS) $(CALAMARI_MODELS)
	$(call ocr-ocrd,OCR-D-OCR4,OCR-D-TEXT4,OCR-D-HOCR4,)

$(OUTPUT_OCR_OCRD_TESSERACT34_DIRNAME): $(TESSERACT_DATA) $(OCRD_TASKS) $(CALAMARI_MODELS)
	$(call ocr-ocrd,OCR-D-OCR34,OCR-D-TEXT34,OCR-D-HOCR34,)

$(OUTPUT_OCR_OCRD_CALAMARI_TESSERACT3_DIRNAME): $(TESSERACT_DATA) $(OCRD_TASKS) $(CALAMARI_MODELS)
	$(call ocr-ocrd,OCR-D-CORRECT3,OCR-D-TEXTC3,OCR-D-HOCRC3,-ALIGN3)

$(OUTPUT_OCR_OCRD_CALAMARI_TESSERACT4_DIRNAME): $(TESSERACT_DATA) $(OCRD_TASKS) $(CALAMARI_MODELS)
	$(call ocr-ocrd,OCR-D-CORRECT4,OCR-D-TEXTC4,OCR-D-HOCRC4,-ALIGN4)

$(OUTPUT_OCR_OCRD_CALAMARI_TESSERACT34_DIRNAME): $(TESSERACT_DATA) $(OCRD_TASKS) $(CALAMARI_MODELS)
	$(call ocr-ocrd,OCR-D-CORRECT34,OCR-D-TEXTC34,OCR-D-HOCRC34,-ALIGN34)

$(OUTPUT_OCR_GOOGLE_LOWRES_FLATTENED_DIRNAME): $(DOWNSCALED_INPUT_DIRNAMES) $(INPUT_FILENAMES_DOWNSCALED) $(GOOGLE_API_KEY)
	$(call ocr-google,$(INPUT_FILENAMES_DOWNSCALED),$(DOWNSCALED_INPUT_DIRNAME),$(DOWNSCALED_INPUT_DIRNAMES))

$(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME): $(OUTPUT_OCR_GOOGLE_LOWRES_FLATTENED_DIRNAME) $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES)
	$(PYTHON_RUN) scripts.unflatten_directory_ocrd $< '' '' .json $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@ .json
	$(PYTHON_RUN) scripts.convert_google_ocr_json_to_text $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(OUTPUT_OCR_GOOGLE_HIRES_DIRNAME): $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED) $(GOOGLE_API_KEY) $(INPUT_FILENAMES_UPSCALED)
	$(call ocr-google,$(INPUT_FILENAMES_UPSCALED),$(INPUT_DIRNAME),$(INPUT_DIRNAMES))
	$(PYTHON_RUN) scripts.convert_google_ocr_json_to_text $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(OUTPUT_OCR_BILINEAR_GOOGLE_DIRNAME): $(BILINEAR_INPUT_DIRNAME) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED) $(GOOGLE_API_KEY) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-google,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED),$<,$(INPUT_DIRNAMES))
	$(PYTHON_RUN) scripts.convert_google_ocr_json_to_text $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(OUTPUT_OCR_WAIFU2X_LOW_NOISE_GOOGLE_DIRNAME): $(WAIFU2X_LOW_NOISE_INPUT_DIRNAME) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED) $(GOOGLE_API_KEY) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-google,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED),$<,$(INPUT_DIRNAMES))
	$(PYTHON_RUN) scripts.convert_google_ocr_json_to_text $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(OUTPUT_OCR_WAIFU2X_HIGH_NOISE_GOOGLE_DIRNAME): $(WAIFU2X_HIGH_NOISE_INPUT_DIRNAME) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED) $(GOOGLE_API_KEY) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-google,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED),$<,$(INPUT_DIRNAMES))
	$(PYTHON_RUN) scripts.convert_google_ocr_json_to_text $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(OUTPUT_OCR_POTRACE_GOOGLE_DIRNAME): $(POTRACE_INPUT_DIRNAME) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED) $(GOOGLE_API_KEY) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-google,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED),$<,$(INPUT_DIRNAMES))
	$(PYTHON_RUN) scripts.convert_google_ocr_json_to_text $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(OUTPUT_OCR_SRGAN_SCANNED_GOOGLE_DIRNAME): $(SRGAN_SCANNED_INPUT_DIRNAME) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED) $(GOOGLE_API_KEY) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-google,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED),$<,$(INPUT_DIRNAMES))
	$(PYTHON_RUN) scripts.convert_google_ocr_json_to_text $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(OUTPUT_OCR_SRGAN_CDBT6_GOOGLE_DIRNAME): $(SRGAN_CDBT6_INPUT_DIRNAME) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED) $(GOOGLE_API_KEY) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-google,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED),$<,$(INPUT_DIRNAMES))
	$(PYTHON_RUN) scripts.convert_google_ocr_json_to_text $(INPUT_HUMAN_JUDGEMENTS_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $@

$(OUTPUT_DESKEWED_OCR_TESSERACT3_DIRNAME): $(OUTPUT_DESKEWED_SCRIPT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_FILTERED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS3),$<,$(INPUT_FILENAMES_FILTERED))

$(OUTPUT_DESKEWED_OCR_TESSERACT34_DIRNAME): $(OUTPUT_DESKEWED_SCRIPT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_FILTERED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS34),$<,$(INPUT_FILENAMES_FILTERED))

$(OUTPUT_DESKEWED_OCR_TESSERACT4_DIRNAME): $(OUTPUT_DESKEWED_SCRIPT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_FILTERED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_FILENAMES_FILTERED))

$(OUTPUT_OCR_DOWNSCALED_TESSERACT4_DIRNAME): $(DOWNSCALED_UNFLATTENED_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_FILENAMES_UPSCALED))

$(OUTPUT_OCR_BILINEAR_TESSERACT4_DIRNAME): $(BILINEAR_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_FILENAMES_UPSCALED))

$(OUTPUT_OCR_WAIFU2X_LOW_NOISE_TESSERACT4_DIRNAME): $(WAIFU2X_LOW_NOISE_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_FILENAMES_UPSCALED))

$(OUTPUT_OCR_WAIFU2X_HIGH_NOISE_TESSERACT4_DIRNAME): $(WAIFU2X_HIGH_NOISE_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_FILENAMES_UPSCALED))

$(OUTPUT_OCR_POTRACE_TESSERACT4_DIRNAME): $(POTRACE_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_FILENAMES_UPSCALED))

$(OUTPUT_OCR_WAIFU2X_HIGH_NOISE_TESSERACT4_TWOPASS_DIRNAME).%: $(WAIFU2X_HIGH_NOISE_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(OUTPUT_OCR_WAIFU2X_HIGH_NOISE_TESSERACT4_DIRNAME).detected-languages.%
	$(call ocr-tesseract-twopass,$(TESSERACT_OPTIONS4),$<,$(OUTPUT_OCR_WAIFU2X_HIGH_NOISE_TESSERACT4_DIRNAME).detected-languages.$(subst .,,$(suffix $@)))

$(OUTPUT_OCR_SRGAN_SCANNED_TESSERACT4_DIRNAME): $(SRGAN_SCANNED_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED))

$(OUTPUT_OCR_SRGAN_CDBT6_TESSERACT4_DIRNAME): $(SRGAN_CDBT6_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED))

$(OUTPUT_OCR_SRGAN_CDBT6_SP_TESSERACT4_DIRNAME): $(SRGAN_CDBT6_SP_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED))

$(OUTPUT_OCR_SRGAN_CDBT6_ROTATE_TESSERACT4_DIRNAME): $(SRGAN_CDBT6_ROTATE_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED))

$(OUTPUT_OCR_SRGAN_CDBT6_SPROTATE_TESSERACT4_DIRNAME): $(SRGAN_CDBT6_SPROTATE_INPUT_DIRNAME) $(TESSERACT_DATA) $(INPUT_DIRNAMES) $(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED)
	$(call ocr-tesseract,$(TESSERACT_OPTIONS4),$<,$(INPUT_HUMAN_JUDGEMENTS_FILENAMES_UPSCALED))

$(OUTPUT_OCR_TESSERACT_4_GOOGLE_LOWRES_DIRNAME): $(INPUT_UPSCALED_HIGH_CONFIDENCE_FILENAMES) $(OUTPUT_OCR_TESSERACT4_DIRNAME) $(OUTPUT_OCR_GOOGLE_LOWRES_DIRNAME)
	$(PYTHON_RUN) scripts.combine_tesseract_with_google $^ $@
