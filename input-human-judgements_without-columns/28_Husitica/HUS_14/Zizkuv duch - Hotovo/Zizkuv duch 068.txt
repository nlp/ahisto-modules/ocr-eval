68
Pověřující list Rybkovi.

rušiti a tupiti a těch jste pomocni pohříchu. Nejprv králi Zikmun-
dovi uherskému, kacířskému králi, zrádci Pána Boha i čtení jeho
svatého, násilníku panen i paní, mordéři, žháři, zhoubci jazyka
českého, a s ním pánům Švamberkovi a Švihovským, Henrichovi
Elstrberkovi, Kolovratům pomahali jste se jim postavili proti
Pánu Bohu i proti čtení a ustavení božímu a přikázání. A chtí vás
zavésti o vaše duše i o vaše statky a vámi chtí sobě před králem
čest činiti, vy abyšte věrně sloužili a pracovali a o své duše
přišli etc. I nedivíme se tomu, že kdož Bohu věren není, také
lidem nebude. A tak třebať se jich vám vystřihati, afby vás více
nezrazovali o vaše duše; a takét i nám to činí aneb činiti chtí,
nebudeliť pomoci boží. A věřímeť Pánu Bohu našemu, žeť nás
jich chytrostí a úkladův vystříže. A Pane Bože, dejž vám, abyste
z jich osidel také vypadli a k Pánu Bohu hleděli, jenž jest vám
dal tělo i duši. A chtěli(-li) byšte, abychom nepálili a nebrali
i země nehubili, i čemu jste Pánu Bohu svobody nedali a těm
čtyřem artikulím spasitedlným ut supra, o nichž jste prvé slyšeli?
A k nám mluvili, že jim chtí svobodu dáti, a chválíce, že jest to
dobré. A toť jsou selhali před tváří boží i před námi.

3. Pověřující list Rybkovi z Lužnice
k Třeboňským.
V poli u Německého Brodu, 10. ledna 1422.
Pán buoh všemohúcí rač býti s vámi i s námi svú svatú milostí.
Bratří v pánu bohu! Jan Žižka z hradu Kalichu, zprávce obcí
České země příchylných a plnících zákona buožieho v naději
buoží, rychtáří, purkmistru, konšelóm i obcí města Třeboňského.
Posielám k vám Rybku z Lužnice, ukazatele listu tohoto, svých
úmyslóv zpraveného; i žádámť, abyste jemu věřili, což bude
