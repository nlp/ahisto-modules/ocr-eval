80
Závěrečné provolání sjezdového zápisu.
bohu setrvale bez pochybení položiti, čekajíce od něho věčné
odplaty.
I prosímeť vás milé obce, ve všech a ze všech krajin, knížat,
pánuov, rytířuov, panoší, měšťanuov, řemeslníkuov, robotězuov,
sedlákuov, i lidí všech stavuov, a zvláště napřed všech věrných
Čechův, abyste se k tomu dobrému svolili a nám toho radni
a pomocni byli. A my vám zase též držeti, plniti i mstíti (chceme),
pro milého pána boha, pro jeho svaté umučení, pro vysvobození
pravdy zákona božího, svatých a jich zvelebení, ku pomoci věrným
církvi svaté a zvláště jazyka českého i slovenského, i všeho
křesťanství, ku pozdvižení věrným a ku potupě neústupným
a zjevným kacířům a pokrytým a zloskvrníkům, aby pán buoh
všemohúcí nám i vám ráčil svú pomoc dáti i zvítěziti nad ne-
přátely svými i našimi, a za nás i s vámi bojovati svú mocí a ne-
odlučovati nás své svaté milosti. Amen.
Budiž pán buoh s námi i s vámi, v nichž jste, a kdež se líbí
trojici svaté! A toho pro lepší svědomí a potvrzení a jistotu
s větší pilností vedle duchovenství nad bídný rozum světa tohoto,
my svrchu psaní s dobrým rozmyslem vědomě, dobrú volí k tomuto
zápisu a listu svolujeme, a svolujeme jej skutečně držeti a zacho-
vati i ostříhati s pomocí nestvořené a na věky požehnané trojice
svaté. Amen. Tak pán buoh dej!
11. Výstraha Hradeckých Žižkovi před
úkladným vrahem.
V Králové Hradci, 22. listopadu 1423.
Pán buoh všemohúcí rač býti s tebú, se všemi bratřími věrnými
i s námi hříšnými svú svatú milostí a pomocí. Bratře Žižko i bratří
naši nejmilejší! Věz, že jsme jednoho Opočenských strany jali,
