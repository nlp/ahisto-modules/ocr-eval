Manifest svazu plzeňskému.
67
hřiechy smrtedlné na všem zboží aby stavoval, což móž najdál;
a to pod základem svrchupsaným. Pakli bychom nezdrželi
příměřie svrchupsaného, jehožto bože nedaj: tehdy máme
i slibujem základ svrchupsaný od napomenutie v měsíci splniti
na Krumlově neb na Novém Hradě, kdež nám naši věřitelové
ukáží. Pakli bychom základu svrchupsaného v měsíci nesplnili,
tehdy jim dáváme plnú moc svrchupsaným, urozenému pánu
Oldřichovi z Rozmberka neb jeho purkrabiem, aby mohli
napřed nám i našim všem láti, přímluvati, stavovati, jímati,
šacovati tak dlúho, dokudž bychom jim svrchupsaného základu
nevyplnili. A k tomu ke všemu na potvrzenie své jsme pečeti
k tomuto listu přívěsili. Jenž jest dán léta ot narozenie božieho
tisícieho čtyřstého a dvůdcátého, ten pondělí oktav svatého
Martina

2. Manifest katolickému svazu plzeňskému.
Z počátku února 1421.
My, Jan Žižka, Chval z Machovic, hejtmani a správce lidu
táborského, pravě českého v naději boží, napomínámeť vás pro
boží umučení, všecky rytíře, panoše, měšťany i sedláky landfrídu
plzeňského, abyste se Pánu Bohu neprotivili více a přikázání
jeho svatému ani těm čtyřem kusům spasitedlným, o kteréž my
bojujeme s boží pomoci. A vy se jim protivíte a nás tisknete od
toho dobrého a od našich duší spasení. Nejprve od slova božího
slyšení, druhé od těla božího a krve boží přijímání; sami tak
přijímati nechcete a nám i jiným věrným bráníte. Třetí kněžstva
nadání, ježto byste je měli rušiti jako kacířská, proti Pánu Bohu
nadání těch bráníte a chcete na tom umříti a nebezpečně smrt
chcete trpěti v kacířství. Čtvrtý kus. Hříchy smrtedlné měli byste
