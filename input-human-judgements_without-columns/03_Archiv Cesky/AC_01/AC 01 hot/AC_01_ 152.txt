152
C.I. Staré zápisy rodu Sternberského, 1424.
šteina mincmeistr na horách Kutnách, Diwiš Bořek z Miletínka a Jan ze Smiřic,
rukojmě swrchupsaného p. Alše, s nim a zaň slibujem, že to vše zdrží a splní
což swrchupsáno stojí. Pakliby toho neučinil a na čem přestúpil, tehda sme pro-
padli čtyři tisíce kop gr. swrchupsaných, jménem základu, múdrým a opatrným
mužóm purkmistróm, konšelóm i obci měst Pražských, a slibujem dáti a zaplatiti
od napomenutie we dwú neděli pořád příštích penězi hotowými a bezewšeho pro-
dlenie. Paklibychom toho neučinili, tehda podnikujem wšechno práwo obecnieho
leženie, jakoby list náš s našimi pečetmi obyčejného leženie na nás měli w městě
Pražském w hospodě, kdež nám od našich wěřitelów swrchupsaných bude ukázáno;
a odtud slibujem nikterakž newynikowati, lečbychom prwé swrchupsaný základ
i s škodami proto wzatými dřiewerečeným našim wěřitelóm hotowými penězi za-
platili a splnili docela a úplně. A ktož tento list s jich dobrú wuoli jmieti bude,
ten má wšechna práwa swrchupsaná a t. d. (Ostatek chybj.)

16.
Umluwa mezi p. Alšem Holickým ze Šternberka a panj Perchtau ze Šternberka o zpráwu
statkůw gegjch.
W Krumlowě, 21 Nov. 1424.
Já Aleš ze Šternberka, odjinud z Holic, wyznáwám tiemto listem wšem,
kdož jej čísti neb čtúce slyšeti budú: že jsem s urozenú pani Perchtú z Krawař,
seděniem na Konopišti, takúto umluwu učinil, jakž w dole psáno stojí. Najprwé,
že paní Perchta jmá kázati slíbiti mně jménem s Konopišti a s Šternberkem a což
k těm hradóm přisluší, i s těmi sirotky, takúžto wýmluwú, když já zastúpím
dluhy sirotčí a wywadim ty jisté služebníky, ježto jsú za ně rukojmě a neboli swé
dali, jakožto pana Beneše z Dubé řečeného Libúně a Jakuba z Bezdějowic, také
i jiné rukojmě, a když to učiním, tehdy aby mně hradowě byli sslúpeni i s těmi
sirotky ale prwé nic. Pakliby buoh sirotków neuchowal, ale hradowé aby mi
sami byli postúpeni. Také já nejmám paní Perchtě nahoře psané piekážeti na
tom zboží, dokawadżby ona chtela s sirotky býti a tak dlúho až do její smrti,
lečby swój staw proměnila. A jestli žeby swój staw promènila a neboli ji buoli neucho-
wal, tehdy jako swrchu psáno stojí, když já ty dluhy její a rukojmè i'ádnè zastúpím,
tehdy mně to zboží, s těmi zámky nahoře psanými, bez zmatku jmá sslúpeno býti i s si-
rotky. A já jmám zajistiti, aby sirotkóm nebylo uprodáwáno ani zastawowáno leč w těch
dluzích, kteříž jsú ostali po otci aneb po mateři. Také jestližeby buoh paní Perchty ne-
uchowal, a neboli Petra syna jejieho, a dcera její Elška sestra Petrowa zrostala a do-
rostla, žeby jměla wdána býti, aby bez mé wuole nebyla wdáwána, a po ni aby nebylo
dáno celé panstwie, kteréžby zuostalo, než jmenowitě tři tisíce kop gr. a pět set
