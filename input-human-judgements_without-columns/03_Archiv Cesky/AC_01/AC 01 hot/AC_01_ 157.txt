C. I. Stare zápisy rodu Šternberského, 1432.
157
domie pečeti statečných a slowútných Wiléma z Postupic odjinud s Hrádku a Be-
neše z Mokrowús přiwěšeny jsú wedlé mne k témuž listu. Jenž dán w neděli In-
vocavit, léta od narozenie syna božieho tisícého čtyřstého třidcátého prwnieho.

20.
Aleš Holický ze Šternberka bratřjm ze Swogšjna zapisuge na zbožj swém 400 kop
gr. dluhu wěnného za pannau Annau ze Šternberka.
Na Křiwoklátė, 14 Mai 1432.
Já Aleš ze Šternberka, odjinud z Holic, seděním na Hrádku Křiwoklátu,
známo činím tiemto listem obecnè wšem, kdož jej uzšie a čisti nebo čtúce slyšeti
budú: ze jsem dlužen jistého a sprawedliwého dluhu 400 kop gr. dobrých střie-
brných rázu Pražského a měny země České wěna prawého, kteréž jsem wěnowal
po urozené panně, panně Anně sestře, dceři někdy urozeného pána, pana Jaro-
slawa ze Šternberka odjinud z Weselé, strýce mého milého, urozeným Petrowi,
Jankowi a Waňkowi bratřím wlastním ze Swojšína, i tomu každému, kdožby tento
list jměl s dobrú jich wolí. A ty jisté penieze, 400 kop gr. swrchupsaných, já
Aleš napředpsaný jmám a slibuji tiemto listem mú dobrú čistú wěrú křesťanskú,
beze wší zlé lsti, wěřícím mým nadepsaným oddati a odkázati na Třešti, na tom
na wšem zboží k Třešti přislušejícím, jestli že mi to zbožie již řečené w té mieře
w mé prawé plné swobodné drženie přijde, tak, aby nadepsaní wěřící moji wšecky
penicze nadepsané jistinné 400 kop gr. swrchupsaných úplně a docela na tom na
wšem zboží sobě wybrali a wybrati mohli swobodně, mého a mých dědiców, er-
bów, poručników i uředników wšech beze wšeho překazu. A jestli žeby mi to
zbožie nadepsané w té mieře nepřišlo w mé prawé plné swobodné drženie, tehdy
nadepsaným wěřicím mým slibuji tiemto listem, mú dobrú čistú wěrú křesťanskú,
beze wšie zlé lsti, nadepsané penieze 400 kop gr. swrchupsaných oddati a odká
zati na jiném mém zboží swobodném, kdež moci budu, a kteréhož práwě úplně
a swobodné mocen budu; aby nadepsaní moji wěřící wšecky penieze ty jisté na-
depsané, totiž 400 kop gr. nadepsaných na tom jistém mém zboží kterémžkoliwěk
mnú jim odkázaném a oddaném brali, wybierali a zdwihali, bráti, wybierati a zdwi-
hati mohli, padesát kop gr. swrchupsaných prawého platu ročnieho čistého, poč-
núce o sw. Hawle w plnú dwú letú pořád zběhlú od dánie tohoto listu najprwě
přištiem. A opět 50 kop gr. nadepsaných ročních o sw. Jiří potom i hned najprwé
příštím, a tak potom při každém sw. Hawlu 50 kop gr. a při každém sw. Jiří
druhých 50 kop gr. swrchupsaných úročních, wšecka léta budúci pořád zběhlá,
až do prawého a plného wybránie swého dluhu swrchu psaného 400 kop gr.
nadepsaných. A kdežkoliwěk na mém zboží já napředpsaný Aleš ze Šternberka
