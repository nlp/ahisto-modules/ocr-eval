A. I. Psanj cjsaře Sigmunda r. 1425, 1426.
25
26.
Témuž: aby zápisy s Henr. Plawenským a kragem Plzenským dokonal, a do Wjdně
k budaucjmu sgezdu knjžat řjšských přigel.
W Skalici, 1423, 5 Dec.
Sigmund z božie milosti Římský král wždy rozmnožitel říše, a Uherský,
Český etc. král.
Urozený wěrný milý! Poslal nám urozený Heinrich Plawenský a drahně
jiných kraje Plzenského přiepis, který jsi jim poslal, žádaje na nich, abyste sie
spolu podle toho přiepisu zawázali; a ten sme přiepis widěli a chwálili, i líbí sie
nám abyste sie spolu zawázali, a sobě proti našim a wašim nepřátelóm, a w naší
abyste službě zostali. Protož dokonajte ten zápis mezi sobú, abyste silnejší byli
a sobě tiem stálejie mohli pomáhati a nepřátelom odolati. Také wěz, že nám
urozený Houpt von Bappenheim maršalk náš říšský psal, že jsú kurfurstowé na
nynějšiem roce w Mohuči konečně na tom ostali, že k nám do Wiedně chtie
osmý den po hromniciech přijeti: a nadějem sie bohdá, že tu mnoho dobrého
proti těm kacieřóm zjednáme. Protož jestližeby kurfurstowé přijeli, a ty to zwieš,
aby k nám také do Wiedně přijel, jakož tu laké k příjezdě jiným pánóm z našie
strany píšem. Neb tu spolu jsúce, budem moci i nám í wám mnoho dobrého
zjednati. Dán v Skalici, w stredu postiece sie k s. Mikuláši, králowstwí našich
Uherského etc. w XXXIX, Římského w šestnáctém a Českého w šestém létě.
Ad mandatum D. Regis: Michael praepositus Boleslaviensis.
Nobili Ulrico de Rozemberg,
fideli nostro dilecto.
27.
Témuž: o geho gednánj s Táborskými a s Pražany, kteréžto schwaluge, též o pomoci.
která p. Oldřichowi z Wratislawi činiti se má.
W Prešpurce, 1426, 9 Febr.
Sigmund z božie milosti Římský král, po wše časy rozmnožitel říše, a
Uherský, Český etc. král.
Urozený wěrný milý! Twým sme listóm, kteréž si nám nynie poslal,
dobře srozoměli; a zwláště, že jsi Tábory byl k sobě na Chusník zawolal a s nimi
mluwil, a na nich prozwěděl, a tak swú snažnost učinil, zdaby byl mohl co do-
brého zjednati; a že sú příměří na tobě požádali, a že jsi jim toho powoliti ne-
chtěl, toho jsme welmi od tebe wděčni; a kterak jsi s druhými městy, s Piese-
ckými a s jinými městy, kteráž sie jich nádrží, také mluwil, a že jsú na to pěknú
4
