A. I. Psanj cjsare Sigmunda r. 1425.
23

23.
Maternowi z Wožice o swém wálečném taženj zpráwu dáwá, též o obležených na
Wožici pjše.
W Dalešicjch, 1425, 12 Oct.
Sigmund z božie milosti Římský král wždy rozmnožitel říše, a Uherský,
Ceský etc. král.
Urozený wèrný milý! Dáwámeť wěděti, že sme dnes s naší wojskú milému
bohu děkujic zdráwi a weseli sem přijeli, a bohdá zajtra dále potáhnúc, mienime
s naším milým synem, jenž sme sie s ním zde dnes sjeli, tak nepřátely hnúli,
chtieliť swým pomoci, jakož prawie, žeť sie musejí od Wožice odstúpiti a hnúti.
Protož od tebe žádáme, aby swé na hradě tú naší příjezdu utěšil, a kázal jim aby
sie bránili a drželi jako dobří lidé, žeť je bohdá naše příjezda wždy odplaší. A kte-
rak sie wšichni běhowé s těmi Tábory jednají, to nám wše daj wěděti. Dan w Da-
lešicích, w pátek před s. Kalixtem, králowslwí našich etc.
Ad mandatum D. Regis: Michael praepositus Boleslaviensis.
Nobili Materno de Wozicz,
fideli nostro dilecto.
24.
Panu Oldřichowi z Rosenberka: odpowěd dáwage, chwalj, že s nepřátely přjměřj ne-
má, a swau milostj se k němu oswědčuge.
U Drašowa polem, 1423, 28 Oct.
Sigmund z božie milosti Římský král, wždy rozmnožitel říše, a Uherský,
Český etc. král.
Urozený wěrný milý! Twému sme listu úplně srozuměli. A jakož nám
mezi jinými kusy píšeš, jakož rozumieme, že s našimi nepřátely ješče žádného pří-
měřie nemáš, a žes toho bez našeho rozkázánie nechtiel pčijieti ɔc. to sme rádi
slyšeli, i žádáme, aby ješče žádného přímétie s nimi nečinil do našeho obeslánie:
neb skoro swé počestné poselstwie chcem u tebe mieti. A na jiné kusy, o něž
nám píšeš, nemohli sme nynie odpowědi dáti; než po těch posléch dáme na to od-
powěd dobrotiwú, a chcem sie k tobě milostiwě ukázati, a tak s tebú naložiti,
jako sme i dřéwe činili, že toho wděčen budeš. Dán polem u Drašowa, den swa-
tých apoštolów Šimoniše a Judy, králowstwi našich Uherského w XXXIX, Řím-
ského w XVI, a Českého w šestém létě.
Ad mandatum domini Regis: Michael praepositus Boleslaviensis.
Nobili Ulrico de Rozemberg,
fideli nostro dilecto.
