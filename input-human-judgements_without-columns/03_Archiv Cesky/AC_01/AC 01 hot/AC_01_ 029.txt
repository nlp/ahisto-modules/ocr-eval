A. I. Psanj cjsaře Sigmunda r. 1429.
29
31.
Témuž: opět o wýplatě hradu Zwjkowa, i o gjzdě swé do Wjdně.
We Fišermundě, 1429, 12 Sept.
Sigmund z božie milosti Řimský král wždy rozmnožitel říše, a Uherský,
Český etc. král.
Urozený wěrný milý! Jakožť sme z Prešpurka psali o Zwiekow, aby w tom
nemeškal pro naše dobré i pro zemské dobré, i tudiež pro swé dobré: i wěřímeť,
že toho pilen budeš, jakožs pak wždy hrdla i statku nelituje rád naši wóli činil
a našeho dobrého pilen byl, aby o ten hrad wždy dokonal, jak muožeš bez mešká-
nie. A wědětiť dáwáme, žeť dnes sme do Wiedni wyjeli, a žeť tu dluho nebudem
moci býti. Protož aby to skonaje u nás bez meškánie byl; a na tom nám mnoho
dobrého, a welikú libost a službu ukážeš. Dán u Fišermundě, ten pondělí po na-
rozenie matky božie, léta božieho tisícieho čtyrstého a potom dwudcátého dewa-
tého, let králowstwí Uherského etc. w XLIII, Římského w XXIX (sic) a Českého
w desátém lélě.
Ad mandatum D. Regis: Caspar Slick Cancell.
Nobili Ulrico de Rosenberg
fideli nostro dilecto.
32.
Témuž: wděčen gsa wýplaty Zwjkowa, napomjná p. Oldřicha, aby rychle k němu do
Wjdně přigel.
We Wjdni, 1429, 17 Sept.
Sigmund z božie milosti Římský král, po wše časy rozmnožitel říše, a
Uherský, Český, Dalmatský etc. král.
Urozený wěrný milý! Jakož nám píšeš po našem poslu, žes již naše ká-
zanie w tom učinil, a ten swój zámek za Zwiekow dal, i k tomu hotowé peníze,
obšed sie jakž moha u přátel, wšecky wěci, kteréž sú k tomu nynie potřebi byly,
wyjednals, a tento ponděli minulý že si sie w hrad uwázal; a také dále jakož nám
w swém listu píšeš: těch nowin jsme od tebe přieliš welmi wděčni, a bohdá
chcem za ně dobrý koláč dáti, ježto nám poděkuješ. A poněwadž je tak, žádáme
od tebe, aby wseda na koně, k nám sem bez meškánie přijel, aťbychom s tebú
o ty i o jiné wěci rozmluwiti mohli; a to učiň, jakožť wěšíme. Dán u Wiedni,
in die Lamberti, let králowstwí našich Uherského etc. w XLIII, Římského w XXIX,
(sic) a Českého w desátém létě.
Ad mandatum D. Regis: Caspar Slick Cancell.
Nobili Ulrico de Rosenberg,
fideli nostro dilecto.
