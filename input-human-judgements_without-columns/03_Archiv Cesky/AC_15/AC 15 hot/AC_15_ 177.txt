s roku 1420—1422.
177

78.
Sigmund, římský, uherský, český atd. král, odvolává své zastavení statků kláštera Vyzo-
vického. (66)
V Norimberce, 23. srpna 1422.

Sigismundus, d. gr. Roman. rex, semper Augustus ac Hung., Boemie, Dalm.,
Croacie etc. rex, not. fac. ten. pres. vniuersis, quod licet alias quadam ardua neces-
sitate constricti ad resistendum peruersis conatibus et machinamentis hereticorum in
Boemia valide consurgencium, qui eciam sue inmanitatis rabie fideles dei christicolas
tam morti tradentes atrociter quam in bonis funditus spoliantes, ecclesias dei demo-
lientes adeo debachati sunt et adhuc in sua pertinacia perdurantes nomen christianum
eciam hodie conantur extinguere penitus, non parcentes sexui vel etati, bona eccle-
siastica tam Pragensis ecclesie quam eciam aliarum ecclesiarum, monasteriorum et
parrochialium, cuiuscunque eciam condicionis existant, tunc certis personis proscripse-
rimus et dederimus possidenda, sicut eciam bona monasterii Wisowicensis ordinis
Cisterciensis, Olomucensis dyocesis, nos fecisse preterito tempore memoramur. Nos
attendentes huiusmodi donacionem et alienacionem bonorum ecclesiasticorum in per-
sonas seculares quacumque occasione seu condicione factas tam a legibus quam ca-
nonibus fore irritas et inanes nec debere subsistere seu valere, non per errorem aut
improuide, sed animo deliberato, maturo omnium electorum sacri Romani imperii et
aliorum multorum principum, comitum et comunitatum imperii sacri et regni Boemie
comunicato consilio prefatam donacionem et alienacionem de possessionibus monasterii
Wisowicensis prefati quibuscumque personis quacumque occasione, conclicione seu
colore factas, auctoritate Romana et Boemie regia tenore presencium reuocanimus,
irritauimus, anullauimus, reuocamus, irritamus, anullamus et nullius dicimus, pro-
nunctiamus et declaramus debere fore roboris vel vigoris; decernentes et volentes,
quod honorabilis .. abbas monasterii Wisowicensis prefati vnacum conuentu suo
eciam auctoritate propria, assumptis sibi ad hoc, si opus fuerit, suis fautoribus et
amicis, se de eisdem bonis sui monasterii omnibus et singulis, vbicunque eciam fuerint
constituta, debeat et possit intromittere, tenere, vtifrui et absque impedimento cuius-
cumque eciam nostras litteras habentis seu alias cum violencia possidentis, ac eciam
sibi pro recuperacione eorundem bonorum tutores, defensores et adiutores eligere et
recipere pro suo comodo, quociens sibi visum fuerit expedire. Presencium sub nostri
regalis sigilli appensione testimonio litterarum. Dat. Nuremberge a. d. mill. quadring.
vicesimo secundo, dominico ante Bartholomei apostoli, regnorum nostrorum anno
Hungarie etc. tricesimo sexto, Roman. tredecimo [sic], Boemie vero tercio.
In plica: Ad mand. dom. regis domino Georgio, episcopo Pataviensi, cancellario
referente, Franciscus prepositus Strigoniensis. – Verso: R(egistrata). Henricus Fye.
Z pečeti zbyl jen proužek pergamenový. Srv. takovýž list kl. Sedleckého v AČ. XIV. 406 č. 15.
Archiv Český xv.
