z roku 1426-1431.
179
80.
Sigmund, římský, uherský, český atd. král, vyznává, že učiniv posledně zúčtování s Půtou
z Častolovic, jest mu ještě dlužen 5000 zl., z kterých zaplatí 2000 zl. o sv. Martině nejblíže
příštím a zbývající tři tisíce zl. o středopostí taktéž nejblíže příštím.(14)
V Prešpurce, 20. srpna 1429.
Sigismundus, d. gr. Roman. rex, semper Aug. ac Hung., Boh., Dalm., Croacie
etc. rex, not. fac. ten. presen. vniuersis, quod nos nouiter cum nobile [sic] Puotla
de Czastolowicz, fideli nostro dilecto, racionem f[a]cientes et ipsum pro omnibus de-
bitis, in quibus sibi obligabamur, contentum reddentes, sibi adhuc in quinque millibus
florenis auri remansimus debitores. Idcirco volentes prefatum Puotham de prefatis
pecuniis contentum reddere et securum, sibi tenore presencium promittimus verbo
regio et spondemus, quod prefato Puothe et heredibus suis aut illi, qui presentem
litteram cum ipsius bona voluntate habuerit, de prefatis quinque millibus florenis
duo millia super festo s. Martini proxime venturo et residua tria millia super medio
quadragesime similiter proxime affuture cum pecuniis albis siue paruis tunc in regno
nostro Hungarie currentibus realiter persoluere volumus et pagare contradiccione et
renitencia cessantibus quibuscumque. In cuius rei testimonium sigillum nostrum pre-
sentibus est appensum. Dat. Posonii a. d. mill. quadring. vic. nono sabbato ante
festum s. Bartholomei apostoli, regnorum nostrorum a. Hungarie etc. XLIII°, Roman.
decimo nono, Bohemie vero decimo.
In plica: Ad mandatum dom. regis Caspar Slick. — Verso: Registrata. So-
luta est ad plenum et in toto.
K listině proříznuté přivěšena jest na proužku pergamenovém menší červená pečeť krále Sigmunda.

81.
Oldřich z Rosenberka, Vilém z Hasenburka, Albrecht z Koldic, Aleš ze Šternberka, Hynek
z Rotenburka, Jaroslav z Dubé a Jan z Ústí vyznávají, že král Sigmund učiniv účet s Půtou
z Častolovic o dluhy a škody a o služby, zůstává mu dlužen ještě 9600 kop gr., a že řeče-
nému Půtovi v sumě té zastavuje král hrad a město Kladsko s městečkem Landekem a By-
střicí, s městem a zámkem Frankensteinem, s městem Radkovem, jakož i klášter Kamenec
i se vším jich příslušenstvím. (15)
V Norimberce, 29. července 1431.
Wir Vlrich von Rosemberg, Wilhelm von Hasenburg, Albrecht von Koldicz,
Alesch von Sternberg, Hinke von Rotemburg, Jaroslav von der Duben, Jan von
Ausik bekennen vnd tun kunt offintlich etc, das der allirdurchluchtigiste fürste kunig
Sigmund, röm., czu Vngern, Dalmacie, Croacie etc. und kunig zu Behem, vnsir
gnediger liber herre, mit dem edlen Puothen von Czastolowicz abgerechent hat vmb
23*
