A. XVI.
PŘÍKLADY LISTŮW HOLDOWNÍCH
Z XV STOLETÍ.
(Prwní dwa wzati jsau a originálu w archivu Třebonském; ostatní wšickni pocházejí
z též knihy někdy kláštera Chotěšowského, nám od důst. p. J. H. Karlíka půjčené, z které sme
i zápisy nahoře na str. 34 — 63 podané wybrali.)

1.
My purkmistr a radda města Hory Tábor, wám richtáři, konšelom i wšie
obci města Milewska přiezeň naši wzkazujem, přikazujíc přísně, aby konečně do
neděle nynie přištie tři sta žita čebrów a padesát kop grošów nám bez wýmluwy
dali. Pakli toho neučiníte, tehdy s pomocí boží wás k tomu wšelikým obyčejem
a nad to ohněm připrawíme, že naše rozkázanie musíte učiniti. Dat. fer. Vta ante
Augustini, annorum etc. tricesimo octavo, nostro sub secreto.
(L.S. appress.)

2.
My purkmistr a radda města Hory Tábor, wám richtáři a konšelóm se wší
obcí w Benešowě přiezeň naši wzkazujem, přikazujíce přísně, aby hned jakž tento
list uzříte, k nám s plnú mocí od swé obce wyslali, újezd, ježto k richtářstwí
wašemu přisluší, k témuž s sebú pojmúce, a s námi se smluwili. Pakli toho ne-
