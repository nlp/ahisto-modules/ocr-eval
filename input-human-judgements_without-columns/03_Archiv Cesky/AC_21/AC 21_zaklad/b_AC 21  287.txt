z roku 1437. 287
geschickt hat: als bitten wir euch mit vleisz vnd begern, das ir uns dieselben wein
unverczogenlich her lasset fwren auf vnser gelt; und was die fuer gesteen wirt,
das wellen wir von stund an, so sy her bracht werden, schaffen schon zu beczalen.
— Geben zu Prag am mittichen nechsten vor corporis Christi anno eiusdem etc
tricesimo septimo. Commissio propria domine regine.
Dem edlen vnserm besunder liben vnd getrewen Vlrichen von Rosenbergk.
Orig. pap. v Třeboň. arch. hist. Nr: 388. - (A).
21.
Oldřich z Rožmberka žádá Smila z Křemže, aby lidí jeho ve vesnicích, jež si proti jeho
zápisům od císaře vyprosil, na pokoji nechal dle úmluvy, kterou spolu mají. — V Krumlově
17. června 1437.
Službu svú vzkazujem, pane Smile milý! Zpraveni sme, že si na ciesařově
Mti, pána našeho milostivého, vyprosil některé vesnice, kteréž prve od JMti v zá-
pisích našich máme, a že mieníš těm lidem našim škoditi. Myt sme tomu věřiti
nechtěli, by ty takovú prosbu proti nám učinil, věda, že od JMti prvé zápisy na
ty vesnice i jiné sboží jmáme. A nám se nezdá, by ty takovú prosbu od ciesařovy
vieš. Protož věříme, že nám dle té umluvy, kterúž spolu máme, jakož dobře vieš,
našich lidí u pokoji necháš. A na toť žádáme odpovědi tvé listem svým při tomto
poslu. — Datum v Krumlově feria II. post Viti anno etc XXXVII°.
Oldřich z Rosemberga.
Strenuo militi domino Smilloni de Krzems.
Na rubu: Smilovi z Kremže. Koncept na papíře kníž. arch. Kruml. I. 5 BPa. 5 i. — (G).
O zápisech cís. Sigmunda viz článek Theod. Wagnera: Jan Smil z Křemže v Časopise musea král.
Čes. 1888 str. 180.

22.
Jan Smil z Křemže Oldřichovi z Rožmberka: že on vesnic sobě nevyprosil, nýbrž císař koupil
od něho jeho dědictví, dal mu za ně Prachatice s vesnicemi; že má majestát na zboží Korunské
dříve, než Oldřich; zboží Pořešínské že již ve dskách nalezl, a že nevzal nabízeného mu zboží
Svérazského. — V Prachaticích 18. června (1437).
Službu svú vzkazuji Tvé Mti, urozený pane milý! Jakož mi Tvá Mt píše
a jsa zpraven, že bych já na ciesařově Mti vyprosil některaké vesnice: rač Vaše
Mt věděti, že sem na Jeho Mti já nic nevyprosil, než Jeho Mt pán mój milostivý
pobiedil jest mne k trhu a kúpil jest u mně dědicstvie mé. A za to dal mi jest
Prachatice a ty vesnice, jakož Tvé Mti tajno nenie. A také ja[k Va]še Mt píše, že
jmáte na to zápis: rač věděti, žeť já jmám prvé, než Vaše Mt, [od] krále nebož-
tíka *) pána našeho. A to Tvá Mt dobře vie, že jmám majestát na to sbožie na
Korunské; to sem Tvé Mti pravil na Krumlově, když sem byl u Tré Mti. Protož,
