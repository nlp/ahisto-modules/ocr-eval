z let 1420-1422. 281

10.
Rakouský vévoda Albrecht Oldřichovi z Rožmberka: o osvobození několika Rakušanů Husity
zajatých. — Ve Vídni 16. ledna 1421.

Albrecht von gotes gnaden herczog ze Osterreich etc. Edler, sunder lieber!
Als die vngelaubigen Hussen Hainreichen von Gapelpach, Ernsten den Dorner,
Hannsen den Reiffendorffer vnd ander ettleich ir gessellen, vnser landlewt in deinem
dinst, geuangen habent vnd die du von in vmb ain summ gross ausgenomen hast
auf ain widerstellen: begern wir vnd bitten dich mit fleiss, daz du dir die egenanten
viser landlewt lassest ernstleich empholhen sein, damit sy der vencknuss mit eren
ledig werden, wan dich das eret vnd künftigen frumen bringen mag, als du selb
wol versteen macht. Geben ze Wienn an phincztag vor sand Anthonien tag anno etc
quadringentesimo vicesimo primo.
Dominus dux in consilio.
Dem edeln vnserm besunder lieben Vlreichen von Rosemberg.
Orig. na papíře v Třeboňském archivu, hist. Nr. 221. – (A).

11.
Král Sigmund Oldřichovi z Rožmberka o jeho dědičné opravě na Svérazi a Zátoni, a o zápisu
platů klášterských z nich Jindřichovi z Drahova. — V Prešpurce dne 6. května (1422).
Zikmund z Božie milosti Římský král, po vše časy rozmnožitel říše, a Uherský
a Český etc král. Urozený věrný milý! Jakož jsme statečnému Jindřichovi z Dra-
hova, věrnému našemu milému, čtyři sta kop zapsali na tom zbožie Svéráz a Záton
za svú službu, ježto je nám učinil, aby ty platy, ježto nám od kláštera do komory
našie dávají, vybral až na sražení te summy; jakož pak ten list, ješto dřieveřečený
Jindřich od nás jmá, lépe svědší. Tak jsme srozuměli, že ty jisté platy zdviháš
a sě v ně viežeš některakým listem, ježto od našeho milého bratra krále Václava
máš od svých přědkóv. A poněvadž jsme na přiepisu tvého listu znamenali, že tvoji
přědci i ty v tom listu viece zapsáno nemáte, nežli opravu dědičnú, a my tobě
v uopravu sáhnúti nechcem, než neznamenáme, by platy od toho jměl vzieti: protož
od tebe žádáme i prosíme se všie snažností, aby tomu Jindřichovi z toho zbožie
dvě stě kop vydal, a my jemu ostatek toho dluhu jinde ukážem. A ufáme, že to
pro nás učiníš, aby jemu dále práce nečinil, neb nám velikú libost na tom ukážeš.
— Dán v Prešpurce tu středu na svatého Jana v oleji vařeného leta královstvie
našich etc.
De mandato domini regis.
Nobili Ulrico de Rozenberg, fideli nostro dilecto.

Orig. papírový s pečetí odpadlou. Kníž. archiv Krumlov. I. 3 KS. 28 y. — (G).
Datum v Prešpurce ve středu na sv. Jana v oleji (t. j. 6. května) se hodí, neboť král Sigmund
skutečně byl v Prešpurce roku 1422 dne 6. května, a byla ten den středa. Že scházejí léta i podpis písařův
Archiv Český XXI. 36
