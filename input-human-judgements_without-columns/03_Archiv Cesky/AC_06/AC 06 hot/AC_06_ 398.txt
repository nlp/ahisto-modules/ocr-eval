398
Paběrky písemností wšelikých z let 1421—1438.
těch najdále stáwalo; a toho krále Sigmunda za krále a pána dědičného této koruny
České, pro jeho nehodnost, nikoli nemieti ani přijímati, než jemu sě wedle toho
města hlawnieho a wšech jich pomocníków protiwiti, dokudž nás najdále stáwati bude,
a ižádného jiného za krále této koruny nepřijímati, kohožby město Pražské swrchu-
psané s swými pomocníky nepřijalo w časech budúciech, kolikrátby toto králowstwie
skrze zbawenie krále osiřalo. Slibujíce dále ižádných úředníków w městě našem
nepřijímati ani pod ně slušeti, než ty kteříž nám od pánów Pražských wysláni, dáni
a usazeni budú, w ty slušeti a jich poslušni býti máme, se wšemi požitky dřiewe
na krále a úředníky jeho sprawedliwě slušejícími zřieti a hleděti, nikam z země
w nižádných přech na wyššie práwo sě neodmietajíc ani odwoláwajíc, beze wšeho
odpieranie, tak dlúho, donižby pán buoh wšemohúcí této koruně České pána a krále
nedal, a ten jednostajným swolením wšie obce Českého králowstwie nedošel koruno-
wánie řádného a w této zemi obyčejného.
Kteréžto kusy swrchupsané, i každý z nich, slibujem a máme wésti a držeti
i zachowati úplně a docela, beze wšeho přerušenie, pod ztracením cti i wiery našie,
kteréž sě tiemto listem odsuzujem, jestližebychom na čem který kus swrchupsaný
přestúpili, jehož buože nedaj, a to na nás očitě anebo hodným swědomím bylo do-
líčeno a swětle dokázáno.
Tomu na swědomie a jistotu pewnější pečet našeho města wětšie s naším
dobrým wědomím i s wolí naší k tomuto listu jest přiwěšena. Jenž jest dán a psán
léta od narozenie syna buožieho tisícieho čtrstého a potom jedenmezcietmého léta,
w středu před swatým Duchem.

5.
Stawowé Morawští zapisují se pánům Českým k zachowáwani čtyr artikulůw Pražských, k ne-
uznání Sigmunda za krále etc.
Bez datum (1421 m. Mai) (Kopie arch. Třebon.)
My N. haubtman, Petr z C. etc. wyznáwáme tiemto listem wšem, ktož jej
uzřie neb čtúc uslyšie, že sme takowúto úmluwu a smlúwu s múdrými a opatrnými
pány i obcí slawného města Pražského a s urozenými pány zemskými, obcí Táborskú,
s zemany, rytieri, panošemi, městy i wšemi obcemi králowstwie Českého k zákonu
božiemu příchylnými učinenie (sic) mocí tohoto listu činíme, že ty čtyři kusy prawé
a spasitedlné a písmem swatým stále pewně utwrzené, o kteréž sú sě swrchupsaní
páni i obce wěrně zasadili, dobrowolně sme přijali a k nim přistúpili a mocí tohoto
listu přijímáme a přistupujeme. Kteřížto čtytie artikulowé slowo od slowa takto
stojie psáni: Najprw aby slowo božie po králowstwí Českém a markrabstwie Mo-
lawském swobodně a bez překazy od křestanských kněží bylo zwěstowáno a kázáno.
