442
Paběrky písomností všelikých z let 1421—1438.
44.
Odpowědné listy pánůw a rytířůw Českých hojných proti knížatům Saským w Míšni k ruce
pánůw Plawenských dané.
Roku 1437 okolo 22 Mai. (Orig. w arch. Drášďanském.)
Wězte, wysoce urozená kniežata a páni, páni Fridrichu, Sigmunde i Wiléme
bratřie wéwody Saské, lankrabě Durinské a markrabě Mišenské, že my Jan a Přibík
bratřie Zajieci z Waldeka i s swými služebníky chcme waši i wšech wašich nepřie-
telé býti pro tak mocné weliké bezprawie, kteréž wy urozeným pánóm Hendrichowi
staršiemu i mlazšiemu purkrabiem Mišenským a pánóm Plawenským našim zwláště
přátelóm dobrým činíte, a proti bohu a práwu, jeho béřete (sic), jímž je Ciesařowa
Mt pán náš milostiwý milostiwě darowal i pójčil jim, a ježto sú i práwem získali.
A swú čest poctiwě ohražujem, i chcem w jich míru i nemíru státi. Dún tu středu
po sw. Duše, an. dom. M°cccc°xxxvij°.

(2 sigilla appressa.)
W týž smysl i w táž slowa psaní odpowědná dali (dato eodem):
Matěj z Nemosic. (insign. tři kapry.)
Wáclaw Myška z Hrádku.
Diwiš Bořek z Miletínka sed. na Kunětické hoře.
Bořek z Hrádku a Krištof z Chwalkowa.
(Bořkonum insigne idem.)
Aleš z Boskowic a z Richwaldu, Wilém z Mečkowa, Jan Sekretář
z Kostelce, Diviš z Nenabylic, Bořek z Nowéwsi, Braňsaud z Ne-
nabylic, Licek z Chwalkowa, Oldřich z Pawlowa.
Jan z Bezdědic sed. na Kokoříně.
Waněk z Miletínka sed. na Hradištku. (insign. Bořkonum.)
Jiřík z Dubé sed. na Wysmburco, Petr z Náchodu sed. na
Adrspachu.
Jetřich z Janowic sed. na Chlumci, Wilém z Ilburka sed. na
Haušteině.
Záwiše a Kuneš bratřic z Klinštýna.
