150 1437.
imperpetuum virtute presencium, videlicet, quod exnunc in antea
omnes et singuli cives utriusque sexus, incole et inhabitatores
supradicte civitatis et suburbiorum Brode Bohemicalis et ipsorum
heredes et successores imperpetuum libere possint et valeant suas
universas et singulas possessiones, hereditates, proprietates, allo-
dia, agros, redditus, domus et bona sua mobilia et immobilia, in
quibuscunque consistant, intus et extra easdem civitatem et sub-
urbia aut alibi, ubicunque talia sita noscuntur, quibuscunque
eciam possint vocabulis designari, cuicunque homini seculari seu
personis secularibus vendere, legare, donare, testari in vita vel
in morte et iuxta sue voluntatis arbitrium ordinare iure heredi-
tario possidenda, sic tamen, quod huiusmodi testamentum iuxta
consuetudinem ab antiquis observatam temporibus et ad instar
nostre Maioris civitatis Pragensis celebretur. Sin autem aliquem
seu aliquos ex dictis civibus, incolis et habitatoribus predictarum
civitatis et suburbiorum, viris et mulieribus, sine donacione, testa-
mento, ordinacione seu disposicione, non relictis legittimis here-
dibus seu legittimo herede masculini vel feminini sexus, ab hac
luce migrare contingeret, extunc universe et singule possessiones,
hereditates, proprietates, allodia, agri, redditus et bona mobilia
et immobilia, in quibuscunque rebus consistant, intus et extra
supradictas civitatem et suburbia, ubicunque talia sita noscuntur,
ad proximiores et propinquiores taliter decedentis seu deceden-
cium consangwineos masculini et feminini sexus tunc superstites
libere ac iure hereditario perveniant et devoluantur absque im-
pedimento cuiuscunque; sic tamen, quod ille vel illi, cui vel quibus
bona huiusmodi immobilia per aliquem ex civibus seu incolis, ut
premittitur, donabuntur vel legabuntur, vel ad ipsum devoluta
fuerint, in ipsis bonis residenciam faciat personalem vel donet
vel vendat huiusmodi bona homini seculari in eisdem bonis ex-
tunc reşidere volenti et residenciam facienti: proviso insuper, quod
uxores relicte de supradictis civitate et suburbiis decedencium in
suis dotibus ac aliis bonis sibi datis vel in testamento assignatis
pacifice permaneant et gaudeant de eisdem, sicut hoc alias ibidem
extitit antiquitus observatum, salvis tamen nostris et regni nostri
Bohemie serviciis in premissis. Ceterum prefatis civibus, incolis
et inhabitatoribus Brode Bohemicalis per hoc indultum concedi-
mus et ipsis indulgemus, quod ipsi in antea in causis iudiciariis
