94 1434.
notamus in mente, effectu ad oculum realiter demonstremus, ecce,
dum pridem in peculiari regno nostro Bohemie secta Wiklefistica
Johannis Huss et Jeronimi dampnate memorie falsis imbuta dog-
matibus, Domini timore posposito, spiritum vertiginis assumendo,
sacrosanctam fidem orthodoxam, supra solidum firmamentum petre,
qui Christus est, stabilitam, suis perversitatibus heresumque fo-
mentis subvertere, inconsutilem salvatoris nostri tunicam caninis
dentibus lacerare velutque singularis ferus et aper de silva vi-
neam domini Sabaoth depasci et demoliri, nec non fidelem Christi
populum contagiosum reddere et a via salutis in devium et in
laquei scopulos abducere nequiter moliretur; ipsique heretici, deo
et ecclesie odibiles et rebelles, nedum creatoris nostri semper
venerandum nomen ore impudico et lubrico blasphemarent, sed
et sacratissimum divine eucaristie sacramentum, nostre salutis
viaticum, et reliquias sanctorum venerandas pedibus conculcarent
sacrasque edes polluerent ac pollutas et prophanatas ignis vora-
gine et alias funditus devastarent, Jhesu Nazareni, crucifixi do-
mini nostri, ac sanctorum eius truncarent ymagines et omni belua
crudeliores effecti immani sevicia christianum sanguinem in cru-
delitatis poculo absorbentes, in populum Domini, velut rampnus
in ligna sevientes, sue impietatis gladios in Christi fidelium con-
verterent viscera occisorum, katholicumque nomen e medio extin-
guere cupientes, etati et sexui non parcendo, religiosorum et deo
devotorum presbyterorum quorumlibet ac Christo sacratarum vir-
ginum carnes, omni spreto humanitatis officio, derelinquerent celi
volucribus et terre bestiis laniandas et inter alias nephandissi-
mas et execrandas suas abhominabiles factiones, quas perpetra-
runt hucusque et spiritu dyabolico obstinati perpetrare in dies
non desistunt, quosque nec lingwa depromet, nec calamus exarabit;
dum memorati Wiklefiste Husiteque heretici suorum scelerum
fimbrias dilatarent, et inter ceteros in honorandos magistrum ci-
vium, consules et cives sive opidanos opidi nostri Pilsnensis, nobis
utique fideles et sincere dilectos, tamquam in christicolas et fidei
tetragonos immobiles et fideles huiusmodi pravitatis heretice ini-
micos et hostes, sue perversitatis aculeos acuissent ac replicatis,
iteratis, ymmo multiplicatis vicibus, armata manu valida per dies,
menses et annos prefatum nostrum Pilsznense opidum circumvallas-
sent ac eciam aggeribus et fossatis aliisque hostilibus municioni-
