426
CLXXXII.
1436, April 14, Krummau. — Abt Rüdiger, Prior Dietrich und
Johann der Kellermeister sowie der gesammte Convent zu Goldenkron
verkaufen dem Buzek von Ruben und dessen Erben den Hof in Ruben
sammt Zubehör um 160 Schock Groschen.

Orig. Perg. im Stiftsarchive zu Hohenfurt. Von mir bereits abgedruckt
in Font. rer. Austr. 2. XXIII. 266-267, N. 213. In böhmischer Sprache.
Wegen Ruben vergl. die Anm. zu N. CXLVII. Der Käufer war ein Haracher;
loco cit. p. 267. Von den Mitsieglern sei erwähnt, dass der Abt Sigmund
(Pirchan) von Hohenfurt nach etwa 1442 zum Bischofe von Salona erhoben
längere Zeit hindurch das Geschäft eines Weihbischofs in Passau versah und
Chwal von Chmelná vielleicht aus jenem Chmelná im Bezirke Krumman.
Pfarre Berlau stammte. Wegen Čstibor von Wlčetin vergl. auch N. CLXIV.
Anm. 1, S. 375.

CLXXXIII.
1486, April 15, o. AO. — Abt Rüdiger und der Convent zu Golden-
kron bekennen, dass Herr Ulrich von Rosenberg zum Zeugniss und auf
ihre Bitte sein Sigel an den vorhergehenden Kaufbrief gehängt habe.

My Ruthger opat a Dyetrzich przewor, Jan klicznik
a wssken conuent classtera Swate Coruni wyznawame tiemto
listem wssem, ktoz gey czisty neb cztucze slisseti budu, tak
iakoz gsme dwuor w Rownem se wssim przislussenstwim pro
nasse y pro classterske dobre slowutnemu panossy Buzkowi
8 (sic, z) Rowneho y gebo buduczim prodaly, iakoz list kteryz
gsme Buzkowi na to pod nassimi peczetmy daly plnyegi
vkazuge, protoz prosilisme vrozeneho pana pana Oldrzicha z
Rozmberka, aby k tomu listu kteryz gsme Buzkowi daly.
swu peczet na swiedomye prziwiesil. A toho na pewnost, ze
gest k tomu nasse dobra wuole a ze gest pan z Rozmberka
swrchupsany peczet swu k tomu listu przywiesil na swiedomie
k nassie prozbie, nasse gsme peczety przywiesily k tomuto
listu. Genz gest dan leta od narozenie syna bozieho tisiczieho
cztrsteho trzidczateho ssesteho, tu nedyely na prowod.1

1 Die Benennung ,Probéltsuntă´ (auch der weisse Sonntag, Sonntag Quasi-
modo) ist wenigstens auch bei den Deutschen des Oberplaner Bezirkes
noch gang und gäbe. Die Urkunde, um deren Sigelung es sich geban-
delt, ist die nächst vorhergehende.
