429
postupiti a zasie y s tiemto listem wratiti bez otporu y bez
zmatku. To zwlasscze przidawagicz, gestli zeby tyz Smilek
ktery ribnik drziewe nebyli na tiech sbozich swymi naklady
vdielal, ze to czozby nadielo toho ribnika wynalozil gesstoby
swiedomim kragian tudiez wuokolij przisediсzich to pokazal to
gemu przipostupowanij wsij swrchupsanych ma zasie wraczeno
a s summu w tomto listu zapsanu zaplaczeno byti. A take to
bude moczi, kdyz a komuzby chtiel dati, zastawiti, smieniti
neb prodati w tez a takowe prawo, iakoz gemu samemu tiemto
listem gest zapsano. A ktoz koli tento list mieti bude s czasto-
psaneho Smilka dobru wolij a swobodnu, ten ma mieti wsseczko
prawo k tomuto zapisu, iakzto on sam. Na potwrzenie toho
nass cziesarzky magestat kazalisme prziwiesiti k tomuto listu.
Genz gest dan leta od narozenie sina bozieho tisiczieho cztyrz-
steho trzidczateho sedmeho w sobotu po swate trogiczi, let
kralowstwij nassich Vherskeho etc. padesatem prwem, Rzim-
skeho w sedmimezidczietmem, Czeskeho w sedminadcztem,
a cziesarzstwie w patem letie. Ad relationem Johannis de
Cunwald subcamerarii — (auf dem Umbug links.)

Original auf Pergament im fürstl. Schwarzenbergischen Archive zu
Krummau. An pergamener Pressel hängendes, bis auf eine geringfügige Ver-
letzung an der Umschrift sehr gut erhaltenes grosses Sigel von gewöhnlichem
Wachs. Man erblickt auf der Vorderseite im Mittelfelde den Kaiser in vollem
Schmucke auf einem Throne sitzend, mit Krone, Scepter und Reichsapfel: zu
Seiten des Thrones aber je einen zweiköpfigen Adler, welcher mit jo einem
Schnabel und je einem Fuss vier Schilde mit dem Reichs-, dem böhmischen
und dem ungarischen (dieses zweifach: Doppelkreuz und Querbalken) Wappen
hält. Unter dem Thron in einer Nische ein Schild mit dem Wappen Luxem-
bargs, zur Linken des kaiserlichen Hauptes aber ein Kreuz, dessen Balken
in Strahlen verlaufen. Umschrift: „SIGISMVNDVS . DEI . GRACIA
RO(MA)NORVM . IMPERATOR . SEMPER . AVGVSTVS.AC.HVNGARIE.
BOHEMIE . DALMACIE . | CROACIE RAME . SERVIE. GALICIE
LODOMERIE . BVLGAERIEQ' . REX . ET . LVCEMBVRGENSIS . HERES´.
Im Mittelfelde der Rückseite ein grosser Doppeladler und lautet die Umschrift:
,† AQVILA . EZECHIELIS. SPONSE. MISSA . EST. DE. CELIS. VOLAT.
IPSA . SINE. META. QVONEC. VATES . NEC. PROPHETA. EVOLAUIT.
ACIUS´. Die Anfertigung dieses Sigels wurde schon im J. 1417, demnach
16 Jahre vor dem Beginne des Kaiserthums Sigmunds, befohlen. Th. Sickel
im Anz. f. Kunde deut. Vzt. 1872, S. 14.
