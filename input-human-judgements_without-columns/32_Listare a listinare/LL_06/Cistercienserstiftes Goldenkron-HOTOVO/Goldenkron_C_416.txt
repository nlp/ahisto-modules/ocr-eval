416
nemen wirdest. Und wan du desselben kloster güter meniger
innhast, als wir vernemen, begern wir an dich und emphelhen
dir auch ernstleich von des egenanten unsers herren des
kunigs wegen, daz du dem abbt und dem conuent daselbs zu
der Heiligen Kron aller irer güter so du innehast unuerczo-
genleich abtretest und in die inantwůrttest und si auch daran
vngeirret lassest. Und was deins willen darinn sey, das lasse
uns an deinem brief verschriben widerumb wissen. Geben zu
Wienn an montag vor sand Michels tag, anno etc. quadrin-
gentesimo vicesimo secundo. D. dux in consilio.
Original auf Papier im fürstl. Schwarzenbergischen Archive zu Krum-
mau. Das Sigel, womit das Schreiben verschlossen war, ist in Papier ge-
druckt, hat im Mittelfelde einen Fünfpass mit Wappen und sonst noch ein
Umschrift; beides unkennbar oder unlesbar.

CLXXIV.
1425. – Verzeichniss der vor den heranrückenden Hussiten in die Burg
zu Krummau geflüchteten Kleinodien des Klosters Goldenkron.
Clenodia monasterii Sanctae Coronae per con-
ventum ad fideles manus commendata in castro
Crumloviensi reposita anno 1425. 1
Quum Husitae de Praga intrarent Austriam cum magno
exercitu ad devastationem.
Item imago beatae virginis deaurata.2 Item una crux
cum pede deaurata. Item cruces parvae. Item duae coronulae.

1 Dass das Jahr 1425 möglich ist, kann man aus Palacky, Gesch. von
Böhmen. III. b. 388-389 und 397-398, ersehen. Darnach könnte die
Uebertragung des Schatzes nach Krummau ebenso gut im Frühling als
im Herbste jenes Jahres bewerkstelligt worden sein. Wahrscheinlicher
ist die letztere Jahreszeit. Denn am 13. Mai kam ein Heer der Tabo-
riten und Waisen vor Wittingau an, musste aber von hier mit Schande
abziehen und wandte sich dann gegen Gratzen, welches erobert und in
Brand gesteckt ward, wobei auch viele Urkunden Herrn Ulrichs von
Rosenberg vom Feuer verzehrt wurden. Später jedoch, am 12. November.
sehen wir die vereinigten Schaaren der Taboriten, Waisen und Prager
Klosterbruck bei Znaim mit Sturm nehmen und hierauf in Oesterreich
eindringen, wo sie nach mehrtägiger Belagerung der Stadt und Burg
Rotz dieselben am 25. November eroberten. — Sonst ist dieses Inventar
zu vergleichen mit dem nachträglich aufgefundenen vom J. 1418; siehe
N. CLXVI a.
2 Eine im J. 1664 niedergeschriebene kurze Geschichte des Klosters Gol-
denkron (MS. im Hohenfurter Stiftsarchive, in lat. Sprache) bringt gleich-
