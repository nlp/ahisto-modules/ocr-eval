270
FALSA. Č. 377: 23. května 1432.
Třeboň, Schwarzenb, archiv: Hluboká, I. 1 A𝛽 č. 13, or. perg.
Schmidt, MVGDB. XXXII, 322 a XXXIII, 193 (regg.). — RI. XI, 9155 („Fälschung)". — Sedlá-
ček, Zbytky register král. 222 č. 1626 (mezi padělky).
Schmidtův výklad falsa (tamže XXXIII, 193), který však třeba opraviti a doplniti, podepřel
Mareš (ČČH. I, 377), odsoudiv listinu jakožto padělek po stránce vnější (srv. též Sedláček na uv.
m.). Srv. výše č. 372 a poznámky k tomuto listu, dole uvedené.

My Zigmund, z božie milosti římský král, po vše časy rozmnožitel říše a uher-
ský, český, dalmacký, charvatský etc. král, přikazujem všem našim přísně, chtiec
tak jmieti, pod naší milostí lidem našim, kteříž jsú příslušeli klášteru Milevskému1
i také příslušejíciem k zbožie Týnskému na Vltavě, lidem arcibiskupstvie českého
diocesis pražské, i lidem našeho zámku Hluboké, kteréž jest držal někdy řečený
Hájek4 ku purkapstvie od uroz. Václava z Dubé, věrného našeho milého, v ta
doba také purkrabie pražského, a potom po téhož Hájka smrti s Hodětína řečený
Kunát s Winterberka, kterýž jest Kunát měl dceru Hájkovu, a nemohúce s ním
dokonati o ty škody, kteréž jest na nás četl, a chvátajíce do německých zemí naší
jiezdú pro mnohé potřeby pilné našie a svaté říše: i poručili sme uroz. Oldřichovi
z Rozemberka, věrnému našemu milému, aby sě smluvil s dřéve řečeným Kunátem
vo hrad náš Zviekov i o vězně i o všecky jiné věci tak, jakož je on k tomu hradu
sám držal, jakožto pak naši listové uroz. Oldřichovi z Rozemberka na to danie šíře
svědčí; protož přikazujem všem rychtářóm, hajným, úředníkuom i obecným lidem
všelikakým toho našeho svrchu jmenovaného zbožie, aby všichni tak, jakož jsú za
Hájka i za Kunáta, holdovali i platili i všiem tyemž obyčejem poddáni byli a po-
slušni byli uroz. Oldřicha z Rozemberka, věrného našeho milého, až do výplaty
naší neb našich budúcích vedle listuov jeho od nás jemu na to daných. Toho na
svědomie svú jsme královskú pečet k tomuto listu přitisknúti kázali. Dán
v Parmě léta po božiem narození tisícieho čirstého a potom XXXII°, v pátek
před svatým Urbannem, let království našich uherského etc. v XLVI°, římského ve
XXII. a českého ve XII. létě.

Menší pečeť kr. Zikmunda, jež byla přitištěna vzadu, odpadla. — Na rubu současným
pismem: Popel. - Pozdějším písmem: Poručení krále Zigmunda Milivským, Teynským a Hlubockým,
aby poslušni byli p. Woldřicha z Rožm. 1432. — Něm. reg. 17. stol.

a) tak or.
1) viz Schmidt, MVGDB. XXXIII, 193. 2) viz Schmidt tamže. 3) Hlubokou držel v zá-
pise král. Mikuláš z Lobkovic (viz pozn. u č. 367). 4) Jan Hájek z Hodětína byl nejspíše pur-
krabím hlubockým: Sedláček, Hrady VII, 134 se o tom nezmiňuje, výklad jeho je na tomto místě
vůbec chybný (srv. Schmidt na uv. m., srv. též výše pozn. u č. 367); srv. dále pozn. 8. 5) Václav
Dubé a z Leštna, nejv. purkr. pražský r. 1420. 6) Kunát Kaplit ze Sulevic a na Vintrberku,
7) totiž Kunát. 8) list z 8. června 1431 výše č. 182; srv. však vyznání Kunátovo z 29. června
1433 (č. 230), že Oldřichovi postoupil jen Zvíkov, avšak vsi hlubocké, kterých se asi zmocnil po
smrti svého tchána Jana Hájka z Hodětína, vrátil Mikulášovi z Lobkovic. 9) naráží se tu na
podvrženou listinu z 31. pros. 1421 (viz č. 367). 10) Mikuláš Popel z Lobkovic, pro něhož nej-
spíše byl určen původní list, psaný na tomto perg.; viz Mareš, ČČH. I, 377.
