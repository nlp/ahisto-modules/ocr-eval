190
1423 leden 11 — květen 9. čís. 307, 308.
příslušný lenní slib složí do rukou jeho plnomocného zá-
stupce Albrechta z Koldic, hejtmana svídnického a javor-
ského.

Orig. perg. 34x27–9 cm, lat. K listině jest při-
věšena na černožlutých hedvábných nitích ku-
latá majestátní pečeť krále Zikmunda z vosku
přirozené barvy.
Na plice: Ad mandatum domini regis domino G[eorgio],
e[piscopo] Pat[aviensi], cancellario referente Franciscus, prepo-
situs Strigoniensis.
Na rubu: R. Henricus Fye. — Wy koning Sigmund vorleyet
dy prowenschaft Gebnen dem hirczog Ledwig von Coben und seynem
geerben alzo, das her swere hern Albrecht von Colditcz an seyner
statt. — N. 8.

Lenní reg. č. 8. — Pražské rep. č. inv. 426, č. rep. 228.
Regesta imperii XI., str. 383, Č. 5440.

308.
1423, k věten 9. Košice.
(Jenž jest dán v Cossicziech léta Božieho narozenie
tissícze čtrstého a v třietiemmezsczietmiem létě
tu neděli před Božiem vstúpení.)

Jan z Opočna slibuje Zikmundovi, králi římskému,
uherskému a českému, že bude věrně pečovati o majetek
sirotků po neb. Janu z Hradce, jejichž poručníkem byl od
krále ustanoven. Veškeré dluhy, které za ně zaplatí z vlast-
ních peněz, obdrží po jejich dospělosti; kdyby v nynějších
neklidných válečných dobách bylo něco ze svěřeného zboží
bez jeho viny ztraceno, nebude mu to k »škodě a hanbě
přičteno«.

Orig. perg. 39x23—6 cm, čes. K listině jest při-
věšeno na pergamenových proužcích osm kulatých
pečetí: 1. Jana z Opočna z černého vosku, 2. Jana
z Michalovic z červeného vosku, 3. Viléma Zajíce
z Hasenburka z červeného vosku, 4. Jindřicha
z Elsterbergu z červeného vosku, 5. z 1/3 zničená
Aleše ze Šternberka z černého vosku, 6. Hynka
Hlaváče z Lipé z černého vosku, 7. Viléma z Rie-
senburka a a ze Švihova z černého vosku, 8. Frie-
dricha z Kolovrat z černého vosku,
Na rubu: Nus. 211. Vormundtschaftlsche Verbü[r]gung des Jo-
hann von Opoczna uber die Johann von (následuje škrtnuté »Hra-
deczische«) Waysen de No. 1423.
Rosenthal DZ, lit. A. č. 211. — Vídeňské rep. č. 1247.
Archiv Český VI., str. 409.
