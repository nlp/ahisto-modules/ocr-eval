Čís. 280, 281.
1419 říjen 26 — 1420 únor 19.
173
Oldřicha z Tecku (von Teggk), Friedricha z Helfensteinu,
Albrechta z Rechbergu. Hanuše ze Staaden (von Stadgen)
a jiných würtenberských velmožů, Zikmunda, krále řím-
ského a krále uherského i českého, za vrchního pána všech
jejich říšských a českých lenních statků.

Orig. perg. 40x26-5 cm, něm. K listině jest při-
věšena na pergamenovém proužku kulatá pečeť
Rudolfa ze Sulzu z vosku zelené barvy.

Na rubu: Wirtenberg. — M cccc xix. — Připsán a corigován.
– T. B. Fol. 135.
Knihy priv. A II., fol. 135pv—136, B II., fol. 135pv—136.
Rosenthal KA, lit. A, č. 176. — Vídeňské rep. č. 1221.
Lünig, Codex Germaniae I., str. 1431, č. CCCL.

281.
1420, únor 4. Vratislav.
(Datum Wratislavie anno Domini millesimo
quadringentesimo vigesimo quarta die Februarii,
regnorum nostrorum anno Hungarie tricesimo
tercio, Romanorum vero decimo.)

Zikmund, král římský, uherský a český, nařizuje
Václavovi z Dubé a z Leštné, podkomořímu království
Českého, aby na úřad notáře pražského ungeltu uvedl po
Erhardovi, pražském měšťanovi, Jana Ulmannova, rovněž
měšťana pražského.
Orig perg. vlhkem poškozen 37x19–7 cm, lat.
K listině jest přivěšena na pergamenovém prouž-
ku kulatá, uprostřed prasklá majestátní pečeť
krále Zikmunda z vosku přirozené barvy.

Na plice: Ad mandatum d. regis d. Jo. episcopo Luthomislensi
referente Michael de Priest, canonicus Pragensis.
Na rubu: Rta.
Rosenthal MR, č. 30. — Vídeňské rep. č. 1222.
Regesta imperii XI., str. 281, č. 4002.

282.
1420, únor 19
(Geben am montage noch deme sontage, als man
in der kirchen Gotis singet Esto mishi in deum
etc. noch Cristi gebort firczenhundert jar und
dornoch in deme czwenczigsten jar.)
