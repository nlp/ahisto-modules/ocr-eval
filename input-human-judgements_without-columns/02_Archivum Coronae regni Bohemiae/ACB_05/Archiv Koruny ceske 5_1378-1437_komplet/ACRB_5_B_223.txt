Čis. 357, 358.
1437 bezen 14 — březen 18.
223
funfczigisten, des Romischen im sibenundczwen-
czigisten, des Behemischen im sibenczehenden und
des keisertums im vierden jaren.)

Zikmund, císař římský a král uherský i český, po-
tvrzuje z moci českého krále měšťanům města Lauban
(Luban) všechna privilegia, která obdrželi od císaře
Karla IV., krále Václava IV. a od markrabat brani-
borských.
Orig. perg. 54x28-9 cm, něm. K 1istině jest na
pergamenovém proužku přivěšena oboustranná
kulatá majestátní pečet císaře Zikmunda z vosku
přirozené barvy.
Na plice: Ad mandatum domini Imperatoris Petrus Kalde, pre-
positus Northusiensis,
Na rubu: Rta. — Confirmatio civibus de Luben.— N. 245.
Confirmatio Privilegiorum der stadt Luban von Kayser Sigismundo.
1437.
Rosenthal DZ, lit. A, Č. 245. — Vídeňské rep. č. 1294.
J Čelakovský, O registrech, str. 125. – Regesta imperii XI. 2,
str. 396, č. 11.717.

358.
1437, březen 18. Praha.
(Geben zů Prage nach Crists geburt vierzechen-
hundert yar und darnach in dem sybenunddrissi-
gisten yare am nechsten montag vor Sant Bene-
dicten tag, unserr reiche des Hungrischen etc. im
funfzigisten, des Roemischen im sybenundzweintzi-
gisten, des Behemischen im sybenzechenden und
des keysertumbs im vierden jaren.)

Zikmund, císař římský a král uherský i český, po-
tvrzuje listinu krále Václava IV. ze dne 15. května 1417,
obsahující rozhodčí výrok Hynka Berky z Dubé ze dne
2. prosince 1416 ve sporu mezi radou a obcí města Žitavy.

Orig. perg. 68x45—8 cm, něm. K listině jest při.
věšena na černo-žlutých hedvábných nitích dvou-
stranná kulatá majestátní pečeť císaře Zikmun-
da z vosku přirozené barvy.
Na plice: Ad mandatum domini imperatoris domino G(eorgio)
cancellario referente Petrus Kalde, prepositus Northusiensis.
