#!/bin/bash
set -e -o xtrace -o pipefail

INPUT=/var/tmp/ahisto-2020-08-11/'Knihovna podle ID'
CACHE=output-ocr-google-lowres-flattened

input_dirnames() {
    ls "$INPUT" | grep -E '^[0-9]*$' | sort
}

input_filenames() {
    cd "$INPUT" && input_dirnames | parallel --bar --nice 10 --halt 2 -- 'find {} -type f' | grep -E '^[0-9]*/[0-9]*\.[^/]*$' | sort
}

OUTPUT_UNFLATTENED=../public_html/2021-07-26-output-ocr-google-lowres-`input_filenames | wc -l`
OUTPUT="$OUTPUT_UNFLATTENED"-flattened

mkdir -p "$OUTPUT"
input_dirnames | (cd "$OUTPUT" && parallel --bar --nice 10 --halt soon,fail=100% --jobs 48 -- 'mkdir -p {}')
input_filenames | parallel --bar --nice 10 --halt soon,fail=100% --jobs 48 --joblog google-vision.joblog --resume-failed -- "if test -e '$CACHE'/{.}.json || test -e '$CACHE'/{.}.error; then test -e '$CACHE'/{.}.json && cp '$CACHE'/{.}.json '$OUTPUT'/{.}.json; test -e '$CACHE'/{.}.error && cp '$CACHE'/{.}.error '$OUTPUT'/{.}.error; touch '$OUTPUT'/{.}.cached; else bash scripts/google-vision-annotate.sh google-api-key '$INPUT'/{} '$OUTPUT'/{.}.json '$OUTPUT'/{.}.error; fi"
input_filenames | nice -n 10 python3 -m scripts.convert_google_ocr_json_to_text None "$OUTPUT"
