390 D.V. Wýpisky práwní a saudní

174.

1437, m. Oct. Quinto Matthiae, A. 5.

In causa, jakož Johannes de Risemberg

et de Rabie mluwil od sirotkuow někdy

Držkrajowých Stoklasowých, majíli wlásti

swým zbožím, léta majíc? a otázán ode
pánów pan Jan, kterýž jest od sirotkuow

mluwil, jsúli ti sirotci pacholíci čili dě-

wečky, a on řekl že jsú děwečky, a že
pacholíka nenie, a by byl pacholík maje

léta, požíwalby zbožie swého wedle práwa

země této; ale poněwadž sú děwečky, a
Wáclaw Warlich jich strýc sprawedliwý
a společník jest dskami: nalezli sú za
práwo, a pan Zbyněk Zajiec z Hazmburka
a pan Zdeněk z Rožmitála dominorum
potaz exportaverunt: že Wáclaw Warlich
má jimi wlásti w jich zboží, jakožto jich
strýc a společník wedle řádu této země,
a má jim sprawedliwě učiniti. A jakož

Warlich žádal na pánech, aby bylo po-
wědieno, w které chwíli mají jemu zbožie

sstúpiti sirotčieho a listy s sirotky wydati:

tak sú páni kázali, aby se toto stalo Wá-

clawowi Warlichowi od této neděle naj-

prw příštie w šesti neděléch pořád zběh-

lých, kteráž neděle dostane se tu neděli

po swatém Ondřeji. Actum ut supra.
Talmb. 78b. Mus. 17ab.

175.

1437, m. Febr. Quarto Matthiae….

Juxta causam, ubi Petrus de Piečiny

citat Častolora: ibi domini barones audita

querela actoris et tabulis ipsius dotalitii,

invenerunt pro jure, et Petrus de Zwieřetic

et Hynko de Čerwené hory dominorum

consilium exportaverunt: quod praefatus

Petrus actor est justus ad hereditates

in Piečině et alias hereditates in que-

rela nominatas virtute dotalitii sui, quod

habet post uxorem suam, et suam, et citatus

Častolor respiciat suos disbrigatores et

eorum hereditates pro nezpráwu, ut est

juris, et pro eo dederunt actori pro jure

obtento. Dedit memoriales. lbi, videli-

cet feria Vta post Oculi, Častolor prae-

dictus suscepit Petro actori pro inducto,

dominato et taxato, et pro hereditatibus

dotalibus condescendit juxta inventionem

factam per dominos barones. Actum anno

domini MCCCCXXXVII, quatuor tempo-

rum quadragesimae. Talmb. 776. Mas.

178-9.

176.

1437, 11 Jun. Quarto Matthiae...

Juxta causam, ubi Zdislaw Wlk citat

o hrad Krakowec. Ibi domini barones in

pleno judicio, auditis querela et tabulis

ipsius actoris, sicut vendidit Andres Špa-

lek Zdislao hereditates suas in querela

nominatas, et audita defensa citatorum et

responsione et literis eorum, quas An-

dreas Špalek super bona et hereditati-

bus in literis nominatis dedit, invenerunt

et Petrus de Zwieřetic et Jaroslaus Plichta

de Žirotin dominorum consilium expor-

taverunt: že tak jakož jest Ondřej

Špa-

lek dal statek swój Hrdoňowi Gutlowi

a Jankowi bratřím, a na to jim listy zdělal,

tak jakož ti listowé mezi nimi s obú

stranú ukazují a swědčie, a jakož je On-

dřej Špalek wložil týž statek swój a dě-

diny we dsky zemské trhem Zdislawowi
