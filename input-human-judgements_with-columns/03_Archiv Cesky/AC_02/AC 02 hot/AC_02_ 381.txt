ze starých desk zemských králowstwí Českého r. 1437. 381

(sic) Treb. 401a. Mus. 170. Vienn. 3483,
fol. 23a. sq. Talmb. 72ab.

153.
1437, 29 Jan. Quarto Matthiae, A. 3.
Domini barones in pleno judicio inve-
nerunt o přísahu, kteraká má býti obecně
zemanóm země této, et Petrus de Zwie
řetic et Hanušius Kolowrat dominorum
consilium exportaverunt:
že přísalia ta krátká, jakož jest byla
prwé pány uložena a ustawena, aby ta
šla před sě se zmatkem, jakož staré
práwo jest, a jménem tato: »na tom

přisahám, jímž mě Petr winí w této

žalobě, tiem jsem jemu ničímž newi-
nen; tak mi buoh pomáhaj i wšichni
swětí!« Pakliby kto takowý byl, žeby
chtěl přísahu učiniti, křiw jsa, a úřed-

níky by to shledáno bylo, žeby křiwě
ku přísaze šel: že úředníky takowý má
od přísahy zdwižen býti, a dán u wazbu
ku purkrabí Pražskému, a má býti po-

stawen před pány na súdě, a tu panský

nález trpěti.

Actum coram dominis baronibus praedic-

tis, anno domi. MCCCCXXXVII, fer. III

ante purificationem S. Mariae. Treb. 401ab.
Vienn. 3483. Talmb. 72b. Mus. 170a.

154.
1437, m. Jan. Quarto Matthiae, A. 3.
Serenissimus princeps et dominus,
D. Sigismundus Rom. Imp. et Boh. rex,

cum dominis baronibus in pleno judicio
residentibus invenerunt, et Ulricus de Ro-

senberg cu Petrus de Michalowic domi-

norum consilium exportaverunt:

Aby nálezowé prwní panští, jakož je

uslyšíte čteny, moc měli.

It. o ohni w této zemi, aby žádný

swéwolně nepúštěl.

lt. o silnice, aby šly, jakož od sta-

radáwna byly, a ižádný aby na nich ne-

překážel.

It. cla a mýta powýšená a přičiněná

znowu aby minula, a cla aby byla wedle

starého obyčeje brána, jich nepowyšujíc.

It. o minci, aby nebyla w této zemi

jinde dělána, než u Hory, buďto na zla-

tých, na groších, na peniezích, neb na

haléřích.

It. nowí hradowé a twrze aby ne-

byli děláni, a kteříž hradowé a twrze

jsú pro jisté příčiny zbořeni, aby děláni

a oprawowáni nebyli zase bez wuole naj-

jasn. kniežete a pána p. Sigmunda Cies.

Mti krále Českého a zemské.

A ti nálezowé wšichni, tito i prwní,

aby byli držáni pod pokutami w prwních

nálezích popsanými.

Actum coram dominis baronibus, ut supra.

Trebon. 401b. Vienn. 3483. Talmb. 72b.

Mus. 170b.

155.

1437. Quarto Matthiae, A. 5.

Domini barones in pleno judicio: Tak

jakož Ciesařowa Milost pan Sigmund Če-

ský král se pány zjednali sú a osadili

úřad plný u desk zemských, a prowoláno

jest, že práwa propúšteji zemská, aby před

se šla wšem zemanuom wuobec: nalezli

sú, že ktožkoliwěk potřebuje komorníka

k wedení práwa a řádu zemského, na

póhony i na jiné wěci wšechny wedle
