Rejstřík věcný. 705

niků 549; pohnaného neztra-
tivá 549; původova 549; za
nemoc na vyhlášení 549; po-
pravcům a kmetům vydána býti
má 550; o škody útratní 550;
se zmatkem 550; přísahy ne-
provésti 549 na přísahu se
poddati 626.

přítel nejbližší 555.

probíř královský z Drasova Arnošt
50; srv. frzuchar.

probošt kostela Pražského Arnošt
Šlejnic z Tollšteina 437.

prodati a doprodati statek 557.

prokurator vyslán srovnati strany
55.

prokurator královský Matyáš Libák

z Radovesic 338, 358, 463.

promlčení smlouvy 338; spolku
564; správy 602.

protonotář Pražanů mistr Jeronym
423.

prsten půjčený 26; zlatý 342.

průvod, provésti, provozovati 547,
před soudem 627.

ptáky čižebné krásti 364.

o půhonech nálezové 536-541,
617-21; nedodaný 621; proti
obyčeji 27; přijati 618; smí-

šený 537; zmatečný 271, 355, |:

525, 618; půhonu zdvižení 31,
597, 643, propohánění z trojí
věci 295 ; viz pohnati.

půjčiti, půjčka 627 ; slíbena půjčka
19, 555; peníze půjčené 194,
360, 367.

purkrabí nejv. Pražský Jindřich
z Hradce 424; Jan z Janovic
463; Zdeněk ze Šternberka
517, 589.

purkrabí hradu Pražského Bohu-
slav Chrt z Ertína (1511-18)
1, 270, 276-7, 301, 333.

purkrabí Karlšteinský Tobiáš ze
Sulevic (1511) 1.

purkrabí kraje Hradeckého Bo-
huněk Černín 48, 123.

purkrecht 555.

půtka 553.

původ na soudé 550.

pych VI, 552, 621; člověka pod-
daného 69; lesní 136, 139,
177; oloupení poddaných 11;

polní 138; na těle 145-6; na
židu 358, 622; nahraditi 179,
210; napraviti 621, 623; z
pychu pohnati 30, 40, 52, 141,
203, 272, 306, 308, 321-2,
358, 362, 377, 379, 402-3,
412-3, 441, 621 ; spáchati 156,
166, 174, 180, 185, 192, od
více osob 552; pro pych uvěz-
niti 154; opět pohnati 107.

Rada královský 558; rada stará
městská 17, 579.

rčení 559, 628; promlčení 293;
neprovésti 560, 629; v dětin-
ství učiněné 291; umírá 629;
věna nevzíti 292; vydati pe-
níze odkázané 32; nesplněno
61, 65, 72, 274, 283, 286,
296, 320, 337, 341, 363, 377,
418, 629.

řečí viniti 560.

regenti království Českého IV, V,

registra komorní 281, 329; ku-
pecká 271, 294, 630; pur-
krabská 279, 345; soudu ko-
morního VI-VIII.

relací, relatoři 558; ke dskám

z neplnění summy 308; ze

škod nákladních a útratních

305, 308, 322.

řetěz zlatý 399.

robota beze stravy 311; robot
nevypravení 249; robotou obtí-

žiti klášter 56; roboty odpí-

rati 311.

rod, rodové zřízení 560.

roucha, látky na né 176, 221.

rozdíl 2, 250, 259-264, 497,
560, 607; z rozdílu pohnati 631.

rozvodnění 560, 593.

k ruce věrné přijati 423; uscho-
vati 641.

ručiti za někoho 98.

rukojemství 310, 312, 364, 558,
za věno 6, 47-8, 332, 368,
377, 386, 443, 475, 629; ne-
poskytnouti 19; v rukojemství
nestáti 209-1; z rukojemství
pohnati 13, 300; slíbiti 46;
z rukojemství nevyvazení 368,
371.

rukojmí 558, 629; za dluh 629;
slibu odpírající 629; pod zá-

klad 630; k ležení napomenouti
525 ; žádati 181.

rybník, rybnikáři 560; na ryb-
níku právo koupiti 403; ryb-
níku skopání 631; o rybník
smlouva 355; rybníkem strže-
ným škoda 560; v rybníku
ostrovec 113.

rytíře nepohnati pro zbití pacho-
lete 272; člověk rytířský 592,
631, služebníkem 638.

Sedlo ženské 445,

sena pobrání 625, 637.

sestra 571, nedílná 482.

silnice 578; svobodná král. 133,
165, 198; viz cesta.

sirotek léta mající 572; let ne-
mající 572; bez poručníka 573;
se sirotkem spolek 564; si-
rotka stravování nahraditi 39;
sirotčí statek 573, peníze 31,
208, 348; ze sirotků pohnati
626.

sladovnice kmetična 306.

slib 578, 638; zločinci daný 578;
ze slibu pohnati 15, 16, 179,
351, 353-4, 407, 578; učiniti
180; viz rčení.

slova důtklivá 340, 606, 631;
hanlivá 46, 609 ; posměšná 632.

služba boží nekonána 552; vá-
lečná v cizině 14, 25, 28; do
služby přijímati 203; služba
služebníku smluvena 155, ne-
zaplacena 330, 335, 361.

služebník, sloužiti 577 ; služebník
člověk rytířský 638; služeb-
níkům odkázati 99; služebníka
postavení 287.

smlouva 574, 633; o léčení 166 ;
mrtvá (promlčená) 338 ; o mzdě
164-5 ; manželská 242 ; nepeče-
těná 279; o rybník 355; o
statek 236; svatební 14; do
tří životů trvající 29; o vodu
633; o vyplácení peněz synovi
183; nedodržena 19, 55, 59,
64, 289, 299, 317, 322, 324,
325, 338, 349, 361, 366,
376, 381, 574, 633; smlouvou
prodati statek 576.

na sněm vyslaní z krajů (1517)
318.

89
