608
Rejstřík věcný.

Odběžné 22.
Odpustky 330, 333.
Odúmrť 234, 250, králi 495,
496, 503.
Ofěry 57, pobrány knězi 4.
Official duchovní v Praze (1446)
24.
Ohaři 102.
Ohlavy 438.
Olejnického cechu artikule 442,
464.
Oprava 75; nad klásterem 411.
Ostrožné 387. |
Ostrožníci 439.
Pacholek hostinský při řemesle
454; nájemní 475; rychtá-
řův 471, 488, 490; těžící 399.
Palič 283, 284.
Panoše, pečeti 83.
Pastva, spor o ni 128, 137;
pastýř městský 127; pastýřka
čaruje 307.
Pasy tlumoční 438.
Pavezy malované a žilované 474,
475.
Paytlíky 443.
Pečet kusem mistrovským u zlat-
níkův 481, 483; u listin práv-
ních důležita 408; menší, sekret
448; v majestátu s kontra-
sigillem 504; zlatá 471.
Pekárny 444, klášterské 382.
Pekařského cechu artikule 442-5.
Peníze cechovní 443, horské 81,
hořské 108, kostelní 4, kurní
a ženné 244, lesní 74, 88,
pomorní 121, rybní 243;
— mince měněna 193, neberná
173, 203, ražena v Budějovi-
cích 192, v Kutné Hoře 146,
Rosenberským dopuštěna 131,
mincíř 433; peníze dopravo-
vány s průvodem střelcův 146,
pálení peněz 482;
— groše 297, české bílé 216,
moravské váhy 511, groš má
sedm peněz 122; haléře 444,
454; hfivny horské 394; krej-
cary 297; orlíky 203; peníze
bílé 203, sedm do groše 122;
talenty 243; zlaté, váha jich
108, 115, cena v groších 93,
209, ostřihování 483.

Pergamen, parkamen, 35, 239,
266, 397, 414, 421, 484;
proužky s visutou pečetí 397
až 426, 497-560.
Perly 482.
Pikanky (?) 343.
Pilařského cechu artikule 471-3.
Písař nejv. zem. Mikuláš z Eger-
berka (1413) 402;
— Burian z Lípy na Lipnici
(1463) 132;
— Mikuláš z Landšteina (1474)
238, (1481) 411.
Písaři (notarii) v kanceláři krá-
lovské :
— Dythmar (z Meckebachu 1348)
495;
— František (1353) 498;
— Jan Eistetský (1355) 501;
— Jindřich z Wesalie (1361)
503;
— Jan z Kladska (1358) 387;
— Mikuláš z Kroměříže (1360-1)
391, 504;

— Milič z Kroměříže (1362) 506;
—— Petr Javorský či z Javorníka ?
(1371) 510;
— Theodorik Damerovský (1374)
510;
— Mikuláš probošt Kamerický
(1377) 513;
— Mikuláš z Poznaně (1377)
514;
— Vlachník z Weitmile (1385-9)
518, 526;
— Martin scholastik (1381-4)
514, 517;
— František probošt Nordhauský
(1395) 533;
Václav kanovník Pražský
(1400) 394;
— Jan z Bamberka (1410-13)
402, 550-3, 558;
— František probošt Ostřihom-
ský (1422) 407.
Písař markraběte Morav. Jošta
Ondřej (1386) 520;
— Theodorich z Prahy (1394)
531; srv. Protonotář.
Písař akt arcibiskupových Mařík
Kačer (1413) 558.
Písař desk zemských Václav
z Kozmic (1413) 558.

Písař. komory cís. Pavel (1370)
392.
Písař komorní 314; obroční 110.
Pivo bílé 80, 468; hostinské
241, 468, na ležení a na vy-
dání 468, Pražské dobré 467
až 469; piva vaření 136, 137,
278, 467, zemanům a sedlá-
kům v městě netrpěno 468;
piva nedostatek 240; pivnice
457, 469; pivovary 467-469.
Pláště branné stojaté 474.
Plat roční 411, 542, faráři 312,
klášterní 431, komorní 87, na
kostely 408, 409.
Plavy lesu 110.
Plsty sedlářské neb ševcovské
453, 487, 490.
Plstnáři mezi kloboučníky 453.

Pocta medem 98.
Počty úředníků panských chybné
69.
Poddaní purkrechtní 398, 399;
úroční 369-379, 548; pomoc
pánovi k ženitbě 66; nikdo jich
nesmí staviti 498; shořeli do-
stávají dříví 92, neplatí úroku
144; vrchnosti se o ně prou
299-304.
Podkomoří království Česk. (1400)
898, 394;
— Sigmund (Huler 1389) 526;
— Konrád bisk. Verdenský (1405)
546;
— Hájek z Hodětína (1413)
402;
— Jakub z Vřesovic (1525) 319.
Podkomoří císařovny Elišky Ol-
dřich (ze Sulzbachu) a jeho
náměstek Dětlin (1389) 523.
Podkoteční obchodníci v Praze
488, 490.
Podmistří u nožířů 438, v pivo-
vaře 469,
Pod obojí přijímání podmínkou
mistrovství u Praž. cechův
438, 460, 464, 467, 470,
472, 475, 476, 481, 484,
487, 490.
Podomní obchod se zapovídá 464.
Podruh a podruhyně v komoře
pivní 469; podruzi u řemesel
57, 150, 442, 443, 465.
