376 B. VIII. Zápisy Pražské
Beneš Košík z Lomnice.
Mikuláš z Lobkowic dictus Chudý.
Jan z božie milosti biskup Olomucký, kostela Litomyšlského poručník obecny.
My richtář, šempmistr, konšelé i wšecka obec na Horách Kutných.
My richtář, purgmistr a konšelé přísežní i wšecka obec města Čáslawi,
Já Hynek Berka z Dubé odjinud z Lipého, fojt Budišinský a w Lužicích.
My richtář, purgmistr, konšelé i wšecka obec města Nowého Kolína nad Labem.

My Albrecht z Koldic, pán na Bielini.
Já Příbek z Týnce odjinud z Chrašťowic.
Já Tas z Rizemburka.
Já Mikuláš z Lipničky.
Já Jan z Koloděj.
Já Jan z Seslawec.
Já Niklas Makšin.
My Dětřich z Čestic a Michal z Němčic.
Jan z Újezda a Ondřej od Hory.
My Jaroslaw Berka z Dubé seděním na

Milšteině.
Hynek z Wetly starší.
Hynek z Wetly mladší.
Jakeš z Krowa.
Fridrich z Krowa mladší.
Fridrich Pancieř.
Mikuláš z Chwališowa.
Zdeněk ze Smojna.
Niklas Zalezen.
Petr z Howswerda.
Kunat z Jilowého.

Jaroš Karas.
Hanušek Žabka.
Nikoláš z Lukendorfu.
Srša.
Petr Tryras.
Hlawně Zálezl.
Hawel Zálezil.
Mikuláš z Lewle.
Aleš.
Marcin Gor.

Přibek z Týnce.
Racek z Stwolenek.
Hotowec.
Janek Křestianek.
Jan Šlechta z Malkowic.
Martin Hawranek.
Marcin z Hauswerdu.
Swatka i jiní naši wšichni pacholky.
Já Jan Kapléř z Sulěwic.
My Bohuslaw z Dúpowa

a Diwiš z Zahrádky.
Já Čeněk z Ronowa odjinud z Letowic.
Hanuss Warmstorff genant,

Welfl, Lewtold, Čeřen,
Hanuš Čeřen,
Hanuš Tichnic,
Nikl von Reden,
Nikl Hermanstorf,
Daniel von Romburk,
Hodko von Rzemkow mit allen unserm

gesinde.
Já Wilém z Ronowa odjinud z Žrnosek.
Janek z Třeblewic.
Macejík z Medwědic.
Oldřich Sloták a naši wšichni pacholci.
Hanuss von Panewicz.
Nikl Daksse.
Hanuss von der Weze.
Fridman von Gererstorf.
Petr Makssin.
Henrich Powericz.
