1297

1085, 1133, 1149, 1150; purkrabí
1077.
ze Žebráka Zajícové: Oldřich 339, Zbyněk
228, 339.
Žebrání žáků 1122.
Želetice 1127.
Železo, ferrum 487, 583, 584, 590-592,
733, 816, 942; bavorské 599, chebské
599, kadaňské 599; ruční, ferrum
manuale 18, 68, 79.
Želivo, Zelew, klášter 901.
Žena obecní, mulier communis, gemaine
frau 20.
Žernoseky, Zrnoseky, Srnossek 422, 423.
Žertéri, truffatores, teuscher 13.
Žestoky, Zestok, 1013-1015.
Žháři, paliči, incendiarii, incensores,
mortprenner 19, 20, 64, 75, 336, 364,
365, 1142, 1144.
Žhářství, zapálení, pálení, oheň, incen-
dium, ignis, fewer, brant, mortprant,
19, 20, 66, 77, 181, 182, 209, 210, 688,
692, 753, 754, 1046, 1141, 1143; noční,
nocturna incendia 186, 187; úkladné,
secretum seu mortiferum incendium
240, 241, 601, 640.
Židé v Čechách, služebníci komory krá-
lovské, Judei Boemie, regie camere

servi, die Juden in dem lande zu

Behem gesessen, camerknechte 20, 26,

161, 328, 329, 346-348, 352, 437,

438, 559, 597, 767, 768, 821, 871, 872,

937, 946; v Chebsku, die Juden zu

Eger 200, 201, 390, 443, 444, 708, 721,

809, 810, 837, 866.

Žirotín hrad, 1003, 1163.

Žitava, Sittavia 189, 410, 411, 538.

Žitavský Jan, Sittauwer, přísežný na

Horách Kutných 443.

ze Žitavy, Sittawia, Častalov 6, Chval

41, Jindřich 6, 41.

Žitenice u Litoměřic 462.

Žito, siligo 216, 305, 307, 792.

Žiželice, Zewslicz, ves 939.

Žleby 162, 163, 165, 246–248.

Žlutice, oppidum Luticz, Lueticz 487,

615, 616.

Žofie, Žofka, královna Česká 928, 930,

972, 1106, 1137, 1138, 1152, 1153,

1182.

Žoky chmele, sacci humuli, 484, 485,

solní, sacci 483.

Žoldnéři městští, stipendiarii 1114, 1116.

Žumburk, Sumirburch, Sumerburc 48

49, 107, 108, 1185.
