svěřiti — uvěřiti

světle — jasně, zřejmě

světlý — jasný, zřejmý

světnicě : světnicě súdná - soudní
síň

svítězilý — vítězný

svítěziti — zvítěziti

svláčeti — svlékati

svoditi — svádéti; svoditi ot viery —
odvádéti od víry

svolánie — sněm, koncil

svrchovaný : svrchovaný — přietel —
nejlepší, nejdokonalejší

šelmový — zvířecí, obludný; sto-
lice šelmová — morová

škola : obecná škola — universita

školní : mistr školní — správce
školy; obyčej školní — scholas-
tická metoda

škornicě f. pl. — škorně

šlechetný — ušlechtilý

šlojieř — šlojíř, závoj

štóla, štula — součást mešního
roucha

švécký — švédský

tafat — taft, jemná tkanina

tarenský (lat. Tricaricensis) —
z Tricaricu (Italie)

tělesný — hmotný

telonenský — toulonský, z Toulonu
(Francie)

telonský v. dolenský

tepati — tlouci, bíti

tepruv — teprv

termacenský — z Tournay (Belgie)

tknúti sě — dotknouti se

tlačiti — utlačovati; tlačiti nohama
— rozšlapávati

tovariš — společník, druh

tovařišstvie : tovařišstvie svatých —
společenství se svatými

traktátec — traktát, učené pojed-
nání

transsubstanciacio (lat.) — přepod-
statnění, proměna

tresktánie — trestání; kárání

tresktati — trestati; kárati

trpědlivost — trpělivost, strpení

trútiti — truditi, tížití

trvati — setrvávati

tržiti :tržiti o něco — kupčiti

s něčím

třiesla pl. n. — třísla, slabiny

tučný — tlustý, vypasený

tudiež — tam; také

tuhý : tuhý nepřietel — krutý, ne-
úprosný

turonenský — touronský

tvářnost — výraz tváře

tvrditi — utvrzovati

týti — prospívati

učedlník — učedník, žák
učenie : vysoké učenie, obecné učenie —
universita

účinek — čin, skutek

úd — příslušník, člen; přívrženec

udatně — opovážlivě

uhlédati : uhlédati svój čas — najíti
vhodný čas

ucházěti — unikati, utíkati

ujímati — ubírati

ujíti — uniknouti

újma — nedostatek

úfati : úfati někomu — doufati
v někoho

uhroziti — zastrašiti
úkora — pohana
ukrátili — zkrátiti

293
