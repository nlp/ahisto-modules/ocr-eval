FROM python:3.7

ENV DEBIAN_FRONTEND=noninteractive \
    TERM=xterm

COPY requirements.txt /

RUN set -e -o xtrace \
  ; apt-get -qy update \
  ; apt-get -qy install --no-install-recommends libgl1 \
                                                libglib2.0-0 \
                                                parallel \
  ; parallel --will-cite \
  ; pip install -U pip \
  ; pip install -r /requirements.txt

COPY scripts /scripts/

WORKDIR /
